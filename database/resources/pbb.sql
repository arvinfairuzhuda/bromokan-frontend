/*
Navicat MySQL Data Transfer

Source Server         : local_ubuntu
Source Server Version : 50733
Source Host           : 192.168.1.10:3306
Source Database       : pbb

Target Server Type    : MYSQL
Target Server Version : 50733
File Encoding         : 65001

Date: 2021-03-28 20:39:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for access
-- ----------------------------
DROP TABLE IF EXISTS `access`;
CREATE TABLE `access` (
  `id` int(11) NOT NULL,
  `access_control_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of access
-- ----------------------------

-- ----------------------------
-- Table structure for access_control
-- ----------------------------
DROP TABLE IF EXISTS `access_control`;
CREATE TABLE `access_control` (
  `id` int(11) NOT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of access_control
-- ----------------------------

-- ----------------------------
-- Table structure for active_pinjaman_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `active_pinjaman_kelompok`;
CREATE TABLE `active_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_aktif` date NOT NULL,
  `alokasi_dana` varchar(50) NOT NULL,
  `prosentase_jasa_active` int(100) NOT NULL,
  `jenis_jasa_active` int(50) NOT NULL,
  `jangka_bulan_active` varchar(50) NOT NULL,
  `sistem_angsuran_active` varchar(100) NOT NULL,
  `lunas` smallint(100) NOT NULL,
  `spk_kelompok` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`loan_id`),
  KEY `loan_id` (`loan_id`),
  CONSTRAINT `active_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of active_pinjaman_kelompok
-- ----------------------------

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `ip` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `get` longtext,
  `post` longtext,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of activity
-- ----------------------------

-- ----------------------------
-- Table structure for angsuran
-- ----------------------------
DROP TABLE IF EXISTS `angsuran`;
CREATE TABLE `angsuran` (
  `id_angsuran` int(11) NOT NULL,
  `id_transaksi` int(25) NOT NULL,
  `loan_id` int(25) NOT NULL,
  `pokok` decimal(50,0) NOT NULL,
  `jasa` decimal(50,0) NOT NULL,
  `denda` decimal(50,0) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_angsuran`),
  KEY `id_transaksi` (`id_transaksi`),
  KEY `loan_id` (`loan_id`),
  CONSTRAINT `angsuran_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`),
  CONSTRAINT `angsuran_ibfk_2` FOREIGN KEY (`id_transaksi`) REFERENCES `jurnal_transaksi` (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of angsuran
-- ----------------------------

-- ----------------------------
-- Table structure for desa
-- ----------------------------
DROP TABLE IF EXISTS `desa`;
CREATE TABLE `desa` (
  `id_desa` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nama_desa` varchar(30) DEFAULT NULL,
  `telepon_kades` varchar(20) DEFAULT NULL,
  `nip_kades` varchar(30) DEFAULT NULL,
  `nama_sekdes` varchar(30) DEFAULT NULL,
  `telepon_sekdes` varchar(20) DEFAULT NULL,
  `alamat_kantor_desa` varchar(250) DEFAULT NULL,
  `telepon_kantor` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_desa`),
  KEY `username` (`username`,`id_kecamatan`),
  KEY `id_kecamatan` (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of desa
-- ----------------------------
INSERT INTO `desa` VALUES ('1', 'kasreman', '130', 'dsase', '123', null, null, '08122222222', null, null, '2021-03-28 10:47:14', '2021-03-28 10:47:14');

-- ----------------------------
-- Table structure for jaminan
-- ----------------------------
DROP TABLE IF EXISTS `jaminan`;
CREATE TABLE `jaminan` (
  `id_jaminan` int(25) NOT NULL,
  `jenis_jaminan` varchar(50) NOT NULL,
  `nomor_kepemilikan` int(100) NOT NULL,
  `merk_tipe` varchar(50) NOT NULL,
  `alamat_jaminan` varchar(50) NOT NULL,
  `nilai jaminan` decimal(10,0) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jaminan`),
  KEY `id_jaminan` (`id_jaminan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of jaminan
-- ----------------------------
INSERT INTO `jaminan` VALUES ('1', 'asd', '123', '123', 'fasd', '123', null, null);

-- ----------------------------
-- Table structure for jurnal_transaksi
-- ----------------------------
DROP TABLE IF EXISTS `jurnal_transaksi`;
CREATE TABLE `jurnal_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `rek_debit` int(100) NOT NULL,
  `rek_kredit` int(100) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `tgl_transaksi` varchar(50) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_transaksi`),
  KEY `rek_debit` (`rek_debit`),
  KEY `rek_kredit` (`rek_kredit`),
  KEY `nip` (`nip`),
  CONSTRAINT `jurnal_transaksi_ibfk_1` FOREIGN KEY (`rek_debit`) REFERENCES `rekening_akun` (`parent_rekening`),
  CONSTRAINT `jurnal_transaksi_ibfk_2` FOREIGN KEY (`rek_kredit`) REFERENCES `rekening_akun` (`parent_rekening`),
  CONSTRAINT `jurnal_transaksi_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of jurnal_transaksi
-- ----------------------------

-- ----------------------------
-- Table structure for kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(50) NOT NULL,
  `nama_camat` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------

-- ----------------------------
-- Table structure for kelompok_peminjam
-- ----------------------------
DROP TABLE IF EXISTS `kelompok_peminjam`;
CREATE TABLE `kelompok_peminjam` (
  `kd_kelompok` varchar(10) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `id_desa` int(30) NOT NULL,
  `nama_kelompok` varchar(50) NOT NULL,
  `alamat_kelompok` text NOT NULL,
  `telepon_kelompok` varchar(50) NOT NULL,
  `tanggal_berdiri` date NOT NULL,
  `nama_ketua_kel` varchar(50) NOT NULL,
  `nama_sekretaris_kel` varchar(50) NOT NULL,
  `nama_bendahara` varchar(50) NOT NULL,
  `jenis_kegiatan` varchar(50) NOT NULL,
  `tingkat_kelompok` varchar(50) NOT NULL,
  `fungsi_kelompok` varchar(50) NOT NULL,
  `jenis_usaha_kelompok` varchar(50) NOT NULL,
  `status_kelompok` varchar(50) NOT NULL,
  `produk_pinjaman` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kd_kelompok`),
  KEY `nip` (`nip`),
  KEY `username` (`username`),
  KEY `id_desa` (`id_desa`),
  CONSTRAINT `kelompok_peminjam_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`),
  CONSTRAINT `kelompok_peminjam_ibfk_2` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of kelompok_peminjam
-- ----------------------------

-- ----------------------------
-- Table structure for kunjungan_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `kunjungan_kelompok`;
CREATE TABLE `kunjungan_kelompok` (
  `id_kunjungan` int(30) NOT NULL,
  `kd_kelompok` varchar(10) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `tanggal_kunjungan` date NOT NULL,
  `hasil_kunjungan` longtext NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kunjungan`),
  KEY `nip` (`nip`),
  KEY `kd_kelompok` (`kd_kelompok`),
  CONSTRAINT `kunjungan_kelompok_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `person_pegawai` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of kunjungan_kelompok
-- ----------------------------

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `id_jaminan` int(11) NOT NULL,
  `nik_penjamin` int(16) DEFAULT NULL,
  `nik_member` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `nama_member` varchar(100) DEFAULT NULL,
  `jk_member` varchar(50) DEFAULT NULL,
  `tempat_lahir_member` varchar(50) DEFAULT NULL,
  `tanggal_lahir_member` varchar(100) DEFAULT NULL,
  `alamat_ktp` varchar(100) DEFAULT NULL,
  `no_kk` varchar(50) DEFAULT NULL,
  `url_foto` varchar(50) DEFAULT NULL,
  `no_hp_member` varchar(100) DEFAULT NULL,
  `jenis_usaha_member` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_member`),
  KEY `id_jaminan` (`id_jaminan`,`nik_penjamin`),
  KEY `nik_penjamin` (`nik_penjamin`),
  KEY `username` (`username`),
  CONSTRAINT `member_ibfk_1` FOREIGN KEY (`nik_penjamin`) REFERENCES `penjamin` (`nik_penjamin`),
  CONSTRAINT `member_ibfk_2` FOREIGN KEY (`id_jaminan`) REFERENCES `jaminan` (`id_jaminan`),
  CONSTRAINT `member_ibfk_3` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', '1', '33', 'dsaasd', 'leo', '', 'l', '432', '4312', 'fdas', 'fdas', 'fdas', null, null, null, null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for pemanfaat
-- ----------------------------
DROP TABLE IF EXISTS `pemanfaat`;
CREATE TABLE `pemanfaat` (
  `kd_kelompok` varchar(10) NOT NULL,
  `id_member` int(25) NOT NULL,
  `alokasi_pemanfaat` decimal(50,0) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kd_kelompok`,`id_member`),
  KEY `kd_kelompok` (`kd_kelompok`,`id_member`),
  KEY `id_member` (`id_member`),
  CONSTRAINT `pemanfaat_ibfk_1` FOREIGN KEY (`kd_kelompok`) REFERENCES `kelompok_peminjam` (`kd_kelompok`),
  CONSTRAINT `pemanfaat_ibfk_2` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pemanfaat
-- ----------------------------

-- ----------------------------
-- Table structure for penjamin
-- ----------------------------
DROP TABLE IF EXISTS `penjamin`;
CREATE TABLE `penjamin` (
  `nik_penjamin` int(25) NOT NULL,
  `nama_lengkap_penjamin` varchar(50) NOT NULL,
  `hubungan_penjamin` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nik_penjamin`),
  KEY `nik_penjamin` (`nik_penjamin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of penjamin
-- ----------------------------
INSERT INTO `penjamin` VALUES ('33', 'fdasdf', 'afsdfasd', null, null);

-- ----------------------------
-- Table structure for person_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `person_pegawai`;
CREATE TABLE `person_pegawai` (
  `nip` varchar(30) NOT NULL,
  `USERNAME` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nip`),
  KEY `nip` (`nip`),
  KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of person_pegawai
-- ----------------------------

-- ----------------------------
-- Table structure for pinjaman_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `pinjaman_kelompok`;
CREATE TABLE `pinjaman_kelompok` (
  `loan_id` int(11) NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `nilai_pengajuan` int(100) NOT NULL,
  `prosentase_jasa_kelompok` int(50) NOT NULL,
  `jangka_bulan_kelompok` varchar(50) NOT NULL,
  `jenis_jasa` varchar(100) NOT NULL,
  `sistem_angsuran_pokok` varchar(50) NOT NULL,
  `note_pinjaman_kelompok` varchar(250) NOT NULL,
  `state_pinjaman_kelompok` varchar(100) NOT NULL,
  `jenis_produk_pinjaman_kelompok` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`loan_id`),
  KEY `loan_id` (`loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of pinjaman_kelompok
-- ----------------------------

-- ----------------------------
-- Table structure for rekening_akun
-- ----------------------------
DROP TABLE IF EXISTS `rekening_akun`;
CREATE TABLE `rekening_akun` (
  `id_rekening` int(11) NOT NULL,
  `parent_rekening` int(11) NOT NULL,
  `rek_debit` int(11) NOT NULL,
  `rek_kredit` int(11) NOT NULL,
  `nama_rekening` varchar(50) NOT NULL,
  `kode_rekening` int(100) NOT NULL,
  `level_rekening` int(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_rekening`),
  KEY `parent_rekening` (`parent_rekening`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rekening_akun
-- ----------------------------

-- ----------------------------
-- Table structure for saldo_bulan_berjalan
-- ----------------------------
DROP TABLE IF EXISTS `saldo_bulan_berjalan`;
CREATE TABLE `saldo_bulan_berjalan` (
  `id` int(11) NOT NULL,
  `id_rekening` int(25) NOT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `total_debit` decimal(10,0) DEFAULT NULL,
  `total_kredit` decimal(10,0) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_rekening` (`id_rekening`),
  CONSTRAINT `saldo_bulan_berjalan_ibfk_1` FOREIGN KEY (`id_rekening`) REFERENCES `rekening_akun` (`id_rekening`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of saldo_bulan_berjalan
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_active` varchar(10) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', '$2y$10$3Q8va1xZemnqz/KdU4p/q.iPQf/aJIOk.nkkmfpJoJ7jqY5zA8zSK', 'YES', 'admin', null, null, '1');
INSERT INTO `users` VALUES ('leo', '$2y$10$TOhoMggAkb/UOFRR.fDfruzL9VuXxZR/jvhYLcOfn1zjBa9Xsefj2', null, null, '2021-03-28 02:40:16', '2021-03-28 02:40:16', '2');

-- ----------------------------
-- Table structure for verified_pinjaman_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `verified_pinjaman_kelompok`;
CREATE TABLE `verified_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_verified` date NOT NULL,
  `alokasi_dana_verified` varchar(50) NOT NULL,
  `prosentase_jasa_verified` int(100) NOT NULL,
  `jenis_jasa_kelompok_verified` int(50) NOT NULL,
  `jangka_kelompok_verified` varchar(50) NOT NULL,
  `sistem_angsuran_verified` varchar(100) NOT NULL,
  `jenis_pinjaman_verified` varchar(100) NOT NULL,
  `to_waiting_list` smallint(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`loan_id`),
  KEY `loan_id` (`loan_id`),
  CONSTRAINT `verified_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of verified_pinjaman_kelompok
-- ----------------------------

-- ----------------------------
-- Table structure for waiting_pinjaman_kelompok
-- ----------------------------
DROP TABLE IF EXISTS `waiting_pinjaman_kelompok`;
CREATE TABLE `waiting_pinjaman_kelompok` (
  `loan_id` int(20) NOT NULL,
  `tgl_waiting` date NOT NULL,
  `alokasi_dana_waiting` varchar(50) NOT NULL,
  `prosentase_jasa_waiting` int(100) NOT NULL,
  `jenis_jasa_kelompok_waiting` int(50) NOT NULL,
  `jangka_kelompok_waiting` varchar(50) NOT NULL,
  `sistem_angsuran_kelompok` varchar(100) NOT NULL,
  `activated` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`loan_id`),
  KEY `loan_id` (`loan_id`),
  CONSTRAINT `waiting_pinjaman_kelompok_ibfk_1` FOREIGN KEY (`loan_id`) REFERENCES `pinjaman_kelompok` (`loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of waiting_pinjaman_kelompok
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
