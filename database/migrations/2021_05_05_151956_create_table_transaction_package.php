<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTransactionPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->double('price_destination')->after('id_destination')->nullable();
            $table->integer('id_package')->nullable()->after('price_destination');
            $table->double('price_package')->after('id_package')->nullable();
        });

        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('person');
            $table->string('day');
            $table->integer('id_destination');
            $table->double('amount');
            $table->enum('status', ['ENABLE', 'DISABLE']);
            $table->timestamps();
            $table->integer('created_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('price_destination');
            $table->dropColumn('id_package');
            $table->dropColumn('price_package');
        });
        Schema::dropIfExists('packages');
    }
}
