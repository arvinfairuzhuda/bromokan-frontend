"use strict";

// Class definition
var KTWizard1 = function () {
	// Base elements
	var _wizardEl;
	var _formEl;
	var _wizardObj;
	var _validations = [];

	// Private functions
	var _initValidation = function () {
		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
		// Step 1
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					address1: {
						validators: {
							notEmpty: {
								message: 'Address is required'
							}
						}
					},
					postcode: {
						validators: {
							notEmpty: {
								message: 'Postcode is required'
							}
						}
					},
					city: {
						validators: {
							notEmpty: {
								message: 'City is required'
							}
						}
					},
					state: {
						validators: {
							notEmpty: {
								message: 'State is required'
							}
						}
					},
					country: {
						validators: {
							notEmpty: {
								message: 'Country is required'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));

		// Step 2
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					id_destination: {
						validators: {
							notEmpty: {
								message: 'Pilih salah satu destinasi yaa !'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));

		// Step 3
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));

		// Step 4
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					id_hotel: {
						validators: {
							notEmpty: {
								message: 'Hotel mana yang kamu pilih?'
							}
						}
					},
					hotel_room: {
						validators: {
							notEmpty: {
								message: 'Kamu harus mengisikan berapa jumlah kamar yang ingin kamu sewa yaa!'
							},
							greaterThan: {
								message: 'Kamu harus mengisikan berapa jumlah kamar yang ingin kamu sewa yaa!',
								min: 1,
							}
						}
					},
					hotel_date_start: {
						validators: {
							notEmpty: {
								message: 'Kamu harus mengisikan berapa lama kamu mau menginap yaa!'
							}
						}
					},
					hotel_date_end: {
						validators: {
							notEmpty: {
								message: 'Kamu harus mengisikan berapa lama kamu mau menginap yaa!'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));

		// Step 5
		_validations.push(FormValidation.formValidation(
			_formEl,
			{
				fields: {
					name: {
						validators: {
							notEmpty: {
								message: 'Halo namanya siapa nih?'
							}
						}
					},
					email: {
						validators: {
							notEmpty: {
								message: 'Email kakak tolong diisi yaa.'
							}
						}
					},
					phone: {
						validators: {
							notEmpty: {
								message: 'Nomor teleponnya tolong diisi yaa, agar kami bisa menghubungi kakak.'
							}
						}
					},
					whatsapp: {
						validators: {
							notEmpty: {
								message: 'Nomor whatsapp nya juga yaa, biar kita mudah untuk menghubungi kakak.'
							}
						}
					},
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap({
						//eleInvalidClass: '',
						eleValidClass: '',
					})
				}
			}
		));
	}

	var _initWizard = function () {
		// Initialize form wizard
		_wizardObj = new KTWizard(_wizardEl, {
			startStep: 1, // initial active step number
			clickableSteps: false  // allow step clicking
		});

		// Validation before going to next page
		_wizardObj.on('change', function (wizard) {
			if (wizard.getStep() > wizard.getNewStep()) {
				return; // Skip if stepped back
			}

			// Validate form before change wizard step
			var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
			var needHotel = document.getElementById("need_hotel").checked;
			var idHotel = document.getElementById("id_hotel").value;
			if (!needHotel && (wizard.getStep()) == 4) {
				validator = _validations[wizard.getStep() - 2]
			}

			if (validator) {
				validator.validate().then(function (status) {
					if (status == 'Valid') {
						wizard.goTo(wizard.getNewStep());

						KTUtil.scrollTop();
					} else {
						Swal.fire({
							text: wizard.getStep() === 4 && !idHotel ? "Hotel mana yang kamu pilih?" : "Aduh, ada yang belum kamu isi nih :(",
							icon: "error",
							buttonsStyling: false,
							confirmButtonText: "Ok, got it!",
							customClass: {
								confirmButton: "btn font-weight-bold btn-light"
							}
						}).then(function () {
							if ((wizard.getStep()) !== 4) {
								KTUtil.scrollTop();
							}
						});
					}
				});
			}

			return false;  // Do not change wizard step, further action will be handled by he validator
		});

		// Change event
		_wizardObj.on('changed', function (wizard) {
			KTUtil.scrollTop();
		});

		// Submit event
		_wizardObj.on('submit', function (wizard) {
			Swal.fire({
				text: "Berangkatkan ke Bromo nih ?",
				icon: "success",
				showCancelButton: true,
				buttonsStyling: false,
				confirmButtonText: "Gas!",
				cancelButtonText: "Tunggu.",
				customClass: {
					confirmButton: "btn font-weight-bold btn-primary",
					cancelButton: "btn font-weight-bold btn-default"
				}
			}).then(function (result) {
				if (result.value) {
					_formEl.submit(); // Submit form
				} else if (result.dismiss === 'cancel') {
					Swal.fire({
						text: "Tunggu apa lagi kak?",
						icon: "error",
						buttonsStyling: false,
						confirmButtonText: "Sebentar",
						customClass: {
							confirmButton: "btn font-weight-bold btn-primary",
						}
					});
				}
			});
		});
	}

	return {
		// public functions
		init: function () {
			_wizardEl = KTUtil.getById('kt_wizard');
			_formEl = KTUtil.getById('kt_form');

			_initValidation();
			_initWizard();
		}
	};
}();

jQuery(document).ready(function () {
	KTWizard1.init();
});
