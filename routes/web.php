<?php

use App\Http\Controllers\AuthController;
use App\Models\Destination;
use App\Models\Hotel;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [AuthController::class, 'login'])->name('login');
Route::get('register', [AuthController::class, 'register'])->name('register');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::post('attemptLogin', [AuthController::class, 'attemptLogin'])->name('attemptLogin');
Route::post('storeRegister', [AuthController::class, 'storeRegister'])->name('storeRegister');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/kebutuhanmu/sesuaikan', 'OrderController@custom')->name('order.custom');
Route::get('/pesananku/mailtest/{code}', 'OrderController@testMail')->name('order.testMail');
Route::resource('/kebutuhanmu', 'OrderController');
Route::get('/pesananku/gagal', 'OrderController@failed')->name('order.failed');
Route::get('/pesananku/{code}', 'OrderController@finish')->name('order.finish');

//Modal
Route::get('/modal/penginapan/{id}', 'ModalController@penginapan')->name('modal.penginapan');
Route::get('/modal/destination/{id}', 'ModalController@destination')->name('modal.destination');
Route::get('/modal/wisata/{id}', 'ModalController@wisata')->name('modal.wisata');

//Ajax
Route::get('/ajax/getHotelById/{id}', function ($id) {
    return Hotel::find($id);
});

Route::get('/ajax/getDestinationById/{id}', function ($id) {
    return Destination::find($id);
});
