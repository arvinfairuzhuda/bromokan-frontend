<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination_wisata extends Model
{
    use HasFactory;

    public $table = 'destination_wisata';

    public function destination()
    {
        return $this->hasMany(Destination::class);
    }

    public function wisata()
    {
        return $this->hasMany(Wisata::class);
    }
}
