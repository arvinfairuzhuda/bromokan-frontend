<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;

    public function wisata()
    {
        return $this->belongsToMany(Wisata::class, 'destination_wisata', 'id_destination', 'id_wisata');
    }

    public function package()
    {
        return $this->hasMany(Package::class);
    }
}
