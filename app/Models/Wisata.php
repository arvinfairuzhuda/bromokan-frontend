<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wisata extends Model
{
    use HasFactory;

    public $table = 'wisata';

    public function destination()
    {
        return $this->belongsToMany(Destination::class);
    }

    public function file()
    {
        return $this->hasMany(File::class, 'table_id', 'id');
    }
}
