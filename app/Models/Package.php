<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    public $table = 'packages';

    public function destination()
    {
        return $this->hasOne(Destination::class);
    }

    public function file()
    {
        return $this->hasMany(File::class, 'table_id', 'id');
    }
}
