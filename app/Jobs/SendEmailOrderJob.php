<?php

namespace App\Jobs;

use App\Mail\OrderMail;
use App\Models\Destination;
use App\Models\Hotel;
use App\Models\Konfig;
use App\Models\Transaction;
use App\Models\Transaction_hotel;
use Barryvdh\DomPDF\Facade as PDF;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $code;
    public $transaction;
    public $transaction_destination;
    public $transaction_hotel;
    public $transaction_hotel_data;
    public $priceTicket;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;

        $this->transaction = Transaction::where('code', $code)->first();
        $this->transaction_destination = Destination::find($this->transaction['id_destination']);
        $this->transaction_hotel = Transaction_hotel::where('id_transaction', $this->transaction['id'])->first();
        $this->transaction_hotel_data = Hotel::find($this->transaction_hotel['id_hotel']);

        $priceWeekdays = Konfig::find(26)['value'];
        $priceWeekends = Konfig::find(27)['value'];
        $dateGo = Carbon::parse($this->transaction['start_date']);
        $this->priceTicket = $dateGo->isWeekend() ? $priceWeekends : $priceWeekdays;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data['code'] = $this->code;
        $data['transaction'] = $this->transaction;
        $data['transaction_destination'] = $this->transaction_destination;
        $data['transaction_hotel'] = $this->transaction_hotel;
        $data['transaction_hotel_data'] = $this->transaction_hotel_data;
        $data['priceTicket'] = $this->priceTicket;

        $pdf = SnappyPdf::loadView('pdf.invoice', $data);
        Mail::to('bromokan.adm@gmail.com')->send(new OrderMail($this->code, $pdf->output()));
        Mail::to($this->transaction['email'])->send(new OrderMail($this->code, $pdf->output()));
    }
}
