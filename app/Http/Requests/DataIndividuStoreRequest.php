<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataIndividuStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik_member' => [
                'required'
            ],
            'nama_member' => [
                'required'
            ],
            'nama_member' => [
                'required'
            ],
            'nik_member' => [
                'required'
            ],
            'foto' => [
                'required'
            ],
            'jk_member' => [
                'required'
            ],
            'tempat_lahir_member' => [
                'required'
            ],
            'tanggal_lahir_member' => [
                'required'
            ],
            'alamat_ktp' => [
                'required'
            ],
            'no_kk' => [
                'required'
            ],
            'no_hp_member' => [
                'required'
            ],
            'jenis_usaha_member' => [
                'required'
            ],

            'jenis_jaminan' => [
                'required'
            ],
            'nomor_kepemilikan' => [
                'required'
            ],
            'alamat_jaminan' => [
                'required'
            ],
            'nilai_jaminan' => [
                'required'
            ],
            'merk_tipe' => [
                'required'
            ],

            'nik_penjamin' => [
                'required'
            ],
            'nama_lengkap_penjamin' => [
                'required'
            ],
            'hubungan_penjamin' => [
                'required'
            ],

            'username' => [
                'required'
            ],
            'password' => [
                'required'
            ]
        ];
    }
}
