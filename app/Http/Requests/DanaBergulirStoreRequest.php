<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DanaBergulirStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kd_kelompok' => ['required'],
            'nilai_pengajuan' => ['required'],
            'prosentase_jasa_kelompok' => ['required'],
            'jangka_bulan_kelompok' => ['required'],
            'jenis_jasa' => ['required'],
            'sistem_angsuran_pokok' => ['required'],
            'sistem_angsuran_jasa' => ['required'],
            'jenis_produk_pinjaman_kelompok' => ['required']
        ];
    }
}
