<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataKelompokStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_kelompok' => [
                'required'
            ],
            'alamat_kelompok' => [
                'required'
            ],
            'telepon_kelompok' => [
                'required'
            ],
            'nama_bendahara' => [
                'required'
            ],
            'jenis_kegiatan' => [
                'required'
            ],
            'tingkat_kelompok' => [
                'required'
            ],
            'fungsi_kelompok' => [
                'required'
            ],
            'jenis_usaha_kelompok' => [
                'required'
            ],
            'status_kelompok' => [
                'required'
            ],
            'produk_pinjaman' => [
                'required'
            ],
            'id_desa' => [
                'required'
            ],
            'nama_ketua_kel' => [
                'required'
            ],
            'nama_sekretaris_kel' => [
                'required'
            ],
        ];
    }
}
