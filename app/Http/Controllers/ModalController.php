<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use App\Models\Hotel;
use App\Models\Wisata;
use Illuminate\Http\Request;

class ModalController extends Controller
{
    public function __construct()
    {
    }

    public function penginapan($id)
    {
        $page_title = 'Gaskan Saja';
        $page_description = 'Bromokan Saja';

        $hotel = Hotel::find($id);
        $hotel['photo'] = $hotel->file()->where('table', 'hotels')->get();

        return view('page.order.modal.detail-penginapan', compact('page_title', 'page_description', 'hotel'));
    }

    public function wisata($id)
    {
        $page_title = 'Gaskan Saja';
        $page_description = 'Bromokan Saja';

        $wisata = Wisata::find($id);
        $wisata['photo'] = $wisata->file()->where('table', 'wisata')->get();

        // dd($wisata['photo']);
        return view('page.order.modal.detail-wisata', compact('page_title', 'page_description', 'wisata'));
    }

    public function destination($id)
    {
        $page_title = 'Gaskan Saja';
        $page_description = 'Bromokan Saja';

        $destination = Destination::find($id);

        $wisata = $destination->wisata;
        $wisata = $wisata->map(function ($item) {
            $item['photo'] = $item->file()->where('table', 'wisata')->first();
            return $item;
        });

        return view('page.order.modal.detail-destination', compact('page_title', 'page_description', 'destination', 'wisata'));
    }
}
