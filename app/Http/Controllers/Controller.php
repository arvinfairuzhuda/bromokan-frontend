<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // session('token', '');
    }

    public function request($method, $url, $body = [])
    {
        $client = new Client([
            ['base_uri' => env('APP_URL') . '/api/']
        ]);

        $response = $client->request($method, $url, [
            'body' => json_encode($body)
        ]);

        dd($response);
        return $response->getBody();
    }

    public function authenticated()
    {
        if (session('token')) {
            return route('home');
        }

        return redirect('login');
    }
}
