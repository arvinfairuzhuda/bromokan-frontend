<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $page_title = 'Ke Bromo Tanpa Ribet';
        $page_description = 'Butuh Liburan ? #bromokansaja';

        return view('page.home', compact('page_title', 'page_description'));
    }
}
