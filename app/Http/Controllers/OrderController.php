<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailOrderJob;
use App\Mail\OrderMail;
use App\Models\Destination;
use App\Models\Hotel;
use App\Models\Konfig;
use App\Models\Package;
use App\Models\Transaction;
use App\Models\Transaction_hotel;
use App\Models\Wisata;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $page_title = 'Pilih Pesananmu ya!';
        $page_description = 'Bromokan Saja';
        $packages = Package::all();

        $packages = $packages->map(function ($item) {
            $item['photo'] = $item->file()->where('table', 'packages')->first();
            return $item;
        });

        // dd($packages);
        return view('page.order.index', compact('page_title', 'page_description', 'packages'));
    }

    public function custom()
    {
        $page_title = 'Pilih Pesananmu ya!';
        $page_description = 'Bromokan Saja';
        $hotels = Hotel::all();
        $wisatas = Wisata::all();
        $destinations = Destination::all();

        $hotels = $hotels->map(function ($item) {
            $item['photo'] = $item->file()->where('table', 'hotels')->first();
            return $item;
        });

        $wisatas = $wisatas->map(function ($item) {
            $item['photo'] = $item->file()->where('table', 'wisata')->first();
            return $item;
        });

        $destinations = $destinations->map(function ($item) {
            $item['wisata'] = $item->wisata;
            $item['wisata'] = $item['wisata']->map(function ($item) {
                $item['photo'] = $item->file()->where('table', 'wisata')->first();
                return $item;
            });
            return $item;
        });

        $priceWeekdays = Konfig::find(26)['value'];
        $priceWeekends = Konfig::find(27)['value'];

        // dd($hotels);
        return view('page.order.custom', compact('page_title', 'page_description', 'hotels', 'wisatas', 'destinations', 'priceWeekdays', 'priceWeekends'));
    }

    public function store(Request $request)
    {
        $attr = $request->all();

        $priceWeekdays = Konfig::find(26)['value'];
        $priceWeekends = Konfig::find(27)['value'];

        $dateGo = Carbon::parse($attr['date']);
        $priceTicket = $dateGo->isWeekend() ? $priceWeekends : $priceWeekdays;
        $day = Carbon::parse(trim($attr['hotel_date_start']))->diffInDays(trim($attr['hotel_date_end']));
        $hotelPrice = 0;
        if (isset($attr['need_ticket']) && $attr['need_ticket'] == 'on') {
            $hotel = Hotel::find($attr['id_hotel']);
            $hotelPrice = ($hotel ? $hotel['price'] : 0) * $attr['hotel_room'] * $day;
        }
        $ticketPrice = isset($attr['need_ticket']) && $attr['need_ticket'] == 'on' ? $attr['person'] * $priceTicket : 0;
        $transportPrice = $attr['jeep'] * $attr['price_destination'];

        $amount = $hotelPrice + $ticketPrice + $transportPrice;
        // dd($attr, $hotelPrice);
        $transaction = [
            'code' => time(),
            'name' => $attr['name'],
            'email' => $attr['email'],
            'phone' => $attr['phone'],
            'whatsapp' => $attr['whatsapp'],
            'name' => $attr['name'],
            'start_date' => trim(Carbon::parse($attr['date'])->format('Y-m-d')),
            'end_date' => trim(Carbon::parse($attr['hotel_date_end'])->format('Y-m-d')),
            'person' => $attr['person'],
            'need_ticket' => isset($attr['need_ticket']) && $attr['need_ticket'] == 'on' ? true : false,
            'price_ticket' => $ticketPrice,
            'need_hotel' => isset($attr['need_hotel']) && $attr['need_hotel'] == 'on' ? true : false,
            'price_hotel' => $hotelPrice,
            'need_transport' => true,
            'price_transport' => $transportPrice,
            'id_destination' => $attr['id_destination'],
            'price_destination' => $transportPrice,
            'amount' => $amount,
            'status_transaction' => 'pending',
            'status' => 'ENABLE'
        ];

        // dd($transaction);

        DB::beginTransaction();
        try {
            $id = Transaction::insertGetId($transaction);
            $transaction_hotel = [
                'id_transaction' => $id,
                'id_hotel' => $attr['id_hotel'],
                'day' => $day,
                'room' => $attr['hotel_room'],
                'amount' => $hotelPrice,
                'start_date' => trim(Carbon::parse($attr['hotel_date_start'])->format('Y-m-d')),
                'end_date' => trim(Carbon::parse($attr['hotel_date_end'])->format('Y-m-d')),
            ];
            Transaction_hotel::insert($transaction_hotel);
            DB::commit();

            dispatch(new SendEmailOrderJob($transaction['code']));
            return redirect('/pesananku/' . $transaction['code']);
        } catch (\Exception $e) {
            DB::rollback();
            abort(422);
            // return redirect('/pesananku/gagal');
        }
    }

    public function finish($code)
    {
        $page_title = 'Pesananmu sedang diproses';
        $page_description = 'Bromokan Saja';

        $transaction = Transaction::where('code', $code)->first();

        if (!$transaction) {
            abort(404);
            // return redirect('/');
        }

        $transaction_destination = Destination::find($transaction['id_destination']);
        $transaction_hotel = Transaction_hotel::where('id_transaction', $transaction['id'])->first();
        $transaction_hotel_data = Hotel::find($transaction_hotel['id_hotel']);

        $priceWeekdays = Konfig::find(26)['value'];
        $priceWeekends = Konfig::find(27)['value'];
        $dateGo = Carbon::parse($transaction['start_date']);
        $priceTicket = $dateGo->isWeekend() ? $priceWeekends : $priceWeekdays;

        return view('page.order.finish', compact('page_title', 'page_description', 'transaction', 'transaction_destination', 'transaction_hotel', 'transaction_hotel_data', 'priceTicket'));
    }

    public function failed()
    {
        $page_title = 'Pesananmu gagal dibuat';
        $page_description = 'Bromokan Saja';

        return view('page.order.failed', compact('page_title', 'page_description'));
    }

    public function testMail($code)
    {
        // $pdf = SnappyPdf::loadView('pdf.invoice');
        // return $pdf->inline();
        // return view('pdf.invoice');
        // dd('');
        dispatch(new SendEmailOrderJob($code));

        // dd('success');
    }
}
