<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function login()
    {
        if (session('token')) {
            return redirect('/');
        }
        return view('page.auth.login');
    }

    public function attemptLogin(LoginRequest $request)
    {
        $login = HTTP::post(env('APP_URL') . '/api/login', [
            'username' => $request->username,
            'password' => $request->password,
        ]);

        if ($login->successful()) {
            $response = $login->json();
            session([
                'token' => $response['token'],
                'role' => $response['role'],
                'role_unit' => $response['role_unit']
            ]);

            return response([
                'redirectUrl' => url('/unit'),
                'token' => $response['token']
            ]);
        }

        return $login->json();
    }

    public function register()
    {
        if (session('token')) {
            return redirect('/');
        }
        return view('page.auth.register');
    }

    public function storeRegister(LoginRequest $request)
    {
        $register = HTTP::post(env('APP_URL') . '/api/register', [
            'username' => $request->username,
            'password' => $request->password,
            'password_confirmation' => $request->password,
        ]);

        if ($register->successful()) {
            $response = $register->json();
            session([
                'token' => $response['token'],
                'role' => $response['role'],
                'role_unit' => $response['role_unit']
            ]);

            return redirect('/');
        }

        return $register->json();
    }

    public function logout()
    {
        session(['token' => '']);
        session(['unit' => '']);

        return redirect('login');
    }

    public function unit()
    {
        if (session('unit')) {
            return redirect('/');
        }

        $unit = session('role_unit');

        return view('page.auth.unit', compact('unit'));
    }

    public function chooseUnit($unit)
    {
        session(['unit' => $unit]);

        return redirect('/');
    }
}
