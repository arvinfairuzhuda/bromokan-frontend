<?php

namespace App\Mail;

use App\Models\Destination;
use App\Models\Hotel;
use App\Models\Konfig;
use App\Models\Transaction;
use App\Models\Transaction_hotel;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;
    public $code;
    public $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $pdf)
    {
        $this->code = $code;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Invoice Bromokan')
            ->view('mails.invoice')
            ->attachData($this->pdf, 'invoice.pdf', [
                'mime' => 'application/pdf',
            ]);
    }
}
