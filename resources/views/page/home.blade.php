@extends('layout.default')
@section('styles')
<link href="{{asset('css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!--begin::Home-->
<div class="row mt-20 mt-lg-30">
    <div class="col-lg-6">
        <p class="text-center text-md-left"
            style="font-size: 42px; line-height: 63px; font-weight: bold; color: #152C5B;">
            Tempat Mana Yang <br>
            Ingin Kamu Kunjungi Di <br>
            Bromo?
        </p>
        <small class="text-center text-md-left" style="font-weight: 300; font-size: 16px; line-height: 170%;">
            Perjalananmu lebih mudah, aman dan nyaman bersama Bromokan. <br>
            Media pemesanan Jeep dan homestay dalam sekali sentuh <br>
            dan akan menghubungkan kamu dengan <br>
            destinasi wisata yang ingin kamu kunjungi. <br>
        </small>
        <h1 class="text-primary font-weight-bolder mt-10">
            #BROMOKANSAJA
        </h1>
        <div class="mt-10">
            <a href="{{route('kebutuhanmu.index')}}" class="btn btn-lg btn-secondary" style="font-size: 1.38rem">Pesan
                Sekarang</a>
        </div>
    </div>
    <div class="col-lg-6">
        <img src="{{asset('/media/bg/home-1.png')}}" class="d-none d-md-block w-100">
    </div>
</div>
<!--end::Home-->
<!--end::Entry-->
@endsection