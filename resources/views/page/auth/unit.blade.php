@extends('layout.default')
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container-fluid bg-white">
        <div class="row pt-12" style="width: 100%">
            <div class="col-12 text-center mb-15">
                <h1 style="font-size: 24pt">ePlus DAPM Aplikasi BUMDESMA Kabupaten Ngawi</h1>
            </div>
            @if (collect($unit)->contains('spp'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 7.000.542</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_11_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/spp')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #168BF2; background-color: #168BF2; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-3.svg')}});">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit SPP</h2>
                                <p class="text-inverse-danger my-6">Kegiatan SPP (Simpan Pinjam khusus Perempuan)</p>
                                <a href="{{url('/chooseUnit/spp')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('uep'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/uep')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #04BF8A; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-1.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit UEP</h2>
                                <p class="text-inverse-danger my-6">Unit Ekonomi Produktif</p>
                                <a href="{{url('/chooseUnit/uep')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 4.231.542</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_7_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('kum'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 2.234.542</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_8_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/kum')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #F2B705; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-2.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit KUM</h2>
                                <p class="text-inverse-danger my-6">Karya Usaha Mandiri</p>
                                <a href="{{url('/chooseUnit/kum')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('bisnis_sosial'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/bisnis_sosial')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #D92323; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-4.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit Bisnis Sosial</h2>
                                <p class="text-inverse-danger my-6">Usaha air minum desa, usaha listrik desa dan lumbung
                                    pangan</p>
                                <a href="{{url('/chooseUnit/bisnis_sosial')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 5.231.232</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_9_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('persewaan'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 2.234.524</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_10_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/persewaan')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #168BF2; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-5.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit Persewaan</h2>
                                <p class="text-inverse-danger my-6">Usaha alat transportasi, perkakas pesta, gedung
                                    pertemuan, rumah toko dan tanah milik BUMDes</p>
                                <a href="{{url('/chooseUnit/persewaan')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('perantara'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/perantara')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #04BF8A; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-6.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit Perantara</h2>
                                <p class="text-inverse-danger my-6">(Broker) jasa pembayaran listrik dan pasar desa</p>
                                <a href="{{url('/chooseUnit/perantara')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 7.535.8678</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_12_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('dagang'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 4.654.656</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_1_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/dagang')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #F2B705; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-7.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit Dagang</h2>
                                <p class="text-inverse-danger my-6">Pabrik es, pabrik asap cair, hasil pertanian, sarana
                                    produksi pertanian, dan sumur bekas tambang</p>
                                <a href="{{url('/chooseUnit/dagang')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('usaha'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/usaha')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #D92323; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-8.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Unit Usaha</h2>
                                <p class="text-inverse-danger my-6">(Holding) unit-unit usaha yang dikembangkan melalui
                                    pengembangan kapal desa dan desa wisata</p>
                                <a href="{{url('/chooseUnit/usaha')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 1.454.882</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_2_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                </div>
            </div>
            @endif

            @if (collect($unit)->contains('holding'))
            <div class="col-md-12 d-flex flex-column">
                <div class="row">
                    <div class="col-9">
                        <!--begin::Stats Widget 11-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <span class="symbol symbol-50 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <span class="svg-icon svg-icon-xl svg-icon-success">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <rect fill="#000000" x="4" y="4" width="7" height="7"
                                                            rx="1.5" />
                                                        <path
                                                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                            fill="#000000" opacity="0.3" />
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                    </span>
                                    <div class="d-flex flex-column text-right">
                                        <span class="text-dark-75 font-weight-bolder font-size-h3">Rp 4.556.542</span>
                                        <span class="text-muted font-weight-bold mt-2">Laba Rugi</span>
                                    </div>
                                </div>
                                <div id="kt_stats_widget_3_chart" class="card-rounded-bottom" data-color="success"
                                    style="height: 150px"></div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Stats Widget 11-->
                    </div>
                    <div class="col-3">
                        <a href="{{url('/chooseUnit/laporan_keuangan')}}">
                            <div class="flex-grow-1 p-8 rounded-xl flex-grow-1 bgi-no-repeat"
                                style="height: 90%; background-color: #168BF2; background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url({{asset('media/svg/humans/custom-9.svg')}})">
                                <h2 class="text-inverse-danger mt-2 font-weight-bolder">Laporan Keuangan</h2>
                                <p class="text-inverse-danger my-6">Laporan Keuangan Keseluruhan Unit Usaha BUMDesMA
                                    DAPM Kabupaten Ngawi</p>
                                <a href="{{url('/chooseUnit/laporan_keuangan')}}"
                                    class="btn btn-primary font-weight-bold py-2 px-6">Masuk</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@section('aside-secondary')
@include('layout._aside-third')
@endsection