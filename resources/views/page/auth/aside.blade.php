<!--begin::Aside-->
<div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
    <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom"
        style="background-image: url({{ asset('media/svg/illustrations/login-visual-4.svg') }});">
        <!--begin::Aside title-->
        <h3
            class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
            Aplikasi
            <br> e+DAPM
            <br />Kabupaten Ngawi
        </h3>
        <!--end::Aside title-->
    </div>
</div>
<!--end::Aside-->