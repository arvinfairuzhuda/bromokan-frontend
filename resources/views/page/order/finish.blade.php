@extends('layout.default')
@section('styles')
<link href="{{asset('css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />

<style>
    .tooltip {
        position: relative;
        display: inline-block;
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 170px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 8px 6px;
        position: absolute;
        z-index: 1;
        bottom: 150%;
        left: 50%;
        margin-left: -86px;
        opacity: 0;
        transition: opacity 0.3s;
        white-space: nowrap;
    }

    .tooltip .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
@endsection
@section('content')
<!--begin::Home-->
<div class="row mt-20 mt-lg-30">
    <div class="col-12 col-lg-8">
        <h3 class="mb-10 font-weight-bold text-dark">
            Pesanan kamu sedang diproses yaa, silahkan tunggu dan admin akan menghubungi kamu!
        </h3>
    </div>
    <div class="col-12 col-lg-4">
        <div class="float-right">
            <h5 class="font-weight-bold text-dark">
                Kode Pesananmu
            </h5>
            <h1 class="text-primary font-weight-bolder">
                {{$transaction['code']}}
            </h1>
            {{-- <input type="hidden" id="invoice_code" value="{{$transaction['code']}}">
            <div class="tooltip">
                <button class="btn btn-primary" onclick="myFunction()" onmouseout="outFunc()">
                    <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                    Copy text
                </button>
            </div> --}}
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row justify-content-center pt-8 pt-md-27">
            <div class="col-md-12">
                <!-- begin: Invoice header-->
                <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                    <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
                    <div class="d-flex flex-column align-items-md-end px-0">
                        <!--begin::Logo-->
                        <p class="mb-5 max-w-200px">
                            Bromokan
                        </p>
                        <!--end::Logo-->
                        <span class="d-flex flex-column align-items-md-end font-size-h5 font-weight-bold text-muted">
                            <span>Desa Wonokitri, Pintu Masuk Bromo via
                                Nongkojajar</span>
                            <span>Pasuruan, Jawa Timur, Indonesia</span>
                        </span>
                    </div>
                </div>
                <div class="rounded-xl overflow-hidden w-100 max-h-md-250px mb-15 mb-md-30">
                    <img src="{{asset('media/bg/bg-invoice-5.jpg')}}" class="w-100" alt="" />
                </div>
                <!--end: Invoice header-->
                <!--begin: Invoice body-->
                <div class="row border-bottom pb-10">
                    <div class="col-md-9 py-md-10 pr-md-10">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th
                                            class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                            Perlengkapan</th>
                                        <th
                                            class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                            Jumlah</th>
                                        <th
                                            class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                            Hari</th>
                                        <th
                                            class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                            Harga</th>
                                        <th
                                            class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                            Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="font-weight-bolder font-size-lg">
                                        <td class="border-top-0 pl-0 d-flex align-items-center">
                                            <span class="navi-icon mr-2 rounded-circle bg-warning p-3">
                                                <i class="la la-map-marked-alt
                                                    icon-2x text-light"></i>
                                            </span>
                                            <div>
                                                <div id="name_destination" class="mr-2">
                                                    {{$transaction_destination['name']}}
                                                </div>
                                                <small>
                                                    (+ Jeep)
                                                </small>
                                            </div>
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="jeep_count_total">{{ceil($transaction['person']/5)}}</span>
                                            Kendaraan
                                        </td>
                                        <td class="text-right pt-7">1 Hari</td>
                                        <td class="text-right pt-7">
                                            <div id="price_destination_label">
                                                IDR {{number_format($transaction_destination['price'], 0, '.', '.')}}
                                            </div>
                                        </td>
                                        <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                            <div id="jeep_price_total">
                                                IDR {{number_format($transaction['price_destination'], 0, '.', '.')}}
                                            </div>
                                        </td>
                                    </tr>
                                    @if ($transaction['need_hotel'])
                                    <tr class="font-weight-bolder font-size-lg" id="review_penginapan">
                                        <td class="border-top-0 pl-0 d-flex align-items-center">
                                            <span class="navi-icon mr-2 rounded-circle bg-success p-3">
                                                <i class="la la-home
                                                    icon-2x text-light"></i>
                                            </span>
                                            <span id="nama_hotel_label">{{$transaction_hotel_data['name']}}</span>
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="hotel_room_label">{{$transaction_hotel['room']}}</span> Kamar
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="hotel_day_review_label">{{$transaction_hotel['day']}}</span> Malam
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="harga_hotel_review_label">IDR
                                                {{number_format($transaction_hotel_data['price'], 0, '.', '.')}}</span>
                                        </td>
                                        <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                            <span id="total_hotel_review_label">IDR
                                                {{number_format($transaction_hotel['amount'], 0, '.', '.')}}</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @if ($transaction['need_ticket'])
                                    <tr class="font-weight-bolder font-size-lg" id="review_ticket">
                                        <td class="border-top-0 pl-0 d-flex align-items-center">
                                            <span class="navi-icon mr-2 rounded-circle bg-primary p-3">
                                                <i class="la la-ticket-alt
                                                    icon-2x text-light"></i>
                                            </span>
                                            Tiket Masuk Bromo
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="ticket_count_label">{{$transaction['person']}}</span>
                                            Tiket</td>
                                        <td class="text-right pt-7">
                                            -
                                        </td>
                                        <td class="text-right pt-7">
                                            <span id="harga_ticket_review_label">IDR
                                                {{number_format($priceTicket, 0, '.', '.')}}</span>
                                        </td>
                                        <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                            <span id="total_ticket_review_label">IDR
                                                {{number_format($transaction['price_ticket'], 0, '.', '.')}}</span>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="border-bottom w-100 mt-7 mb-13"></div>
                        <div class="d-flex flex-column flex-md-row">
                            <div class="d-flex flex-column mb-10 mb-md-0">
                                <div class="font-weight-bold font-size-h6 mb-3">
                                    INVOICE MILIK</div>
                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                    <span class="font-weight-bold mr-15">Nama
                                        Lengkap:</span>
                                    <span class="text-right" id="invoice_name">{{$transaction['name']}}</span>
                                </div>
                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                    <span class="font-weight-bold mr-15">Email:</span>
                                    <span class="text-right" id="invoice_email">{{$transaction['email']}}</span>
                                </div>
                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                    <span class="font-weight-bold mr-15">No
                                        Telepon:</span>
                                    <span class="text-right" id="invoice_phone">{{$transaction['phone']}}</span>
                                </div>
                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                    <span class="font-weight-bold mr-15">No
                                        Whatsapp:</span>
                                    <span class="text-right" id="invoice_whatsapp">{{$transaction['whatsapp']}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 border-left-md pl-md-10 py-md-10 text-right">
                        <!--begin::Total Amount-->
                        <div class="font-size-h4 font-weight-bolder text-muted mb-3">
                            TOTAL HARGA</div>
                        <div class="font-size-h1 font-weight-boldest" id="total_amount">
                            IDR {{number_format($transaction['amount'], 0, '.', '.')}}
                        </div>
                        <div class="text-muted font-weight-bold mb-16"></div>
                        <!--end::Total Amount-->
                        <div class="border-bottom w-100 mb-16"></div>
                        <!--begin::Invoice To-->
                        <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                            KEPADA</div>
                        <div class="font-size-lg font-weight-bold mb-10" id="kepada">{{$transaction['name']}}</div>
                        <!--end::Invoice To-->
                        <!--begin::Invoice No-->
                        <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                            NO INVOICE</div>
                        <div class="font-size-lg font-weight-bold mb-10">{{$transaction['code']}}</div>
                        <!--end::Invoice No-->
                        <!--begin::Invoice Date-->
                        <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                            TANGGAL PESAN</div>
                        <div class="font-size-lg font-weight-bold">
                            {{ Carbon\Carbon::parse($transaction['created_at'])->isoFormat('dddd, DD MMMM YYYY hh:mm:ss') }}
                        </div>
                        <!--end::Invoice Date-->
                    </div>
                </div>
                <!--end: Invoice body-->
            </div>
        </div>
    </div>
</div>
<!--end::Home-->
<!--end::Entry-->
@endsection

@section('js')
<script>
    function myFunction() {
        var copyText = document.getElementById("invoice_code");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");

        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied: " + copyText.value;
    }

    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy to clipboard";
    }
</script>
@endsection