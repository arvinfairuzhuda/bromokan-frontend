<div class="modal-body">
    <div data-scroll="true" data-height="300">
        <div class="container py-10 px-5">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <h3 class="text-center">{{$wisata['name']}}</h3>
                        <small>Wonokitri, Pasuruan, Jawa Timur</small>
                    </div>
                </div>
                @if ($wisata['photo'])
                <div class="col-12 mt-10">
                    <!--Carousel Wrapper-->
                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails"
                        data-ride="carousel">
                        <div class="carousel-inner mx-auto w-100 w-lg-80" role="listbox">
                            @foreach ($wisata['photo'] as $i => $ph)
                            <div class="carousel-item {{$i == 0 ? 'active' : ''}}">
                                <img class="d-block w-100" src="{{asset(env('BACKEND_URL') . $ph['dir'])}}" alt="slide">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                    <div class="mt-2">
                        <ol class="carousel-indicators" style="position: relative">
                            @foreach ($wisata['photo'] as $i => $ph)
                            <li data-target="#carousel-thumb" data-slide-to="{{$i}}" class="active"
                                style="height: auto; width: 12rem; border: none;">
                                <img class="d-block w-100 object-fit-cover"
                                    src="{{asset(env('BACKEND_URL') . $ph['dir'])}}" class="img-fluid">
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
                @endif
                <div class="col-sm-12 mt-10 mt-sm-18">
                    <h5>Deskripsi Tempat</h5>
                    <p class="text-dark-65">
                        {!!$wisata['description']!!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-primary btn-lg font-weight-bold">Pilih</button>
</div>

@section('js')
<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js')}}"></script>
@endsection