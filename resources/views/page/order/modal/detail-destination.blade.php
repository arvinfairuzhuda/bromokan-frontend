<div class="modal-body">
    <div data-scroll="true" data-height="300">
        <div class="container py-10 px-5">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <h3 class="text-center">{{$destination['name']}}</h3>
                        <small>Wonokitri, Pasuruan, Jawa Timur</small>
                    </div>
                </div>
                <div class="col-sm-12 mt-10 mt-sm-18 row-scrollable">
                    <div class="row">
                        @foreach ($wisata as $wis)
                        <div class="col-9 col-lg-5 my-5">
                            <!--begin::Card-->
                            <div class="card card-custom card-shadowless">
                                <div class="card-body p-0">
                                    <!--begin::Image-->
                                    <div class="overlay">
                                        <div class="overlay-wrapper rounded bg-light text-center">
                                            <img src="{{ $wis['photo'] ? asset(env('BACKEND_URL') . $wis['photo']['dir']) : ''}}"
                                                alt="" class="w-100 rounded" />
                                        </div>
                                        <div class="overlay-layer d-none d-md-flex">
                                            <button type="button" onclick="modalWisata({{$wis['id']}})"
                                                class="btn font-weight-bolder btn-md btn-primary mr-2">Lihat</button>
                                        </div>
                                    </div>
                                    <!--end::Image-->
                                    <!--begin::Details-->
                                    <div
                                        class="text-center mt-5 mb-md-0 mb-lg-5 mb-md-0 mb-lg-5 mb-lg-0 mb-5 d-flex flex-column">
                                        <a onclick="modalWisata({{$wis['id']}})"
                                            class="font-size-h5 font-weight-bolder text-dark-75 text-hover-primary mb-1">
                                            {{$wis['name']}}
                                        </a>
                                        <span class="font-size-lg">Desa
                                            Wonokitri</span>
                                    </div>
                                    <!--end::Details-->
                                </div>
                            </div>
                            <!--end::Card-->
                        </div>
                        @endforeach
                    </div>
                    <h5>Deskripsi Tempat</h5>
                    <p class="text-dark-65">
                        {!!$destination['description']!!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn btn-primary btn-lg font-weight-bold btn-destination"
        id="destination-{{$destination['id']}}">Pilih</button>
</div>

<script>
    $(".btn-destination").on('click', function(){
        var id = $(this).attr('id').substr(12);
        $("#modal-form").modal('hide');
        $('.card-destination').removeClass("shadow border border-primary")
        $('#card-destination-' + id).addClass("shadow border border-primary")
        $('#id_destination').val(id);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{url("/ajax/getDestinationById/")}}' + '/' + id,
            dataType: 'json',
        success: function (data) {
            $('#name_destination').html(data.name);
            $('#price_destination').val(data.price);
            $('#price_destination_label').html(data.price);
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            totalAmount()
        },error:function($e){
            console.log($e);
        }})
    })
</script>