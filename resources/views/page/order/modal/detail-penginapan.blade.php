<div class="modal-body">
    <div data-scroll="true" data-height="300">
        <div class="container py-10 px-5">
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                        <h3 class="text-center">{{$hotel['name']}}</h3>
                        <small>Wonokitri, Pasuruan, Jawa Timur</small>
                    </div>
                </div>
                @if ($hotel['photo'])
                <div class="col-12 mt-10">
                    <!--Carousel Wrapper-->
                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails"
                        data-ride="carousel">
                        <div class="carousel-inner mx-auto w-100 w-lg-80" role="listbox">
                            @foreach ($hotel['photo'] as $i => $ph)
                            <div class="carousel-item {{$i == 0 ? 'active' : ''}}">
                                <img class="d-block w-100" src="{{asset(env('BACKEND_URL') . $ph['dir'])}}" alt="slide">
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!--/.Slides-->
                    <!--Controls-->
                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!--/.Controls-->
                    <div class="mt-2">
                        <ol class="carousel-indicators" style="position: relative">
                            @foreach ($hotel['photo'] as $i => $ph)
                            <li data-target="#carousel-thumb" data-slide-to="{{$i}}" class="active"
                                style="height: auto; width: 12rem; border: none;">
                                <img class="d-block w-100 object-fit-cover"
                                    src="{{asset(env('BACKEND_URL') . $ph['dir'])}}" class="img-fluid">
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
                @endif
                <div class="col-sm-7 mt-10 mt-sm-18">
                    <h5>Deskripsi Tempat</h5>
                    <p class="text-dark-65">
                        {!! $hotel['services'] !!}
                    </p>
                </div>
                <div class="col-sm-5 mt-10 mt-sm-18">
                    <div class="card">
                        <div class="card-body">
                            <div class="p-3">
                                <h5>Harga</h5>
                                <h1>IDR {{ $hotel['price'] }} <small class="text-muted">per Malam</small></h1>
                                <div class="mt-5">
                                    Ketentuan :
                                    <ul>
                                        <li>Checkin dimulai pukul 12.00 siang sesuai tanggal awal menginap</li>
                                        <li>Batas terakhir checkout pukul 12.00 siang sesuai tanggal terakhir menginap
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
    <button type="button" class="btn font-weight-bolder btn-lg btn-primary btn-pilih-hotel"
        id="hotel-{{$hotel['id']}}">Pilih</button>
</div>

<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js')}}"></script>
<script>
    $('#hotel_date').val($('#date_go .form-control').val());
    $('#hotel_day').html(datediff(parseDate($('#date_go .form-control').val().substr(0, 10)), parseDate($('#date_go .form-control').val().substr(13))));

    $('#hotel_price').html('IDR ' + (parseInt('{{$hotel["price"]}}') * datediff(parseDate($('#date_go .form-control').val().substr(0, 10)), parseDate($('#date_go .form-control').val().substr(13)))));

    $(".btn-pilih-hotel").on('click', function(){
        var hotel_terpilih = document.getElementById("hotel-terpilih");
        hotel_terpilih.style.display = "flex";

        $("#modal-form").modal('hide');
        var id = $(this).attr('id').substr(6);
        console.log(id);
        var target = $('#card-hotel-' + id);
        console.log(target)
        $('.hotel-terpilih').empty();
        target.clone(true, true).appendTo(".hotel-terpilih");
        $('#id_hotel').val(id);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{url("/ajax/getHotelById/")}}' + '/' + id,
            dataType: 'json',
        success: function (data) {
            $('#nama_hotel').val(data.name);
            $('#nama_hotel_label').html(data.name);
            $('#harga_hotel').val(data.price);
            $('#harga_hotel_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#harga_hotel_review_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            totalAmount()
        },error:function($e){
            console.log($e);
        }})
    })
</script>