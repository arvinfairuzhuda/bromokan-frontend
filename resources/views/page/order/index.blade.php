@extends('layout.default')
@section('styles')
<link href="{{asset('css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!--begin::Order-->
<div class="row mt-10">
    <div class="col-12">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Kami punya beberapa paket yang menarik nih yang lebih hemat dan nyaman.</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @if (count($packages) > 0)
                    @foreach ($packages as $p)
                    <div class="col-6 col-md-4 my-5">
                        <!--begin::Card-->
                        <div class="card card-custom card-shadowless" id="card-p-{{$p['id']}}">
                            <div class="card-body p-0">
                                <!--begin::Image-->
                                <div class="overlay">
                                    <div class="overlay-wrapper rounded bg-light text-center">
                                        <img src="{{ $p['photo'] ? asset(env('BACKEND_URL') . $p['photo']['dir']) : ''}}"
                                            alt="" class="mw-100 object-fit-cover" />
                                    </div>
                                    <div class="overlay-layer d-none d-md-flex">
                                        <button type="button" href="#"
                                            class="btn font-weight-bolder btn-lg btn-primary mr-2">Lihat</button>
                                    </div>
                                </div>
                                <!--end::Image-->
                                <!--begin::Details-->
                                <div
                                    class="text-center mt-5 mb-md-0 mb-lg-5 mb-md-0 mb-lg-5 mb-lg-0 mb-5 d-flex flex-column">
                                    <a href="#"
                                        class="font-size-h5 text-dark-75 text-hover-primary mb-1">{{$p['name']}}</a>
                                    <span class="font-size-h4 font-weight-bolder">IDR {{$p['amount']}}</span>
                                </div>
                                <!--end::Details-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                    @endforeach
                    @else
                    <div class="col-12 min-h-250px d-flex justify-content-center align-items-center">
                        <h5 class="text-muted">Belum ada paket yang tersedia</h5>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card card-custom mt-10">
            <div class="card-body" style="display: flex; justify-content: space-between; align-items: center">
                <h5 class="card-label">Tidak cocok dengan paket kami? Kamu bisa sesuaikan kebutuhanmu sendiri loo! </h5>
                <a href="{{route('order.custom')}}" class="btn btn-md btn-primary">Sesuaikan</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true" id="modal-form">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
        <div class="modal-content" id="load-form">
        </div>
    </div>
</div>
<!--end::Modal-->

<!--end::Order-->
<!--end::Entry-->
@endsection