@extends('layout.default')
@section('styles')
<link href="{{asset('css/pages/wizard/wizard-1.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!--begin::Order-->
<div class="row mt-10">
    <div class="col-12">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Pilih sesuai yang kamu butuhkan yaa!</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <!--begin::Wizard-->
                        <div class="wizard wizard-1" id="kt_wizard" data-wizard-state="step-first"
                            data-wizard-clickable="false">
                            <!--begin::Wizard Nav-->
                            <div class="wizard-nav border-bottom">
                                <div class="wizard-steps p-8 p-lg-10">
                                    <!--begin::Intro Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-calendar-1"></i>
                                            <h3 class="wizard-title">Tanggal</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Intro Nav-->
                                    <!--begin::Destination Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-map-location"></i>
                                            <h3 class="wizard-title">Destinasi</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Destination Nav-->
                                    <!--begin::Kendaraan Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <i class="wizard-icon la la-truck-monster d-md-none"></i>
                                            <i class="wizard-icon la la-truck-monster d-none d-md-block"
                                                style="font-size: 5rem"></i>
                                            <h3 class="wizard-title">Kendaraan</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Kendaraan Nav-->
                                    <!--begin::Penginapan Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon2-architecture-and-city"></i>
                                            <h3 class="wizard-title">Penginapan</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Penginapan Nav-->
                                    <!--begin::Biodata Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-list"></i>
                                            <h3 class="wizard-title">Data Diri</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Biodata Nav-->
                                    <!--begin::Review Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <i class="wizard-icon flaticon-globe"></i>
                                            <h3 class="wizard-title">Pesananmu</h3>
                                        </div>
                                        <span class="svg-icon svg-icon-xl wizard-arrow last">
                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
                                                viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <rect fill="#000000" opacity="0.3"
                                                        transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                        x="11" y="5" width="2" height="14" rx="1" />
                                                    <path
                                                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                        fill="#000000" fill-rule="nonzero"
                                                        transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Review Nav-->
                                </div>
                            </div>
                            <!--end::Wizard Nav-->
                            <!--begin::Wizard Body-->
                            <div class="row justify-content-center my-10 px-0 my-lg-15 px-lg-10">
                                <div class="col-xl-12 col-xxl-10">
                                    <!--begin::Wizard Form-->
                                    <form class="form" id="kt_form" action="{{route('kebutuhanmu.store')}}"
                                        method="POST">
                                        @csrf
                                        <input type="hidden" name="jeep" id="jeep">
                                        <!--begin::Intro-->
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <h3 class="mb-10 font-weight-bold text-dark">Jadi kapan kamu mau berkunjung
                                                ke bromo ?
                                            </h3>
                                            <!--begin::Input-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-12">Berangkat tanggal ?</label>
                                                        <div class="col-12">
                                                            <div class="input-group" id="date_go">
                                                                <input type="text" class="form-control"
                                                                    value="{{now()->format('d-m-Y')}}" name="date"
                                                                    id="date" readonly>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label class="col-form-label col-12">Kamu berangkat berapa orang
                                                            nih ?</label>
                                                        <div class="col-12">
                                                            <div class="input-group date">
                                                                <input class="form-control" id="person" name="person"
                                                                    type="text" value="1" />
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-users"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                            <h3 class="mt-10 mb-10 font-weight-bold text-dark">
                                                Butuh kami sediakan tiket juga ?
                                            </h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="checkbox-inline">
                                                            <label class="checkbox checkbox-success checkbox-lg">
                                                                <input type="checkbox" name="need_ticket"
                                                                    id="need_ticket" onclick="needTicket()">
                                                                <span></span>Ya, tolong belikan juga.</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Intro-->
                                        <!--begin::Destination-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="form-group">
                                                <input type="hidden" name="id_destination" id="id_destination" />
                                                <input type="hidden" name="price_destination" id="price_destination" />
                                            </div>
                                            <div id="destination_disable" style="display: none">
                                                <h3 class="mb-10 font-weight-bold text-dark">Karena kamu tidak memesan
                                                    kendaraan, untuk pilihan destinasi kamu tidak ada</h3>
                                            </div>
                                            <div id="destination_enable" style="display: block">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h3 class="mb-10 font-weight-bold text-dark">Kamu mau
                                                            mengunjungi wisata apa aja nih di Bromo ?</h3>
                                                    </div>
                                                    @foreach ($destinations as $des)
                                                    <div class="col-12 my-3 p-0">
                                                        <div class="card card-custom card-destination shadow-sm"
                                                            id="card-destination-{{$des['id']}}"
                                                            onclick="modalDestination({{$des['id']}})">
                                                            <div class="card-header text-dark">
                                                                <div class="card-title">
                                                                    <div class="row">
                                                                        <div class="col-12 my-3">
                                                                            <h4>{{$des['name']}}</h4>
                                                                        </div>
                                                                        <div class="col-12">
                                                                            <h3 class="text-primary">IDR
                                                                                {{$des['price']}}
                                                                                <small class="text-muted">Sekali
                                                                                    berangkat</small>
                                                                            </h3>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card-toolbar d-none d-lg-flex">
                                                                    <a onclick="modalDestination({{$des['id']}})"
                                                                        class="btn btn-warning float-right mr-3">
                                                                        Detail
                                                                    </a>
                                                                    <button type="button"
                                                                        class="btn btn-primary float-right btn-destination"
                                                                        id="destination-{{$des['id']}}">Pilih</button>
                                                                </div>
                                                            </div>
                                                            <div class="card-body row-scrollable">
                                                                <div class="row">
                                                                    @foreach ($des['wisata'] as $wis)
                                                                    <div class="col-9 col-lg-5 my-5">
                                                                        <!--begin::Card-->
                                                                        <div class="card card-custom card-shadowless">
                                                                            <div class="card-body p-0">
                                                                                <!--begin::Image-->
                                                                                <div class="overlay w-100"
                                                                                    onclick="modalWisata({{$wis['id']}})">
                                                                                    <div
                                                                                        class="overlay-wrapper rounded bg-light text-center">
                                                                                        <img src="{{ $wis['photo'] ? asset(env('BACKEND_URL') . $wis['photo']['dir']) : ''}}"
                                                                                            alt="wisata" class="w-100"
                                                                                            style="object-fit: cover" />
                                                                                    </div>
                                                                                    <div
                                                                                        class="overlay-layer d-none d-md-flex">
                                                                                        <button type="button"
                                                                                            onclick="modalWisata({{$wis['id']}})"
                                                                                            class="btn font-weight-bolder btn-md btn-primary mr-2">Lihat</button>
                                                                                    </div>
                                                                                </div>
                                                                                {{-- <div class="w-100">
                                                                                <img src="{{ $wis['photo'] ? asset(env('BACKEND_URL') . $wis['photo']['dir']) : ''}}"
                                                                                alt="wisata" class="w-75"
                                                                                style="object-fit: cover" />
                                                                            </div> --}}
                                                                            <!--end::Image-->
                                                                            <!--begin::Details-->
                                                                            <div
                                                                                class="text-center mt-5 mb-md-0 mb-lg-5 mb-md-0 mb-lg-5 mb-lg-0 mb-5 d-flex flex-column">
                                                                                <a onclick="modalWisata({{$wis['id']}})"
                                                                                    class="font-size-h5 font-weight-bolder text-dark-75 text-hover-primary mb-1">
                                                                                    {{$wis['name']}}
                                                                                </a>
                                                                                <span class="font-size-lg">Desa
                                                                                    Wonokitri</span>
                                                                            </div>
                                                                            <!--end::Details-->
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Card-->
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                </div>
                                <!--end::Destination-->
                                <!--begin::Kendaraan-->
                                <div class="pb-5" data-wizard-type="step-content">
                                    <h4 class="mb-10 font-weight-bold text-dark">Sesuai dengan jumlah orang dan
                                        lamanya kamu di Bromo,
                                        kendaraan yang kamu butuhkan
                                    </h4>
                                    <div class="row">
                                        <div class="col-md-5 d-flex justify-content-center align-items-center">
                                            <img src="{{asset('media/kendaraan/jeep-1.png')}}" class="img-fluid"
                                                alt="Jeep">
                                        </div>
                                        <div class="col-md-7 d-flex justify-content-center align-items-center">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="p-3">
                                                        <h1><span id="jeep_count"></span> Hardtop Jeep</h1>
                                                        <h6><span id="jeep_price_span"></span> <small
                                                                class="text-muted">per
                                                                Hardtop Jeep / max. 5 orang</small></h6>
                                                        <p class="mt-5">Tanggal yang kamu pilih</p>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control" id="jeep_date"
                                                                disabled>
                                                        </div>
                                                        <p class="mt-5">Jumlah orang yang berangkat</p>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="la la-users"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control" id="jeep_person"
                                                                disabled>
                                                        </div>
                                                        <div class="text-dark-65 mt-5">
                                                            <p class="mb-0">Total harga kendaraanmu untuk
                                                                <span id="jeep_person_span"></span> menjadi
                                                            </p>
                                                            <h3 class="font-weight-bold text-success mt-3"
                                                                id="jeep_price"></h3>
                                                        </div>
                                                        {{-- <div class="row mt-10">
                                                                    <div class="col-12">
                                                                        <p class="font-weight-bold text-dark d-inline">
                                                                            Apa kamu mau memesan kendaraan ini juga ?
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="checkbox-inline mt-5">
                                                                            <label
                                                                                class="checkbox checkbox-success checkbox-lg">
                                                                                <input type="checkbox"
                                                                                    name="need_transport"
                                                                                    id="need_transport" checked
                                                                                    onclick="needTransport()">
                                                                                <span></span>Ya, saya butuh
                                                                                kendaraan.</label>
                                                                        </div>
                                                                    </div>
                                                                </div> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Kendaraan-->
                                <!--begin::Penginapan-->
                                <div class="pb-5" data-wizard-type="step-content">
                                    <div class="form-group row">
                                        <input type="hidden" name="id_hotel" id="id_hotel" />
                                        <input type="hidden" name="nama_hotel" id="nama_hotel">
                                        <input type="hidden" name="harga_hotel" id="harga_hotel">
                                        <input type="hidden" name="hotel_day" id="hotel_day">
                                        <input type="hidden" name="total_hotel" id="total_hotel">
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h3 class="font-weight-bold text-dark d-inline">Apakah kamu butuh
                                                penginapan
                                                ?
                                            </h3>
                                            <p class="font-weight-bold text-danger d-inline">*Jika tidak, lewati
                                                saja
                                                tahap
                                                ini ya.</p>
                                        </div>
                                        <div class="col-12">
                                            <div class="checkbox-inline mt-5">
                                                <label class="checkbox checkbox-success checkbox-lg">
                                                    <input type="checkbox" name="need_hotel" id="need_hotel"
                                                        onclick="needHotel()">
                                                    <span></span>Ya, saya butuh penginapan.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-20" id="list_penginapan" style="display: none">
                                        <hr>
                                        <h3 class="mb-10 font-weight-bold text-dark">Kamu mau menginap dimana
                                            nih ?
                                        </h3>
                                        <div class="row">
                                            @foreach ($hotels as $i => $hotel)
                                            <!--begin::Penginapan-->
                                            <div class="col-12 col-md-4 my-5">
                                                <!--begin::Card-->
                                                <div class="card card-custom card-shadowless"
                                                    id="card-hotel-{{$hotel['id']}}">
                                                    <div class="card-body p-0">
                                                        <!--begin::Image-->
                                                        <div class="overlay"
                                                            onclick="modalPenginapan({{$hotel['id']}})">
                                                            <div class="overlay-wrapper rounded bg-light text-center">
                                                                <img src="{{ $hotel['photo'] ? asset(env('BACKEND_URL') . $hotel['photo']['dir']) : ''}}"
                                                                    alt="" class="mw-100 object-fit-cover" />
                                                            </div>
                                                            <div class="overlay-layer d-none d-md-flex">
                                                                <button type="button"
                                                                    onclick="modalPenginapan({{$hotel['id']}})"
                                                                    class="btn font-weight-bolder btn-md btn-primary mr-2">Lihat</button>
                                                                <button type="button"
                                                                    class="btn font-weight-bolder btn-md btn-light-primary btn-pilih-hotel"
                                                                    id="hotel-{{$hotel['id']}}">Pilih</button>
                                                            </div>
                                                        </div>
                                                        <!--end::Image-->
                                                        <!--begin::Details-->
                                                        <div
                                                            class="text-center mt-5 mb-md-0 mb-lg-5 mb-md-0 mb-lg-5 mb-lg-0 mb-5 d-flex flex-column">
                                                            <a onclick="modalPenginapan({{$hotel['id']}})"
                                                                class="font-size-h5 font-weight-bolder text-dark-75 text-hover-primary mb-1">{{$hotel['name']}}</a>
                                                            <span class="font-size-lg">Desa Wonokitri</span>
                                                        </div>
                                                        <!--end::Details-->
                                                    </div>
                                                </div>
                                                <!--end::Card-->
                                            </div>
                                            <!--end::Penginapan-->
                                            @endforeach
                                        </div>
                                        <div class="row mt-15" id="hotel-terpilih" style="display: none">
                                            <div class="col-12">
                                                <hr>
                                                <h3 class="my-8 font-weight-bold text-dark">Hotel yang kamu
                                                    pilih
                                                </h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="hotel-terpilih">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-form-label col-12">Berapa kamar yang mau
                                                        kamu sewa ?</label>
                                                    <div class="col-12">
                                                        <div class="input-group date">
                                                            <input class="form-control" id="hotel_room"
                                                                name="hotel_room" type="text" value="1" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-users"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-12">Dari dan sampai tanggal
                                                        ?</label>
                                                    <div class="col-12 col-lg-6">
                                                        <div class="input-group" id="date_start_hotel">
                                                            <input type="text" class="form-control"
                                                                name="hotel_date_start" id="hotel_date_start" value=""
                                                                readonly required>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 text-center d-lg-none">
                                                        -
                                                    </div>
                                                    <div class="col-12 col-lg-6">
                                                        <div class="input-group" id="date_end_hotel">
                                                            <input type="text" class="form-control"
                                                                name="hotel_date_end" id="hotel_date_end" value=""
                                                                readonly required>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="p-3">
                                                            <p>Harga</p>
                                                            <h4><span id="harga_hotel_label"></span>
                                                                <small class="text-muted">per Malam</small>
                                                            </h4>
                                                            <p>Total</p>
                                                            <h4><span id="total_hotel_label">IDR 0</span>
                                                                <small class="text-muted" id="hotel_day_label"></small>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Penginapan-->
                                <!--begin::Biodata-->
                                <div class="pb-5" data-wizard-type="step-content">
                                    <h4 class="mb-10 font-weight-bold text-dark">Sekarang isi data diri kamu ya
                                    </h4>
                                    <!--begin::Input-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text"
                                                    class="form-control form-control-solid form-control-lg" name="name"
                                                    id="name" placeholder="Nama lengkap kamu" value="" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text"
                                                    class="form-control form-control-solid form-control-lg" name="email"
                                                    id="email" placeholder="Alamat email yang aktif" value=""
                                                    required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nomor Telepon</label>
                                                <input type="text"
                                                    class="form-control form-control-solid form-control-lg" name="phone"
                                                    id="phone" placeholder="Nomor yang bisa dihubungi kami" value=""
                                                    required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nomor Whatsapp</label>
                                                <input type="text"
                                                    class="form-control form-control-solid form-control-lg"
                                                    name="whatsapp" id="whatsapp"
                                                    placeholder="Nomor Whatsapp yang aktif" value="" required />
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Biodata-->
                                <!--begin::Review-->
                                <div class="pb-5" data-wizard-type="step-content">
                                    <!--begin::Section-->
                                    <h4 class="mb-10 font-weight-bold text-dark">
                                        Kamu sudah mengisi semua kebutuhanmu ! <br>
                                        Review kembali, jika sudah cocok tekan tombol
                                        <span class="text-success">Berangkat</span> yaa !
                                    </h4>
                                    <div class="row justify-content-center pt-8 pt-md-27">
                                        <div class="col-md-12">
                                            <!-- begin: Invoice header-->
                                            <div
                                                class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                                                <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
                                                <div class="d-flex flex-column align-items-md-end px-0">
                                                    <!--begin::Logo-->
                                                    <p class="mb-5 max-w-200px">
                                                        Bromokan
                                                    </p>
                                                    <!--end::Logo-->
                                                    <span
                                                        class="d-flex flex-column align-items-md-end font-size-h5 font-weight-bold text-muted">
                                                        <span>Desa Wonokitri, Pintu Masuk Bromo via
                                                            Nongkojajar</span>
                                                        <span>Pasuruan, Jawa Timur, Indonesia</span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="rounded-xl overflow-hidden w-100 max-h-md-250px mb-15 mb-md-30">
                                                <img src="{{asset('media/bg/bg-invoice-5.jpg')}}" class="w-100"
                                                    alt="" />
                                            </div>
                                            <!--end: Invoice header-->
                                            <!--begin: Invoice body-->
                                            <div class="row border-bottom pb-10">
                                                <div class="col-md-9 py-md-10 pr-md-10">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th
                                                                        class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Perlengkapan</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Jumlah</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Hari</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Harga</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="font-weight-bolder font-size-lg">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-warning p-3">
                                                                            <i class="la la-map-marked-alt
                                                                                        icon-2x text-light"></i>
                                                                        </span>
                                                                        <div>
                                                                            <div id="name_destination" class="mr-2">
                                                                            </div>
                                                                            <small>
                                                                                (+ Jeep)
                                                                            </small>
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="jeep_count_total"></span>
                                                                        Kendaraan
                                                                    </td>
                                                                    <td class="text-right pt-7">1 Hari</td>
                                                                    <td class="text-right pt-7">
                                                                        <div id="price_destination_label"></div>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <div id="jeep_price_total"></div>
                                                                    </td>
                                                                </tr>
                                                                <tr class="font-weight-bolder font-size-lg"
                                                                    id="review_penginapan" style="display: none">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-success p-3">
                                                                            <i class="la la-home
                                                                                        icon-2x text-light"></i>
                                                                        </span>
                                                                        <span id="nama_hotel_label"></span>
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="hotel_room_label"></span>
                                                                        Kamar</td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="hotel_day_review_label"></span>
                                                                    </td>
                                                                    <td class="text-right pt-7"><span
                                                                            id="harga_hotel_review_label"></span>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <span id="total_hotel_review_label"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="font-weight-bolder font-size-lg"
                                                                    id="review_ticket" style="display: none">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-primary p-3">
                                                                            <i class="la la-ticket-alt
                                                                                        icon-2x text-light"></i>
                                                                        </span>
                                                                        Tiket Masuk Bromo
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="ticket_count_label"></span>
                                                                        Tiket</td>
                                                                    <td class="text-right pt-7">
                                                                        -
                                                                    </td>
                                                                    <td class="text-right pt-7"><span
                                                                            id="harga_ticket_review_label"></span>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <span id="total_ticket_review_label"></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                    <div class="d-flex flex-column flex-md-row">
                                                        <div class="d-flex flex-column mb-10 mb-md-0">
                                                            <div class="font-weight-bold font-size-h6 mb-3">
                                                                INVOICE MILIK</div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">Nama
                                                                    Lengkap:</span>
                                                                <span class="text-right" id="invoice_name"></span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">Email:</span>
                                                                <span class="text-right" id="invoice_email"></span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">No
                                                                    Telepon:</span>
                                                                <span class="text-right" id="invoice_phone"></span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">No
                                                                    Whatsapp:</span>
                                                                <span class="text-right" id="invoice_whatsapp"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 border-left-md pl-md-10 py-md-10 text-right">
                                                    <!--begin::Total Amount-->
                                                    <div class="font-size-h4 font-weight-bolder text-muted mb-3">
                                                        TOTAL HARGA</div>
                                                    <div class="font-size-h1 font-weight-boldest" id="total_amount">
                                                    </div>
                                                    <div class="text-muted font-weight-bold mb-16"></div>
                                                    <!--end::Total Amount-->
                                                    <div class="border-bottom w-100 mb-16"></div>
                                                    <!--begin::Invoice To-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        KEPADA</div>
                                                    <div class="font-size-lg font-weight-bold mb-10" id="kepada"></div>
                                                    <!--end::Invoice To-->
                                                    <!--begin::Invoice No-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        NO INVOICE</div>
                                                    <div class="font-size-lg font-weight-bold mb-10">-</div>
                                                    <!--end::Invoice No-->
                                                    <!--begin::Invoice Date-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        TANGGAL PESAN</div>
                                                    <div class="font-size-lg font-weight-bold">
                                                        {{now()->format('d-m-Y')}}
                                                    </div>
                                                    <!--end::Invoice Date-->
                                                </div>
                                            </div>
                                            <!--end: Invoice body-->
                                        </div>
                                    </div>
                                </div>
                                <!--end::Review-->
                                <!--begin::Wizard Actions-->
                                <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                    <div class="mr-2">
                                        <button type="button"
                                            class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4"
                                            data-wizard-type="action-prev">Mundur</button>
                                    </div>
                                    <div>
                                        <button type="button"
                                            class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"
                                            data-wizard-type="action-submit">Berangkat</button>
                                        <button type="button"
                                            class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4"
                                            data-wizard-type="action-next">Maju</button>
                                    </div>
                                </div>
                                <!--end::Wizard Actions-->
                                </form>
                                <!--end::Wizard Form-->
                            </div>
                        </div>
                        <!--end::Wizard Body-->
                    </div>
                    <!--end::Wizard-->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true" id="modal-form">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-xl" role="document">
        <div class="modal-content" id="load-form">
        </div>
    </div>
</div>
<!--end::Modal-->

<!--end::Order-->
<!--end::Entry-->
@endsection
@section('js')
<script src="{{asset('js/pages/custom/wizard/wizard-1.js')}}"></script>
<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/pages/crud/forms/widgets/bootstrap-daterangepicker.js')}}"></script>
<script src="{{asset('/js/pages/crud/forms/widgets/input-mask.js')}}"></script>
<script>
    $("#person").inputmask({
        "mask": "9",
        "repeat": 10,
        "greedy": false
    });

    $("#hotel_room").inputmask({
        "mask": "9",
        "repeat": 10,
        "greedy": false
    });

    $('#date_go').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        minDate: new Date(),

        singleDatePicker: true,
        // showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }, function(start, end, label) {
        $('#date_go .form-control').val(start.format('DD-MM-YYYY'));
        $('#hotel_date_start').val(start.format('DD-MM-YYYY'));
        $('#hotel_date_end').val(moment(start).add(1,'days').format('DD-MM-YYYY'));
        $('#jeep_date').val(start.format('DD-MM-YYYY'));
        $('#jeep_day').html(1);

        totalAmount()
    });

    $('#date_start_hotel').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        minDate: new Date(),

        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }, function(start, end, label) {
        $('#date_start_hotel .form-control').val(start.format('DD-MM-YYYY'));
        $('#hotel_day').val(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())));
        $('#hotel_day_label').html(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())) + ' Malam');
        $('#hotel_day_review_label').html(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())) + ' Malam');

        totalAmount()
    });

    function parseDate(str) {
        var mdy = str.split('-');
        return new Date(mdy[2], mdy[1]-1, mdy[0]);
    }

    function datediff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    function countJeep(person) {
        return Math.ceil(person / 5);
    }

    function countJeepPrice() {
        var count = $('#price_destination').val() * countJeep($('#jeep_person').val()) * 1;
        $('#jeep_price').html('IDR ' + (count + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('#jeep_price_total').html('IDR ' + (count + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('#jeep_price_span').html('IDR ' + ($('#price_destination').val() + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        return count;
    }

    function countHotelPrice() {
        var hotelDay = datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())) || 0;
        var priceHotel = parseInt(hotelDay) * parseInt($('#harga_hotel').val()) * parseInt($("#hotel_room").val());
        $('#total_hotel').val(priceHotel);
        $('#total_hotel_label').html('IDR ' + (priceHotel + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        $('#total_hotel_review_label').html('IDR ' + (priceHotel + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        return priceHotel;
    }

    $('#hotel_room').on('keyup', function(){
        $('#hotel_room_label').html(this.value);
        totalAmount()
    });


    $('#date_end_hotel').daterangepicker({
        buttonClasses: ' btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        minDate: new Date(),

        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD-MM-YYYY'
        }
    }, function(start, end, label) {
        $('#date_end_hotel .form-control').val(start.format('DD-MM-YYYY'));
        $('#hotel_day').val(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())));
        $('#hotel_day_label').html(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())) + ' Malam');
        $('#hotel_day_review_label').html(datediff(parseDate($('#date_start_hotel .form-control').val()), parseDate($('#date_end_hotel .form-control').val())) + ' Malam');

        totalAmount()
    });

    $('#person').on('change', function(){
        var value = $(this).val();
        $('#jeep_person').val(value);
        $('#jeep_person_span').html(value + ' orang');
        $('#jeep_count').html(countJeep(value));
        $('#jeep_count_total').html(countJeep(value));
        $('#jeep').val(countJeep(value));

        totalAmount()
    });

    $('#jeep_date').val(moment().format('DD-MM-YYYY'));
    $('#hotel_date_start').val(moment().format('DD-MM-YYYY'));
    $('#hotel_date_end').val(moment().add(1,'days').format('DD-MM-YYYY'));
    $('#hotel_day_review_label').html(datediff(parseDate(moment().format('DD-MM-YYYY')), parseDate(moment().add(1,'days').format('DD-MM-YYYY'))) + ' Malam');
    $('#hotel_room_label').html(1);
    $('#jeep_person').val(1);
    $('#jeep_person_span').html('1 orang');
    $('#jeep_count').html(countJeep(1));
    $('#jeep_count_total').html(countJeep(1));
    $('#jeep').val(countJeep(1));
    $('#jeep_day').html(1);
    countJeepPrice()

    function modalPenginapan(id)
    {
        $("#modal-form").modal();
        $("#load-form").load("<?= url('modal/penginapan') . '/' ?>" + id)
    }

    function modalDestination(id) {
        $("#modal-form").modal();
        $("#load-form").load("<?= url('modal/destination') . '/' ?>" + id)
    }

    function modalWisata(id) {
        $("#modal-form").modal();
        $("#load-form").load("<?= url('modal/wisata') . '/' ?>" + id)
    }

    function priceTicket() {
        var date = parseDate($('#date_go .form-control').val());
        if(date.getDay() === 6){
            return {{$priceWeekends}}
        }else{
            return {{$priceWeekends}}
        }
    }

    function needTicket() {
        var need_ticket = document.getElementById("need_ticket");
        var review_ticket = document.getElementById("review_ticket");

        if (need_ticket.checked == true){
            $('#ticket_count_label').html($('#person').val());
            $('#harga_ticket_review_label').html('IDR ' + (priceTicket() + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#total_ticket_review_label').html('IDR ' + ($('#person').val() * priceTicket() + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            review_ticket.style.display = "table-row";
        } else {
            review_ticket.style.display = "none";
        }
        totalAmount()
    }

    function needHotel() {
        var need_hotel = document.getElementById("need_hotel");
        var list_penginapan = document.getElementById("list_penginapan");
        var review_penginapan = document.getElementById("review_penginapan");

        if (need_hotel.checked == true){
            list_penginapan.style.display = "block";
            review_penginapan.style.display = "table-row";
        } else {
            list_penginapan.style.display = "none";
            review_penginapan.style.display = "none";
        }
        totalAmount()
    }

    function needTransport() {
        var need_transport = document.getElementById("need_transport");
        var destination_enable = document.getElementById("destination_enable");
        var destination_disable = document.getElementById("destination_disable");

        if (need_transport.checked == true){
            destination_enable.style.display = "block";
            destination_disable.style.display = "none";
        } else {
            destination_enable.style.display = "none";
            destination_disable.style.display = "block";
        }
        totalAmount()
    }

    $(".btn-pilih-hotel").on('click', function(){
        var hotel_terpilih = document.getElementById("hotel-terpilih");
        hotel_terpilih.style.display = "flex";

        var id = $(this).attr('id').substr(6);
        var target = $(this).closest('#card-hotel-' + id);

        $('.hotel-terpilih').empty();
        target.clone(true, true).appendTo(".hotel-terpilih");
        $('#id_hotel').val(id);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{url("/ajax/getHotelById/")}}' + '/' + id,
            dataType: 'json',
        success: function (data) {
            $('#nama_hotel').val(data.name);
            $('#nama_hotel_label').html(data.name);
            $('#harga_hotel').val(data.price);
            $('#harga_hotel_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#harga_hotel_review_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            totalAmount()
        },error:function($e){
            console.log($e);
        }})
    })

    $(".btn-destination").on('click', function(){
        var id = $(this).attr('id').substr(12);
        $('.card-destination').removeClass("shadow border border-primary")
        $('#card-destination-' + id).addClass("shadow border border-primary")
        $('#id_destination').val(id);
        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{url("/ajax/getDestinationById/")}}' + '/' + id,
            dataType: 'json',
        success: function (data) {
            $('#name_destination').html(data.name);
            $('#price_destination').val(data.price);
            $('#price_destination_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#price_destination_total_label').html('IDR ' + (data.price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            totalAmount()
        },error:function($e){
            console.log($e);
        }})
    })

    $('#name').on('change', function(){
        $('#invoice_name').html($(this).val());
        $('#kepada').html($(this).val());
    });

    $('#email').on('change', function(){
        $('#invoice_email').html($(this).val());
    });

    $('#phone').on('change', function(){
        $('#invoice_phone').html($(this).val());
    });

    $('#whatsapp').on('change', function(){
        $('#invoice_whatsapp').html($(this).val());
    });

    function totalAmount() {
        var priceTicketTotal = 0;
        var priceHotel = 0;
        if (document.getElementById("need_ticket").checked) {
            priceTicketTotal = $('#person').val() * priceTicket();
        }

        if (document.getElementById("need_hotel").checked) {
            priceHotel = countHotelPrice();
        }

        var total = priceHotel + countJeepPrice() + priceTicketTotal
        $('#total_amount').html('IDR ' + (total + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    }
    totalAmount()
</script>
@endsection