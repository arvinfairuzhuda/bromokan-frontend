@push('styles')
<link href="{{asset('plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush

<div>
    <!--begin: Search Form-->
    <form class="mb-15">
        <div class="row mb-6">
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>RecordID:</label>
                <input type="text" class="form-control datatable-input" placeholder="E.g: 4590" data-col-index="0" />
            </div>
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>OrderID:</label>
                <input type="text" class="form-control datatable-input" placeholder="E.g: 37000-300"
                    data-col-index="1" />
            </div>
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>Country:</label>
                <select class="form-control datatable-input" data-col-index="2">
                    <option value="">Select</option>
                </select>
            </div>
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>Agent:</label>
                <input type="text" class="form-control datatable-input" placeholder="Agent ID or name"
                    data-col-index="4" />
            </div>
        </div>
        <div class="row mb-8">
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>Ship Date:</label>
                <div class="input-daterange input-group" id="kt_datepicker">
                    <input type="text" class="form-control datatable-input" name="start" placeholder="From"
                        data-col-index="5" />
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="la la-ellipsis-h"></i>
                        </span>
                    </div>
                    <input type="text" class="form-control datatable-input" name="end" placeholder="To"
                        data-col-index="5" />
                </div>
            </div>
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>Status:</label>
                <select class="form-control datatable-input" data-col-index="6">
                    <option value="">Select</option>
                </select>
            </div>
            <div class="col-lg-3 mb-lg-0 mb-6">
                <label>Type:</label>
                <select class="form-control datatable-input" data-col-index="7">
                    <option value="">Select</option>
                </select>
            </div>
        </div>
        <div class="row mt-8">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-primary--icon" id="kt_search">
                    <span>
                        <i class="la la-search"></i>
                        <span>Search</span>
                    </span>
                </button>&#160;&#160;
                <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                    <span>
                        <i class="la la-close"></i>
                        <span>Reset</span>
                    </span>
                </button></div>
        </div>
    </form>
    <!--begin: Datatable-->
    <!--begin: Datatable-->
    <table class="table table-bordered table-hover table-checkable" id="kt_datatable">
        <thead>
            <tr>
                <th>NIK</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat KTP</th>
                <th>Nomor KK</th>
                <th>Nomor HP Member</th>
                <th>Jenis Usaha Member</th>
                <th>NIK Penjamin</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    <!--end: Datatable-->
</div>

@push('scripts')
<script src="{{asset('plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script>
    var KTDatatablesSearchOptionsAdvancedSearch = function () {

    $.fn.dataTable.Api.register('column().title()', function () {
        return $(this.header()).text().trim();
    });

    var initTable1 = function () {
        // begin first table
        var table = $('#kt_datatable').DataTable({
            responsive: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html
            lengthMenu: [5, 10, 25, 50],
            pageLength: 10,
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{env('APP_URL')}}" + '/api/individu/DT_list_member',
                type: 'POST',
                headers: { 'Authorization': 'Bearer ' + "{{session('token')}}" },
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'nik_penjamin', 'nik_member', 'jk_member', 'tempat_lahir_member', 'tanggal_lahir_member',
                        'alamat_ktp', 'no_kk', 'no_hp_member', 'Actions',],
                },
            },
            columns: [
                { data: 'nik_member' },
                { data: 'nama_member' },
                { data: 'jk_member' },
                { data: 'tempat_lahir_member' },
                { data: 'tanggal_lahir_member' },
                { data: 'alamat_ktp' },
                { data: 'no_kk' },
                { data: 'no_hp_member' },
                { data: 'jenis_usaha_member' },
                { data: 'nik_penjamin' },
                {
                    data: null,
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],

            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    // switch (column.title()) {
                    //     case 'Country':
                    //         column.data().unique().sort().each(function (d, j) {
                    //             $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
                    //         });
                    //         break;

                    //     case 'Status':
                    //         var status = {
                    //             1: { 'title': 'Pending', 'class': 'label-light-primary' },
                    //             2: { 'title': 'Delivered', 'class': ' label-light-danger' },
                    //             3: { 'title': 'Canceled', 'class': ' label-light-primary' },
                    //             4: { 'title': 'Success', 'class': ' label-light-success' },
                    //             5: { 'title': 'Info', 'class': ' label-light-info' },
                    //             6: { 'title': 'Danger', 'class': ' label-light-danger' },
                    //             7: { 'title': 'Warning', 'class': ' label-light-warning' },
                    //         };
                    //         column.data().unique().sort().each(function (d, j) {
                    //             $('.datatable-input[data-col-index="6"]').append('<option value="' + d + '">' + status[d].title + '</option>');
                    //         });
                    //         break;

                    //     case 'Type':
                    //         var status = {
                    //             1: { 'title': 'Online', 'state': 'danger' },
                    //             2: { 'title': 'Retail', 'state': 'primary' },
                    //             3: { 'title': 'Direct', 'state': 'success' },
                    //         };
                    //         column.data().unique().sort().each(function (d, j) {
                    //             $('.datatable-input[data-col-index="7"]').append('<option value="' + d + '">' + status[d].title + '</option>');
                    //         });
                    //         break;
                    // }
                });
            },

            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        return '\
							<div class="dropdown dropdown-inline">\
								<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown">\
	                                <i class="la la-cog"></i>\
	                            </a>\
							  	<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
									<ul class="nav nav-hoverable flex-column">\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-edit"></i><span class="nav-text">Edit Details</span></a></li>\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-leaf"></i><span class="nav-text">Update Status</span></a></li>\
							    		<li class="nav-item"><a class="nav-link" href="#"><i class="nav-icon la la-print"></i><span class="nav-text">Print</span></a></li>\
									</ul>\
							  	</div>\
							</div>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
								<i class="la la-edit"></i>\
							</a>\
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
								<i class="la la-trash"></i>\
							</a>\
						';
                    },
                },
                // {
                //     targets: 6,
                //     render: function (data, type, full, meta) {
                //         var status = {
                //             1: { 'title': 'Pending', 'class': 'label-light-primary' },
                //             2: { 'title': 'Delivered', 'class': ' label-light-danger' },
                //             3: { 'title': 'Canceled', 'class': ' label-light-primary' },
                //             4: { 'title': 'Success', 'class': ' label-light-success' },
                //             5: { 'title': 'Info', 'class': ' label-light-info' },
                //             6: { 'title': 'Danger', 'class': ' label-light-danger' },
                //             7: { 'title': 'Warning', 'class': ' label-light-warning' },
                //         };
                //         if (typeof status[data] === 'undefined') {
                //             return data;
                //         }
                //         return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                //     },
                // },
                // {
                //     targets: 7,
                //     render: function (data, type, full, meta) {
                //         var status = {
                //             1: { 'title': 'Online', 'state': 'danger' },
                //             2: { 'title': 'Retail', 'state': 'primary' },
                //             3: { 'title': 'Direct', 'state': 'success' },
                //         };
                //         if (typeof status[data] === 'undefined') {
                //             return data;
                //         }
                //         return '<span class="label label-' + status[data].state + ' label-dot mr-2"></span>' +
                //             '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                //     },
                // },
            ],
        });

        var filter = function () {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
        };

        var asdasd = function (value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table.column(index).search(val ? val : '', false, true);
        };

        $('#kt_search').on('click', function (e) {
            e.preventDefault();
            var params = {};
            $('.datatable-input').each(function () {
                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function (i, val) {
                // apply search params to datatable
                table.column(i).search(val ? val : '', false, false);
            });
            table.table().draw();
        });

        $('#kt_reset').on('click', function (e) {
            e.preventDefault();
            $('.datatable-input').each(function () {
                $(this).val('');
                table.column($(this).data('col-index')).search('', false, false);
            });
            table.table().draw();
        });

        $('#kt_datepicker').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });

    };

    return {

        //main function to initiate the module
        init: function () {
            initTable1();
        },

    };

}();

jQuery(document).ready(function () {
    KTDatatablesSearchOptionsAdvancedSearch.init();
});

</script>
@endpush