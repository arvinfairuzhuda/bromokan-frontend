<style type="text/css" media="all">
    @charset "UTF-8";

    /*!
 * Bootstrap v4.6.0 (https://getbootstrap.com/)
 * Copyright 2011-2021 The Bootstrap Authors
 * Copyright 2011-2021 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
 */

    *,
    *::before,
    *::after {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    html {
        font-family: sans-serif;
        line-height: 1.15;
        -webkit-text-size-adjust: 100%;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    article,
    aside,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    main,
    nav,
    section {
        display: block;
    }

    body {
        margin: 0;
        font-family: Poppins, Helvetica, "sans-serif";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #3F4254;
        text-align: left;
        background-color: #ffffff;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        margin-top: 0;
        margin-bottom: 0.5rem;
    }

    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    blockquote {
        margin: 0 0 1rem;
    }

    b,
    strong {
        font-weight: 600;
    }

    small {
        font-size: 80%;
    }

    sub,
    sup {
        position: relative;
        font-size: 75%;
        line-height: 0;
        vertical-align: baseline;
    }

    sub {
        bottom: -0.25em;
    }

    sup {
        top: -0.5em;
    }

    a {
        color: #3eaae8;
        text-decoration: none;
        background-color: transparent;
    }

    a:hover {
        color: #33192c;
        text-decoration: underline;
    }

    a:not([href]):not([class]) {
        color: inherit;
        text-decoration: none;
    }

    a:not([href]):not([class]):hover {
        color: inherit;
        text-decoration: none;
    }

    img {
        vertical-align: middle;
        border-style: none;
    }

    table {
        border-collapse: collapse;
    }

    caption {
        padding-top: 0.75rem;
        padding-bottom: 0.75rem;
        color: #B5B5C3;
        text-align: left;
        caption-side: bottom;
    }

    th {
        text-align: inherit;
        text-align: -webkit-match-parent;
    }

    label {
        display: inline-block;
        margin-bottom: 0.5rem;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    .h1,
    .h2,
    .h3,
    .h4,
    .h5,
    .h6 {
        margin-bottom: 0.5rem;
        font-weight: 500;
        line-height: 1.2;
    }

    h1,
    .h1 {
        font-size: 2rem;
    }

    @media (max-width: 1200px) {

        h1,
        .h1 {
            font-size: calc(1.325rem + 0.9vw);
        }
    }

    h2,
    .h2 {
        font-size: 1.75rem;
    }

    @media (max-width: 1200px) {

        h2,
        .h2 {
            font-size: calc(1.3rem + 0.6vw);
        }
    }

    h3,
    .h3 {
        font-size: 1.5rem;
    }

    @media (max-width: 1200px) {

        h3,
        .h3 {
            font-size: calc(1.275rem + 0.3vw);
        }
    }

    h4,
    .h4 {
        font-size: 1.35rem;
    }

    @media (max-width: 1200px) {

        h4,
        .h4 {
            font-size: calc(1.26rem + 0.12vw);
        }
    }

    h5,
    .h5 {
        font-size: 1.25rem;
    }

    h6,
    .h6 {
        font-size: 1.175rem;
    }

    .lead {
        font-size: 1.25rem;
        font-weight: 300;
    }

    hr {
        margin-top: 1rem;
        margin-bottom: 1rem;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
    }

    small,
    .small {
        font-size: 80%;
        font-weight: 400;
    }

    .img-fluid {
        max-width: 100%;
        height: auto;
    }

    .img-thumbnail {
        padding: 0.25rem;
        background-color: #ffffff;
        border: 1px solid #3252DF;
        border-radius: 0.675rem;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.075);
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.075);
        max-width: 100%;
        height: auto;
    }

    .container,
    .container-fluid,
    .container-xxl,
    .container-xl,
    .container-lg,
    .container-md,
    .container-sm {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    @media (min-width: 576px) {

        .container-sm,
        .container {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {

        .container-md,
        .container-sm,
        .container {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {

        .container-lg,
        .container-md,
        .container-sm,
        .container {
            max-width: 960px;
        }
    }

    @media (min-width: 1200px) {

        .container-xl,
        .container-lg,
        .container-md,
        .container-sm,
        .container {
            max-width: 1140px;
        }
    }

    @media (min-width: 1400px) {

        .container-xxl,
        .container-xl,
        .container-lg,
        .container-md,
        .container-sm,
        .container {
            max-width: 1340px;
        }
    }

    .row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }

    .col-xxl,
    .col-xxl-auto,
    .col-xxl-12,
    .col-xxl-11,
    .col-xxl-10,
    .col-xxl-9,
    .col-xxl-8,
    .col-xxl-7,
    .col-xxl-6,
    .col-xxl-5,
    .col-xxl-4,
    .col-xxl-3,
    .col-xxl-2,
    .col-xxl-1,
    .col-xl,
    .col-xl-auto,
    .col-xl-12,
    .col-xl-11,
    .col-xl-10,
    .col-xl-9,
    .col-xl-8,
    .col-xl-7,
    .col-xl-6,
    .col-xl-5,
    .col-xl-4,
    .col-xl-3,
    .col-xl-2,
    .col-xl-1,
    .col-lg,
    .col-lg-auto,
    .col-lg-12,
    .col-lg-11,
    .col-lg-10,
    .col-lg-9,
    .col-lg-8,
    .col-lg-7,
    .col-lg-6,
    .col-lg-5,
    .col-lg-4,
    .col-lg-3,
    .col-lg-2,
    .col-lg-1,
    .col-md,
    .col-md-auto,
    .col-md-12,
    .col-md-11,
    .col-md-10,
    .col-md-9,
    .col-md-8,
    .col-md-7,
    .col-md-6,
    .col-md-5,
    .col-md-4,
    .col-md-3,
    .col-md-2,
    .col-md-1,
    .col-sm,
    .col-sm-auto,
    .col-sm-12,
    .col-sm-11,
    .col-sm-10,
    .col-sm-9,
    .col-sm-8,
    .col-sm-7,
    .col-sm-6,
    .col-sm-5,
    .col-sm-4,
    .col-sm-3,
    .col-sm-2,
    .col-sm-1,
    .col,
    .col-auto,
    .col-12,
    .col-11,
    .col-10,
    .col-9,
    .col-8,
    .col-7,
    .col-6,
    .col-5,
    .col-4,
    .col-3,
    .col-2,
    .col-1 {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }

    .col {
        -ms-flex-preferred-size: 0;
        flex-basis: 0;
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
        max-width: 100%;
    }

    .row-cols-1>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }

    .row-cols-2>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }

    .row-cols-3>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.3333333333%;
        flex: 0 0 33.3333333333%;
        max-width: 33.3333333333%;
    }

    .row-cols-4>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 25%;
        flex: 0 0 25%;
        max-width: 25%;
    }

    .row-cols-5>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
    }

    .row-cols-6>* {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 16.6666666667%;
        flex: 0 0 16.6666666667%;
        max-width: 16.6666666667%;
    }

    .col-auto {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
        width: auto;
        max-width: 100%;
    }

    .col-1 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 8.3333333333%;
        flex: 0 0 8.3333333333%;
        max-width: 8.3333333333%;
    }

    .col-2 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 16.6666666667%;
        flex: 0 0 16.6666666667%;
        max-width: 16.6666666667%;
    }

    .col-3 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 25%;
        flex: 0 0 25%;
        max-width: 25%;
    }

    .col-4 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 33.3333333333%;
        flex: 0 0 33.3333333333%;
        max-width: 33.3333333333%;
    }

    .col-5 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 41.6666666667%;
        flex: 0 0 41.6666666667%;
        max-width: 41.6666666667%;
    }

    .col-6 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }

    .col-7 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 58.3333333333%;
        flex: 0 0 58.3333333333%;
        max-width: 58.3333333333%;
    }

    .col-8 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 66.6666666667%;
        flex: 0 0 66.6666666667%;
        max-width: 66.6666666667%;
    }

    .col-9 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 75%;
        flex: 0 0 75%;
        max-width: 75%;
    }

    .col-10 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 83.3333333333%;
        flex: 0 0 83.3333333333%;
        max-width: 83.3333333333%;
    }

    .col-11 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 91.6666666667%;
        flex: 0 0 91.6666666667%;
        max-width: 91.6666666667%;
    }

    .col-12 {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }

    @media (min-width: 576px) {
        .col-sm {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }

        .row-cols-sm-1>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .row-cols-sm-2>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .row-cols-sm-3>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .row-cols-sm-4>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .row-cols-sm-5>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 20%;
            flex: 0 0 20%;
            max-width: 20%;
        }

        .row-cols-sm-6>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-sm-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
        }

        .col-sm-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.3333333333%;
            flex: 0 0 8.3333333333%;
            max-width: 8.3333333333%;
        }

        .col-sm-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-sm-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-sm-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .col-sm-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.6666666667%;
            flex: 0 0 41.6666666667%;
            max-width: 41.6666666667%;
        }

        .col-sm-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-sm-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.3333333333%;
            flex: 0 0 58.3333333333%;
            max-width: 58.3333333333%;
        }

        .col-sm-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.6666666667%;
            flex: 0 0 66.6666666667%;
            max-width: 66.6666666667%;
        }

        .col-sm-9 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-sm-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.3333333333%;
            flex: 0 0 83.3333333333%;
            max-width: 83.3333333333%;
        }

        .col-sm-11 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 91.6666666667%;
            flex: 0 0 91.6666666667%;
            max-width: 91.6666666667%;
        }

        .col-sm-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
    }

    @media (min-width: 768px) {
        .col-md {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }

        .row-cols-md-1>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .row-cols-md-2>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .row-cols-md-3>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .row-cols-md-4>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .row-cols-md-5>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 20%;
            flex: 0 0 20%;
            max-width: 20%;
        }

        .row-cols-md-6>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-md-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
        }

        .col-md-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.3333333333%;
            flex: 0 0 8.3333333333%;
            max-width: 8.3333333333%;
        }

        .col-md-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-md-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-md-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .col-md-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.6666666667%;
            flex: 0 0 41.6666666667%;
            max-width: 41.6666666667%;
        }

        .col-md-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-md-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.3333333333%;
            flex: 0 0 58.3333333333%;
            max-width: 58.3333333333%;
        }

        .col-md-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.6666666667%;
            flex: 0 0 66.6666666667%;
            max-width: 66.6666666667%;
        }

        .col-md-9 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-md-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.3333333333%;
            flex: 0 0 83.3333333333%;
            max-width: 83.3333333333%;
        }

        .col-md-11 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 91.6666666667%;
            flex: 0 0 91.6666666667%;
            max-width: 91.6666666667%;
        }

        .col-md-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

    }

    @media (min-width: 992px) {
        .col-lg {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }

        .row-cols-lg-1>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .row-cols-lg-2>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .row-cols-lg-3>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .row-cols-lg-4>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .row-cols-lg-5>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 20%;
            flex: 0 0 20%;
            max-width: 20%;
        }

        .row-cols-lg-6>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-lg-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
        }

        .col-lg-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.3333333333%;
            flex: 0 0 8.3333333333%;
            max-width: 8.3333333333%;
        }

        .col-lg-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-lg-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-lg-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .col-lg-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.6666666667%;
            flex: 0 0 41.6666666667%;
            max-width: 41.6666666667%;
        }

        .col-lg-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-lg-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.3333333333%;
            flex: 0 0 58.3333333333%;
            max-width: 58.3333333333%;
        }

        .col-lg-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.6666666667%;
            flex: 0 0 66.6666666667%;
            max-width: 66.6666666667%;
        }

        .col-lg-9 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-lg-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.3333333333%;
            flex: 0 0 83.3333333333%;
            max-width: 83.3333333333%;
        }

        .col-lg-11 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 91.6666666667%;
            flex: 0 0 91.6666666667%;
            max-width: 91.6666666667%;
        }

        .col-lg-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

    }

    @media (min-width: 1200px) {
        .col-xl {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }

        .row-cols-xl-1>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .row-cols-xl-2>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .row-cols-xl-3>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .row-cols-xl-4>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .row-cols-xl-5>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 20%;
            flex: 0 0 20%;
            max-width: 20%;
        }

        .row-cols-xl-6>* {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-xl-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
        }

        .col-xl-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 8.3333333333%;
            flex: 0 0 8.3333333333%;
            max-width: 8.3333333333%;
        }

        .col-xl-2 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.6666666667%;
            flex: 0 0 16.6666666667%;
            max-width: 16.6666666667%;
        }

        .col-xl-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-xl-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.3333333333%;
            flex: 0 0 33.3333333333%;
            max-width: 33.3333333333%;
        }

        .col-xl-5 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 41.6666666667%;
            flex: 0 0 41.6666666667%;
            max-width: 41.6666666667%;
        }

        .col-xl-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-xl-7 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 58.3333333333%;
            flex: 0 0 58.3333333333%;
            max-width: 58.3333333333%;
        }

        .col-xl-8 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 66.6666666667%;
            flex: 0 0 66.6666666667%;
            max-width: 66.6666666667%;
        }

        .col-xl-9 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }

        .col-xl-10 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 83.3333333333%;
            flex: 0 0 83.3333333333%;
            max-width: 83.3333333333%;
        }

        .col-xl-11 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 91.6666666667%;
            flex: 0 0 91.6666666667%;
            max-width: 91.6666666667%;
        }

        .col-xl-12 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
    }

    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #3F4254;
        background-color: transparent;
    }

    .table th,
    .table td {
        padding: 0.75rem;
        vertical-align: top;
        border-top: 1px solid #EBEDF3;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #EBEDF3;
    }

    .table tbody+tbody {
        border-top: 2px solid #EBEDF3;
    }

    .table-responsive {
        display: block;
        width: 100%;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
    }

    .table-responsive>.table-bordered {
        border: 0;
    }

    .bg-primary {
        background-color: #3eaae8 !important;
    }

    a.bg-primary:hover,
    a.bg-primary:focus,
    button.bg-primary:hover,
    button.bg-primary:focus {
        background-color: #44213b !important;
    }

    .bg-success {
        background-color: #1BC5BD !important;
    }

    a.bg-success:hover,
    a.bg-success:focus,
    button.bg-success:hover,
    button.bg-success:focus {
        background-color: #159892 !important;
    }

    .bg-warning {
        background-color: #FFA800 !important;
    }

    a.bg-warning:hover,
    a.bg-warning:focus,
    button.bg-warning:hover,
    button.bg-warning:focus {
        background-color: #cc8600 !important;
    }

    .border-0 {
        border: 0 !important;
    }

    .border-top-0 {
        border-top: 0 !important;
    }

    .border-right-0 {
        border-right: 0 !important;
    }

    .border-bottom-0 {
        border-bottom: 0 !important;
    }

    .border-left-0 {
        border-left: 0 !important;
    }

    .rounded-sm {
        border-radius: 0.28rem !important;
    }

    .rounded {
        border-radius: 0.675rem !important;
    }

    .rounded-top {
        border-top-left-radius: 0.675rem !important;
        border-top-right-radius: 0.675rem !important;
    }

    .rounded-right {
        border-top-right-radius: 0.675rem !important;
        border-bottom-right-radius: 0.675rem !important;
    }

    .rounded-bottom {
        border-bottom-right-radius: 0.675rem !important;
        border-bottom-left-radius: 0.675rem !important;
    }

    .rounded-left {
        border-top-left-radius: 0.675rem !important;
        border-bottom-left-radius: 0.675rem !important;
    }

    .rounded-lg {
        border-radius: 0.85rem !important;
    }

    .rounded-circle {
        border-radius: 50% !important;
    }

    .rounded-pill {
        border-radius: 50rem !important;
    }

    .rounded-0 {
        border-radius: 0 !important;
    }

    .clearfix::after {
        display: block;
        clear: both;
        content: "";
    }

    .d-none {
        display: none !important;
    }

    .d-inline {
        display: inline !important;
    }

    .d-inline-block {
        display: inline-block !important;
    }

    .d-block {
        display: block !important;
    }

    .d-table {
        display: table !important;
    }

    .d-table-row {
        display: table-row !important;
    }

    .d-table-cell {
        display: table-cell !important;
    }

    .d-flex {
        display: -webkit-box !important;
        display: -ms-flexbox !important;
        display: flex !important;
    }

    .d-inline-flex {
        display: -webkit-inline-box !important;
        display: -ms-inline-flexbox !important;
        display: inline-flex !important;
    }

    @media (min-width: 576px) {
        .d-sm-none {
            display: none !important;
        }

        .d-sm-inline {
            display: inline !important;
        }

        .d-sm-inline-block {
            display: inline-block !important;
        }

        .d-sm-block {
            display: block !important;
        }

        .d-sm-table {
            display: table !important;
        }

        .d-sm-table-row {
            display: table-row !important;
        }

        .d-sm-table-cell {
            display: table-cell !important;
        }

        .d-sm-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .d-sm-inline-flex {
            display: -webkit-inline-box !important;
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }

    @media (min-width: 768px) {
        .d-md-none {
            display: none !important;
        }

        .d-md-inline {
            display: inline !important;
        }

        .d-md-inline-block {
            display: inline-block !important;
        }

        .d-md-block {
            display: block !important;
        }

        .d-md-table {
            display: table !important;
        }

        .d-md-table-row {
            display: table-row !important;
        }

        .d-md-table-cell {
            display: table-cell !important;
        }

        .d-md-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .d-md-inline-flex {
            display: -webkit-inline-box !important;
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }

    @media (min-width: 992px) {
        .d-lg-none {
            display: none !important;
        }

        .d-lg-inline {
            display: inline !important;
        }

        .d-lg-inline-block {
            display: inline-block !important;
        }

        .d-lg-block {
            display: block !important;
        }

        .d-lg-table {
            display: table !important;
        }

        .d-lg-table-row {
            display: table-row !important;
        }

        .d-lg-table-cell {
            display: table-cell !important;
        }

        .d-lg-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .d-lg-inline-flex {
            display: -webkit-inline-box !important;
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }

    @media (min-width: 1200px) {
        .d-xl-none {
            display: none !important;
        }

        .d-xl-inline {
            display: inline !important;
        }

        .d-xl-inline-block {
            display: inline-block !important;
        }

        .d-xl-block {
            display: block !important;
        }

        .d-xl-table {
            display: table !important;
        }

        .d-xl-table-row {
            display: table-row !important;
        }

        .d-xl-table-cell {
            display: table-cell !important;
        }

        .d-xl-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .d-xl-inline-flex {
            display: -webkit-inline-box !important;
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }

    @media (min-width: 1400px) {
        .d-xxl-none {
            display: none !important;
        }

        .d-xxl-inline {
            display: inline !important;
        }

        .d-xxl-inline-block {
            display: inline-block !important;
        }

        .d-xxl-block {
            display: block !important;
        }

        .d-xxl-table {
            display: table !important;
        }

        .d-xxl-table-row {
            display: table-row !important;
        }

        .d-xxl-table-cell {
            display: table-cell !important;
        }

        .d-xxl-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        .d-xxl-inline-flex {
            display: -webkit-inline-box !important;
            display: -ms-inline-flexbox !important;
            display: inline-flex !important;
        }
    }

    .flex-row {
        -webkit-box-orient: horizontal !important;
        -webkit-box-direction: normal !important;
        -ms-flex-direction: row !important;
        flex-direction: row !important;
    }

    .flex-column {
        -webkit-box-orient: vertical !important;
        -webkit-box-direction: normal !important;
        -ms-flex-direction: column !important;
        flex-direction: column !important;
    }

    .flex-row-reverse {
        -webkit-box-orient: horizontal !important;
        -webkit-box-direction: reverse !important;
        -ms-flex-direction: row-reverse !important;
        flex-direction: row-reverse !important;
    }

    .flex-column-reverse {
        -webkit-box-orient: vertical !important;
        -webkit-box-direction: reverse !important;
        -ms-flex-direction: column-reverse !important;
        flex-direction: column-reverse !important;
    }

    .flex-wrap {
        -ms-flex-wrap: wrap !important;
        flex-wrap: wrap !important;
    }

    .flex-nowrap {
        -ms-flex-wrap: nowrap !important;
        flex-wrap: nowrap !important;
    }

    .flex-wrap-reverse {
        -ms-flex-wrap: wrap-reverse !important;
        flex-wrap: wrap-reverse !important;
    }

    .flex-fill {
        -webkit-box-flex: 1 !important;
        -ms-flex: 1 1 auto !important;
        flex: 1 1 auto !important;
    }

    .flex-grow-0 {
        -webkit-box-flex: 0 !important;
        -ms-flex-positive: 0 !important;
        flex-grow: 0 !important;
    }

    .flex-grow-1 {
        -webkit-box-flex: 1 !important;
        -ms-flex-positive: 1 !important;
        flex-grow: 1 !important;
    }

    .flex-shrink-0 {
        -ms-flex-negative: 0 !important;
        flex-shrink: 0 !important;
    }

    .flex-shrink-1 {
        -ms-flex-negative: 1 !important;
        flex-shrink: 1 !important;
    }

    .justify-content-start {
        -webkit-box-pack: start !important;
        -ms-flex-pack: start !important;
        justify-content: flex-start !important;
    }

    .justify-content-end {
        -webkit-box-pack: end !important;
        -ms-flex-pack: end !important;
        justify-content: flex-end !important;
    }

    .justify-content-center {
        -webkit-box-pack: center !important;
        -ms-flex-pack: center !important;
        justify-content: center !important;
    }

    .justify-content-between {
        -webkit-box-pack: justify !important;
        -ms-flex-pack: justify !important;
        justify-content: space-between !important;
    }

    .justify-content-around {
        -ms-flex-pack: distribute !important;
        justify-content: space-around !important;
    }

    .align-items-start {
        -webkit-box-align: start !important;
        -ms-flex-align: start !important;
        align-items: flex-start !important;
    }

    .align-items-end {
        -webkit-box-align: end !important;
        -ms-flex-align: end !important;
        align-items: flex-end !important;
    }

    .align-items-center {
        -webkit-box-align: center !important;
        -ms-flex-align: center !important;
        align-items: center !important;
    }

    .align-items-baseline {
        -webkit-box-align: baseline !important;
        -ms-flex-align: baseline !important;
        align-items: baseline !important;
    }

    .align-items-stretch {
        -webkit-box-align: stretch !important;
        -ms-flex-align: stretch !important;
        align-items: stretch !important;
    }

    .align-content-start {
        -ms-flex-line-pack: start !important;
        align-content: flex-start !important;
    }

    .align-content-end {
        -ms-flex-line-pack: end !important;
        align-content: flex-end !important;
    }

    .align-content-center {
        -ms-flex-line-pack: center !important;
        align-content: center !important;
    }

    .align-content-between {
        -ms-flex-line-pack: justify !important;
        align-content: space-between !important;
    }

    .align-content-around {
        -ms-flex-line-pack: distribute !important;
        align-content: space-around !important;
    }

    .align-content-stretch {
        -ms-flex-line-pack: stretch !important;
        align-content: stretch !important;
    }

    .align-self-auto {
        -ms-flex-item-align: auto !important;
        align-self: auto !important;
    }

    .align-self-start {
        -ms-flex-item-align: start !important;
        align-self: flex-start !important;
    }

    .align-self-end {
        -ms-flex-item-align: end !important;
        align-self: flex-end !important;
    }

    .align-self-center {
        -ms-flex-item-align: center !important;
        align-self: center !important;
    }

    .align-self-baseline {
        -ms-flex-item-align: baseline !important;
        align-self: baseline !important;
    }

    .align-self-stretch {
        -ms-flex-item-align: stretch !important;
        align-self: stretch !important;
    }

    @media (min-width: 576px) {
        .flex-sm-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .flex-sm-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }

        .flex-sm-row-reverse {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: row-reverse !important;
            flex-direction: row-reverse !important;
        }

        .flex-sm-column-reverse {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: column-reverse !important;
            flex-direction: column-reverse !important;
        }

        .flex-sm-wrap {
            -ms-flex-wrap: wrap !important;
            flex-wrap: wrap !important;
        }

        .flex-sm-nowrap {
            -ms-flex-wrap: nowrap !important;
            flex-wrap: nowrap !important;
        }

        .flex-sm-wrap-reverse {
            -ms-flex-wrap: wrap-reverse !important;
            flex-wrap: wrap-reverse !important;
        }

        .flex-sm-fill {
            -webkit-box-flex: 1 !important;
            -ms-flex: 1 1 auto !important;
            flex: 1 1 auto !important;
        }

        .flex-sm-grow-0 {
            -webkit-box-flex: 0 !important;
            -ms-flex-positive: 0 !important;
            flex-grow: 0 !important;
        }

        .flex-sm-grow-1 {
            -webkit-box-flex: 1 !important;
            -ms-flex-positive: 1 !important;
            flex-grow: 1 !important;
        }

        .flex-sm-shrink-0 {
            -ms-flex-negative: 0 !important;
            flex-shrink: 0 !important;
        }

        .flex-sm-shrink-1 {
            -ms-flex-negative: 1 !important;
            flex-shrink: 1 !important;
        }

        .justify-content-sm-start {
            -webkit-box-pack: start !important;
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }

        .justify-content-sm-end {
            -webkit-box-pack: end !important;
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }

        .justify-content-sm-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .justify-content-sm-between {
            -webkit-box-pack: justify !important;
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .justify-content-sm-around {
            -ms-flex-pack: distribute !important;
            justify-content: space-around !important;
        }

        .align-items-sm-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .align-items-sm-end {
            -webkit-box-align: end !important;
            -ms-flex-align: end !important;
            align-items: flex-end !important;
        }

        .align-items-sm-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .align-items-sm-baseline {
            -webkit-box-align: baseline !important;
            -ms-flex-align: baseline !important;
            align-items: baseline !important;
        }

        .align-items-sm-stretch {
            -webkit-box-align: stretch !important;
            -ms-flex-align: stretch !important;
            align-items: stretch !important;
        }

        .align-content-sm-start {
            -ms-flex-line-pack: start !important;
            align-content: flex-start !important;
        }

        .align-content-sm-end {
            -ms-flex-line-pack: end !important;
            align-content: flex-end !important;
        }

        .align-content-sm-center {
            -ms-flex-line-pack: center !important;
            align-content: center !important;
        }

        .align-content-sm-between {
            -ms-flex-line-pack: justify !important;
            align-content: space-between !important;
        }

        .align-content-sm-around {
            -ms-flex-line-pack: distribute !important;
            align-content: space-around !important;
        }

        .align-content-sm-stretch {
            -ms-flex-line-pack: stretch !important;
            align-content: stretch !important;
        }

        .align-self-sm-auto {
            -ms-flex-item-align: auto !important;
            align-self: auto !important;
        }

        .align-self-sm-start {
            -ms-flex-item-align: start !important;
            align-self: flex-start !important;
        }

        .align-self-sm-end {
            -ms-flex-item-align: end !important;
            align-self: flex-end !important;
        }

        .align-self-sm-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .align-self-sm-baseline {
            -ms-flex-item-align: baseline !important;
            align-self: baseline !important;
        }

        .align-self-sm-stretch {
            -ms-flex-item-align: stretch !important;
            align-self: stretch !important;
        }
    }

    @media (min-width: 768px) {
        .flex-md-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .flex-md-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }

        .flex-md-row-reverse {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: row-reverse !important;
            flex-direction: row-reverse !important;
        }

        .flex-md-column-reverse {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: column-reverse !important;
            flex-direction: column-reverse !important;
        }

        .flex-md-wrap {
            -ms-flex-wrap: wrap !important;
            flex-wrap: wrap !important;
        }

        .flex-md-nowrap {
            -ms-flex-wrap: nowrap !important;
            flex-wrap: nowrap !important;
        }

        .flex-md-wrap-reverse {
            -ms-flex-wrap: wrap-reverse !important;
            flex-wrap: wrap-reverse !important;
        }

        .flex-md-fill {
            -webkit-box-flex: 1 !important;
            -ms-flex: 1 1 auto !important;
            flex: 1 1 auto !important;
        }

        .flex-md-grow-0 {
            -webkit-box-flex: 0 !important;
            -ms-flex-positive: 0 !important;
            flex-grow: 0 !important;
        }

        .flex-md-grow-1 {
            -webkit-box-flex: 1 !important;
            -ms-flex-positive: 1 !important;
            flex-grow: 1 !important;
        }

        .flex-md-shrink-0 {
            -ms-flex-negative: 0 !important;
            flex-shrink: 0 !important;
        }

        .flex-md-shrink-1 {
            -ms-flex-negative: 1 !important;
            flex-shrink: 1 !important;
        }

        .justify-content-md-start {
            -webkit-box-pack: start !important;
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }

        .justify-content-md-end {
            -webkit-box-pack: end !important;
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }

        .justify-content-md-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .justify-content-md-between {
            -webkit-box-pack: justify !important;
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .justify-content-md-around {
            -ms-flex-pack: distribute !important;
            justify-content: space-around !important;
        }

        .align-items-md-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .align-items-md-end {
            -webkit-box-align: end !important;
            -ms-flex-align: end !important;
            align-items: flex-end !important;
        }

        .align-items-md-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .align-items-md-baseline {
            -webkit-box-align: baseline !important;
            -ms-flex-align: baseline !important;
            align-items: baseline !important;
        }

        .align-items-md-stretch {
            -webkit-box-align: stretch !important;
            -ms-flex-align: stretch !important;
            align-items: stretch !important;
        }

        .align-content-md-start {
            -ms-flex-line-pack: start !important;
            align-content: flex-start !important;
        }

        .align-content-md-end {
            -ms-flex-line-pack: end !important;
            align-content: flex-end !important;
        }

        .align-content-md-center {
            -ms-flex-line-pack: center !important;
            align-content: center !important;
        }

        .align-content-md-between {
            -ms-flex-line-pack: justify !important;
            align-content: space-between !important;
        }

        .align-content-md-around {
            -ms-flex-line-pack: distribute !important;
            align-content: space-around !important;
        }

        .align-content-md-stretch {
            -ms-flex-line-pack: stretch !important;
            align-content: stretch !important;
        }

        .align-self-md-auto {
            -ms-flex-item-align: auto !important;
            align-self: auto !important;
        }

        .align-self-md-start {
            -ms-flex-item-align: start !important;
            align-self: flex-start !important;
        }

        .align-self-md-end {
            -ms-flex-item-align: end !important;
            align-self: flex-end !important;
        }

        .align-self-md-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .align-self-md-baseline {
            -ms-flex-item-align: baseline !important;
            align-self: baseline !important;
        }

        .align-self-md-stretch {
            -ms-flex-item-align: stretch !important;
            align-self: stretch !important;
        }
    }

    @media (min-width: 992px) {
        .flex-lg-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .flex-lg-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }

        .flex-lg-row-reverse {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: row-reverse !important;
            flex-direction: row-reverse !important;
        }

        .flex-lg-column-reverse {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: column-reverse !important;
            flex-direction: column-reverse !important;
        }

        .flex-lg-wrap {
            -ms-flex-wrap: wrap !important;
            flex-wrap: wrap !important;
        }

        .flex-lg-nowrap {
            -ms-flex-wrap: nowrap !important;
            flex-wrap: nowrap !important;
        }

        .flex-lg-wrap-reverse {
            -ms-flex-wrap: wrap-reverse !important;
            flex-wrap: wrap-reverse !important;
        }

        .flex-lg-fill {
            -webkit-box-flex: 1 !important;
            -ms-flex: 1 1 auto !important;
            flex: 1 1 auto !important;
        }

        .flex-lg-grow-0 {
            -webkit-box-flex: 0 !important;
            -ms-flex-positive: 0 !important;
            flex-grow: 0 !important;
        }

        .flex-lg-grow-1 {
            -webkit-box-flex: 1 !important;
            -ms-flex-positive: 1 !important;
            flex-grow: 1 !important;
        }

        .flex-lg-shrink-0 {
            -ms-flex-negative: 0 !important;
            flex-shrink: 0 !important;
        }

        .flex-lg-shrink-1 {
            -ms-flex-negative: 1 !important;
            flex-shrink: 1 !important;
        }

        .justify-content-lg-start {
            -webkit-box-pack: start !important;
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }

        .justify-content-lg-end {
            -webkit-box-pack: end !important;
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }

        .justify-content-lg-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .justify-content-lg-between {
            -webkit-box-pack: justify !important;
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .justify-content-lg-around {
            -ms-flex-pack: distribute !important;
            justify-content: space-around !important;
        }

        .align-items-lg-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .align-items-lg-end {
            -webkit-box-align: end !important;
            -ms-flex-align: end !important;
            align-items: flex-end !important;
        }

        .align-items-lg-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .align-items-lg-baseline {
            -webkit-box-align: baseline !important;
            -ms-flex-align: baseline !important;
            align-items: baseline !important;
        }

        .align-items-lg-stretch {
            -webkit-box-align: stretch !important;
            -ms-flex-align: stretch !important;
            align-items: stretch !important;
        }

        .align-content-lg-start {
            -ms-flex-line-pack: start !important;
            align-content: flex-start !important;
        }

        .align-content-lg-end {
            -ms-flex-line-pack: end !important;
            align-content: flex-end !important;
        }

        .align-content-lg-center {
            -ms-flex-line-pack: center !important;
            align-content: center !important;
        }

        .align-content-lg-between {
            -ms-flex-line-pack: justify !important;
            align-content: space-between !important;
        }

        .align-content-lg-around {
            -ms-flex-line-pack: distribute !important;
            align-content: space-around !important;
        }

        .align-content-lg-stretch {
            -ms-flex-line-pack: stretch !important;
            align-content: stretch !important;
        }

        .align-self-lg-auto {
            -ms-flex-item-align: auto !important;
            align-self: auto !important;
        }

        .align-self-lg-start {
            -ms-flex-item-align: start !important;
            align-self: flex-start !important;
        }

        .align-self-lg-end {
            -ms-flex-item-align: end !important;
            align-self: flex-end !important;
        }

        .align-self-lg-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .align-self-lg-baseline {
            -ms-flex-item-align: baseline !important;
            align-self: baseline !important;
        }

        .align-self-lg-stretch {
            -ms-flex-item-align: stretch !important;
            align-self: stretch !important;
        }
    }

    @media (min-width: 1200px) {
        .flex-xl-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .flex-xl-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }

        .flex-xl-row-reverse {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: row-reverse !important;
            flex-direction: row-reverse !important;
        }

        .flex-xl-column-reverse {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: column-reverse !important;
            flex-direction: column-reverse !important;
        }

        .flex-xl-wrap {
            -ms-flex-wrap: wrap !important;
            flex-wrap: wrap !important;
        }

        .flex-xl-nowrap {
            -ms-flex-wrap: nowrap !important;
            flex-wrap: nowrap !important;
        }

        .flex-xl-wrap-reverse {
            -ms-flex-wrap: wrap-reverse !important;
            flex-wrap: wrap-reverse !important;
        }

        .flex-xl-fill {
            -webkit-box-flex: 1 !important;
            -ms-flex: 1 1 auto !important;
            flex: 1 1 auto !important;
        }

        .flex-xl-grow-0 {
            -webkit-box-flex: 0 !important;
            -ms-flex-positive: 0 !important;
            flex-grow: 0 !important;
        }

        .flex-xl-grow-1 {
            -webkit-box-flex: 1 !important;
            -ms-flex-positive: 1 !important;
            flex-grow: 1 !important;
        }

        .flex-xl-shrink-0 {
            -ms-flex-negative: 0 !important;
            flex-shrink: 0 !important;
        }

        .flex-xl-shrink-1 {
            -ms-flex-negative: 1 !important;
            flex-shrink: 1 !important;
        }

        .justify-content-xl-start {
            -webkit-box-pack: start !important;
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }

        .justify-content-xl-end {
            -webkit-box-pack: end !important;
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }

        .justify-content-xl-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .justify-content-xl-between {
            -webkit-box-pack: justify !important;
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .justify-content-xl-around {
            -ms-flex-pack: distribute !important;
            justify-content: space-around !important;
        }

        .align-items-xl-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .align-items-xl-end {
            -webkit-box-align: end !important;
            -ms-flex-align: end !important;
            align-items: flex-end !important;
        }

        .align-items-xl-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .align-items-xl-baseline {
            -webkit-box-align: baseline !important;
            -ms-flex-align: baseline !important;
            align-items: baseline !important;
        }

        .align-items-xl-stretch {
            -webkit-box-align: stretch !important;
            -ms-flex-align: stretch !important;
            align-items: stretch !important;
        }

        .align-content-xl-start {
            -ms-flex-line-pack: start !important;
            align-content: flex-start !important;
        }

        .align-content-xl-end {
            -ms-flex-line-pack: end !important;
            align-content: flex-end !important;
        }

        .align-content-xl-center {
            -ms-flex-line-pack: center !important;
            align-content: center !important;
        }

        .align-content-xl-between {
            -ms-flex-line-pack: justify !important;
            align-content: space-between !important;
        }

        .align-content-xl-around {
            -ms-flex-line-pack: distribute !important;
            align-content: space-around !important;
        }

        .align-content-xl-stretch {
            -ms-flex-line-pack: stretch !important;
            align-content: stretch !important;
        }

        .align-self-xl-auto {
            -ms-flex-item-align: auto !important;
            align-self: auto !important;
        }

        .align-self-xl-start {
            -ms-flex-item-align: start !important;
            align-self: flex-start !important;
        }

        .align-self-xl-end {
            -ms-flex-item-align: end !important;
            align-self: flex-end !important;
        }

        .align-self-xl-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .align-self-xl-baseline {
            -ms-flex-item-align: baseline !important;
            align-self: baseline !important;
        }

        .align-self-xl-stretch {
            -ms-flex-item-align: stretch !important;
            align-self: stretch !important;
        }
    }

    @media (min-width: 1400px) {
        .flex-xxl-row {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: row !important;
            flex-direction: row !important;
        }

        .flex-xxl-column {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: normal !important;
            -ms-flex-direction: column !important;
            flex-direction: column !important;
        }

        .flex-xxl-row-reverse {
            -webkit-box-orient: horizontal !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: row-reverse !important;
            flex-direction: row-reverse !important;
        }

        .flex-xxl-column-reverse {
            -webkit-box-orient: vertical !important;
            -webkit-box-direction: reverse !important;
            -ms-flex-direction: column-reverse !important;
            flex-direction: column-reverse !important;
        }

        .flex-xxl-wrap {
            -ms-flex-wrap: wrap !important;
            flex-wrap: wrap !important;
        }

        .flex-xxl-nowrap {
            -ms-flex-wrap: nowrap !important;
            flex-wrap: nowrap !important;
        }

        .flex-xxl-wrap-reverse {
            -ms-flex-wrap: wrap-reverse !important;
            flex-wrap: wrap-reverse !important;
        }

        .flex-xxl-fill {
            -webkit-box-flex: 1 !important;
            -ms-flex: 1 1 auto !important;
            flex: 1 1 auto !important;
        }

        .flex-xxl-grow-0 {
            -webkit-box-flex: 0 !important;
            -ms-flex-positive: 0 !important;
            flex-grow: 0 !important;
        }

        .flex-xxl-grow-1 {
            -webkit-box-flex: 1 !important;
            -ms-flex-positive: 1 !important;
            flex-grow: 1 !important;
        }

        .flex-xxl-shrink-0 {
            -ms-flex-negative: 0 !important;
            flex-shrink: 0 !important;
        }

        .flex-xxl-shrink-1 {
            -ms-flex-negative: 1 !important;
            flex-shrink: 1 !important;
        }

        .justify-content-xxl-start {
            -webkit-box-pack: start !important;
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }

        .justify-content-xxl-end {
            -webkit-box-pack: end !important;
            -ms-flex-pack: end !important;
            justify-content: flex-end !important;
        }

        .justify-content-xxl-center {
            -webkit-box-pack: center !important;
            -ms-flex-pack: center !important;
            justify-content: center !important;
        }

        .justify-content-xxl-between {
            -webkit-box-pack: justify !important;
            -ms-flex-pack: justify !important;
            justify-content: space-between !important;
        }

        .justify-content-xxl-around {
            -ms-flex-pack: distribute !important;
            justify-content: space-around !important;
        }

        .align-items-xxl-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .align-items-xxl-end {
            -webkit-box-align: end !important;
            -ms-flex-align: end !important;
            align-items: flex-end !important;
        }

        .align-items-xxl-center {
            -webkit-box-align: center !important;
            -ms-flex-align: center !important;
            align-items: center !important;
        }

        .align-items-xxl-baseline {
            -webkit-box-align: baseline !important;
            -ms-flex-align: baseline !important;
            align-items: baseline !important;
        }

        .align-items-xxl-stretch {
            -webkit-box-align: stretch !important;
            -ms-flex-align: stretch !important;
            align-items: stretch !important;
        }

        .align-content-xxl-start {
            -ms-flex-line-pack: start !important;
            align-content: flex-start !important;
        }

        .align-content-xxl-end {
            -ms-flex-line-pack: end !important;
            align-content: flex-end !important;
        }

        .align-content-xxl-center {
            -ms-flex-line-pack: center !important;
            align-content: center !important;
        }

        .align-content-xxl-between {
            -ms-flex-line-pack: justify !important;
            align-content: space-between !important;
        }

        .align-content-xxl-around {
            -ms-flex-line-pack: distribute !important;
            align-content: space-around !important;
        }

        .align-content-xxl-stretch {
            -ms-flex-line-pack: stretch !important;
            align-content: stretch !important;
        }

        .align-self-xxl-auto {
            -ms-flex-item-align: auto !important;
            align-self: auto !important;
        }

        .align-self-xxl-start {
            -ms-flex-item-align: start !important;
            align-self: flex-start !important;
        }

        .align-self-xxl-end {
            -ms-flex-item-align: end !important;
            align-self: flex-end !important;
        }

        .align-self-xxl-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .align-self-xxl-baseline {
            -ms-flex-item-align: baseline !important;
            align-self: baseline !important;
        }

        .align-self-xxl-stretch {
            -ms-flex-item-align: stretch !important;
            align-self: stretch !important;
        }
    }

    .float-left {
        float: left !important;
    }

    .float-right {
        float: right !important;
    }

    .float-none {
        float: none !important;
    }

    .w-25 {
        width: 25% !important;
    }

    .w-50 {
        width: 50% !important;
    }

    .w-75 {
        width: 75% !important;
    }

    .w-100 {
        width: 100% !important;
    }

    .w-auto {
        width: auto !important;
    }

    .h-25 {
        height: 25% !important;
    }

    .h-50 {
        height: 50% !important;
    }

    .h-75 {
        height: 75% !important;
    }

    .h-100 {
        height: 100% !important;
    }

    .h-auto {
        height: auto !important;
    }

    .mw-100 {
        max-width: 100% !important;
    }

    .mh-100 {
        max-height: 100% !important;
    }

    .min-vw-100 {
        min-width: 100vw !important;
    }

    .min-vh-100 {
        min-height: 100vh !important;
    }

    .vw-100 {
        width: 100vw !important;
    }

    .vh-100 {
        height: 100vh !important;
    }

    .m-0 {
        margin: 0 !important;
    }

    .mt-0,
    .my-0 {
        margin-top: 0 !important;
    }

    .mr-0,
    .mx-0 {
        margin-right: 0 !important;
    }

    .mb-0,
    .my-0 {
        margin-bottom: 0 !important;
    }

    .ml-0,
    .mx-0 {
        margin-left: 0 !important;
    }

    .m-1 {
        margin: 0.25rem !important;
    }

    .mt-1,
    .my-1 {
        margin-top: 0.25rem !important;
    }

    .mr-1,
    .mx-1 {
        margin-right: 0.25rem !important;
    }

    .mb-1,
    .my-1 {
        margin-bottom: 0.25rem !important;
    }

    .ml-1,
    .mx-1 {
        margin-left: 0.25rem !important;
    }

    .m-2 {
        margin: 0.5rem !important;
    }

    .mt-2,
    .my-2 {
        margin-top: 0.5rem !important;
    }

    .mr-2,
    .mx-2 {
        margin-right: 0.5rem !important;
    }

    .mb-2,
    .my-2 {
        margin-bottom: 0.5rem !important;
    }

    .ml-2,
    .mx-2 {
        margin-left: 0.5rem !important;
    }

    .m-3 {
        margin: 0.75rem !important;
    }

    .mt-3,
    .my-3 {
        margin-top: 0.75rem !important;
    }

    .mr-3,
    .mx-3 {
        margin-right: 0.75rem !important;
    }

    .mb-3,
    .my-3 {
        margin-bottom: 0.75rem !important;
    }

    .ml-3,
    .mx-3 {
        margin-left: 0.75rem !important;
    }

    .m-4 {
        margin: 1rem !important;
    }

    .mt-4,
    .my-4 {
        margin-top: 1rem !important;
    }

    .mr-4,
    .mx-4 {
        margin-right: 1rem !important;
    }

    .mb-4,
    .my-4 {
        margin-bottom: 1rem !important;
    }

    .ml-4,
    .mx-4 {
        margin-left: 1rem !important;
    }

    .m-5 {
        margin: 1.25rem !important;
    }

    .mt-5,
    .my-5 {
        margin-top: 1.25rem !important;
    }

    .mr-5,
    .mx-5 {
        margin-right: 1.25rem !important;
    }

    .mb-5,
    .my-5 {
        margin-bottom: 1.25rem !important;
    }

    .ml-5,
    .mx-5 {
        margin-left: 1.25rem !important;
    }

    .m-6 {
        margin: 1.5rem !important;
    }

    .mt-6,
    .my-6 {
        margin-top: 1.5rem !important;
    }

    .mr-6,
    .mx-6 {
        margin-right: 1.5rem !important;
    }

    .mb-6,
    .my-6 {
        margin-bottom: 1.5rem !important;
    }

    .ml-6,
    .mx-6 {
        margin-left: 1.5rem !important;
    }

    .m-7 {
        margin: 1.75rem !important;
    }

    .mt-7,
    .my-7 {
        margin-top: 1.75rem !important;
    }

    .mr-7,
    .mx-7 {
        margin-right: 1.75rem !important;
    }

    .mb-7,
    .my-7 {
        margin-bottom: 1.75rem !important;
    }

    .ml-7,
    .mx-7 {
        margin-left: 1.75rem !important;
    }

    .m-8 {
        margin: 2rem !important;
    }

    .mt-8,
    .my-8 {
        margin-top: 2rem !important;
    }

    .mr-8,
    .mx-8 {
        margin-right: 2rem !important;
    }

    .mb-8,
    .my-8 {
        margin-bottom: 2rem !important;
    }

    .ml-8,
    .mx-8 {
        margin-left: 2rem !important;
    }

    .m-9 {
        margin: 2.25rem !important;
    }

    .mt-9,
    .my-9 {
        margin-top: 2.25rem !important;
    }

    .mr-9,
    .mx-9 {
        margin-right: 2.25rem !important;
    }

    .mb-9,
    .my-9 {
        margin-bottom: 2.25rem !important;
    }

    .ml-9,
    .mx-9 {
        margin-left: 2.25rem !important;
    }

    .m-10 {
        margin: 2.5rem !important;
    }

    .mt-10,
    .my-10 {
        margin-top: 2.5rem !important;
    }

    .mr-10,
    .mx-10 {
        margin-right: 2.5rem !important;
    }

    .mb-10,
    .my-10 {
        margin-bottom: 2.5rem !important;
    }

    .ml-10,
    .mx-10 {
        margin-left: 2.5rem !important;
    }

    .m-11 {
        margin: 2.75rem !important;
    }

    .mt-11,
    .my-11 {
        margin-top: 2.75rem !important;
    }

    .mr-11,
    .mx-11 {
        margin-right: 2.75rem !important;
    }

    .mb-11,
    .my-11 {
        margin-bottom: 2.75rem !important;
    }

    .ml-11,
    .mx-11 {
        margin-left: 2.75rem !important;
    }

    .m-12 {
        margin: 3rem !important;
    }

    .mt-12,
    .my-12 {
        margin-top: 3rem !important;
    }

    .mr-12,
    .mx-12 {
        margin-right: 3rem !important;
    }

    .mb-12,
    .my-12 {
        margin-bottom: 3rem !important;
    }

    .ml-12,
    .mx-12 {
        margin-left: 3rem !important;
    }

    .m-13 {
        margin: 3.25rem !important;
    }

    .mt-13,
    .my-13 {
        margin-top: 3.25rem !important;
    }

    .mr-13,
    .mx-13 {
        margin-right: 3.25rem !important;
    }

    .mb-13,
    .my-13 {
        margin-bottom: 3.25rem !important;
    }

    .ml-13,
    .mx-13 {
        margin-left: 3.25rem !important;
    }

    .m-14 {
        margin: 3.5rem !important;
    }

    .mt-14,
    .my-14 {
        margin-top: 3.5rem !important;
    }

    .mr-14,
    .mx-14 {
        margin-right: 3.5rem !important;
    }

    .mb-14,
    .my-14 {
        margin-bottom: 3.5rem !important;
    }

    .ml-14,
    .mx-14 {
        margin-left: 3.5rem !important;
    }

    .m-15 {
        margin: 3.75rem !important;
    }

    .mt-15,
    .my-15 {
        margin-top: 3.75rem !important;
    }

    .mr-15,
    .mx-15 {
        margin-right: 3.75rem !important;
    }

    .mb-15,
    .my-15 {
        margin-bottom: 3.75rem !important;
    }

    .ml-15,
    .mx-15 {
        margin-left: 3.75rem !important;
    }

    .m-16 {
        margin: 4rem !important;
    }

    .mt-16,
    .my-16 {
        margin-top: 4rem !important;
    }

    .mr-16,
    .mx-16 {
        margin-right: 4rem !important;
    }

    .mb-16,
    .my-16 {
        margin-bottom: 4rem !important;
    }

    .ml-16,
    .mx-16 {
        margin-left: 4rem !important;
    }

    .m-17 {
        margin: 4.25rem !important;
    }

    .mt-17,
    .my-17 {
        margin-top: 4.25rem !important;
    }

    .mr-17,
    .mx-17 {
        margin-right: 4.25rem !important;
    }

    .mb-17,
    .my-17 {
        margin-bottom: 4.25rem !important;
    }

    .ml-17,
    .mx-17 {
        margin-left: 4.25rem !important;
    }

    .m-18 {
        margin: 4.5rem !important;
    }

    .mt-18,
    .my-18 {
        margin-top: 4.5rem !important;
    }

    .mr-18,
    .mx-18 {
        margin-right: 4.5rem !important;
    }

    .mb-18,
    .my-18 {
        margin-bottom: 4.5rem !important;
    }

    .ml-18,
    .mx-18 {
        margin-left: 4.5rem !important;
    }

    .m-19 {
        margin: 4.75rem !important;
    }

    .mt-19,
    .my-19 {
        margin-top: 4.75rem !important;
    }

    .mr-19,
    .mx-19 {
        margin-right: 4.75rem !important;
    }

    .mb-19,
    .my-19 {
        margin-bottom: 4.75rem !important;
    }

    .ml-19,
    .mx-19 {
        margin-left: 4.75rem !important;
    }

    .m-20 {
        margin: 5rem !important;
    }

    .p-0 {
        padding: 0 !important;
    }

    .pt-0,
    .py-0 {
        padding-top: 0 !important;
    }

    .pr-0,
    .px-0 {
        padding-right: 0 !important;
    }

    .pb-0,
    .py-0 {
        padding-bottom: 0 !important;
    }

    .pl-0,
    .px-0 {
        padding-left: 0 !important;
    }

    .p-1 {
        padding: 0.25rem !important;
    }

    .pt-1,
    .py-1 {
        padding-top: 0.25rem !important;
    }

    .pr-1,
    .px-1 {
        padding-right: 0.25rem !important;
    }

    .pb-1,
    .py-1 {
        padding-bottom: 0.25rem !important;
    }

    .pl-1,
    .px-1 {
        padding-left: 0.25rem !important;
    }

    .p-2 {
        padding: 0.5rem !important;
    }

    .pt-2,
    .py-2 {
        padding-top: 0.5rem !important;
    }

    .pr-2,
    .px-2 {
        padding-right: 0.5rem !important;
    }

    .pb-2,
    .py-2 {
        padding-bottom: 0.5rem !important;
    }

    .pl-2,
    .px-2 {
        padding-left: 0.5rem !important;
    }

    .p-3 {
        padding: 0.75rem !important;
    }

    .pt-3,
    .py-3 {
        padding-top: 0.75rem !important;
    }

    .pr-3,
    .px-3 {
        padding-right: 0.75rem !important;
    }

    .pb-3,
    .py-3 {
        padding-bottom: 0.75rem !important;
    }

    .pl-3,
    .px-3 {
        padding-left: 0.75rem !important;
    }

    .p-4 {
        padding: 1rem !important;
    }

    .pt-4,
    .py-4 {
        padding-top: 1rem !important;
    }

    .pr-4,
    .px-4 {
        padding-right: 1rem !important;
    }

    .pb-4,
    .py-4 {
        padding-bottom: 1rem !important;
    }

    .pl-4,
    .px-4 {
        padding-left: 1rem !important;
    }

    .p-5 {
        padding: 1.25rem !important;
    }

    .pt-5,
    .py-5 {
        padding-top: 1.25rem !important;
    }

    .pr-5,
    .px-5 {
        padding-right: 1.25rem !important;
    }

    .pb-5,
    .py-5 {
        padding-bottom: 1.25rem !important;
    }

    .pl-5,
    .px-5 {
        padding-left: 1.25rem !important;
    }

    .p-6 {
        padding: 1.5rem !important;
    }

    .pt-6,
    .py-6 {
        padding-top: 1.5rem !important;
    }

    .pr-6,
    .px-6 {
        padding-right: 1.5rem !important;
    }

    .pb-6,
    .py-6 {
        padding-bottom: 1.5rem !important;
    }

    .pl-6,
    .px-6 {
        padding-left: 1.5rem !important;
    }

    .p-7 {
        padding: 1.75rem !important;
    }

    .pt-7,
    .py-7 {
        padding-top: 1.75rem !important;
    }

    .pr-7,
    .px-7 {
        padding-right: 1.75rem !important;
    }

    .pb-7,
    .py-7 {
        padding-bottom: 1.75rem !important;
    }

    .pl-7,
    .px-7 {
        padding-left: 1.75rem !important;
    }

    .p-8 {
        padding: 2rem !important;
    }

    .pt-8,
    .py-8 {
        padding-top: 2rem !important;
    }

    .pr-8,
    .px-8 {
        padding-right: 2rem !important;
    }

    .pb-8,
    .py-8 {
        padding-bottom: 2rem !important;
    }

    .pl-8,
    .px-8 {
        padding-left: 2rem !important;
    }

    .p-9 {
        padding: 2.25rem !important;
    }

    .pt-9,
    .py-9 {
        padding-top: 2.25rem !important;
    }

    .pr-9,
    .px-9 {
        padding-right: 2.25rem !important;
    }

    .pb-9,
    .py-9 {
        padding-bottom: 2.25rem !important;
    }

    .pl-9,
    .px-9 {
        padding-left: 2.25rem !important;
    }

    .p-10 {
        padding: 2.5rem !important;
    }

    .pt-10,
    .py-10 {
        padding-top: 2.5rem !important;
    }

    .pr-10,
    .px-10 {
        padding-right: 2.5rem !important;
    }

    .pb-10,
    .py-10 {
        padding-bottom: 2.5rem !important;
    }

    .pl-10,
    .px-10 {
        padding-left: 2.5rem !important;
    }

    .p-11 {
        padding: 2.75rem !important;
    }

    .pt-11,
    .py-11 {
        padding-top: 2.75rem !important;
    }

    .pr-11,
    .px-11 {
        padding-right: 2.75rem !important;
    }

    .pb-11,
    .py-11 {
        padding-bottom: 2.75rem !important;
    }

    .pl-11,
    .px-11 {
        padding-left: 2.75rem !important;
    }

    .p-12 {
        padding: 3rem !important;
    }

    .pt-12,
    .py-12 {
        padding-top: 3rem !important;
    }

    .pr-12,
    .px-12 {
        padding-right: 3rem !important;
    }

    .pb-12,
    .py-12 {
        padding-bottom: 3rem !important;
    }

    .pl-12,
    .px-12 {
        padding-left: 3rem !important;
    }

    .p-13 {
        padding: 3.25rem !important;
    }

    .pt-13,
    .py-13 {
        padding-top: 3.25rem !important;
    }

    .pr-13,
    .px-13 {
        padding-right: 3.25rem !important;
    }

    .pb-13,
    .py-13 {
        padding-bottom: 3.25rem !important;
    }

    .pl-13,
    .px-13 {
        padding-left: 3.25rem !important;
    }

    .p-14 {
        padding: 3.5rem !important;
    }

    .pt-14,
    .py-14 {
        padding-top: 3.5rem !important;
    }

    .pr-14,
    .px-14 {
        padding-right: 3.5rem !important;
    }

    .pb-14,
    .py-14 {
        padding-bottom: 3.5rem !important;
    }

    .pl-14,
    .px-14 {
        padding-left: 3.5rem !important;
    }

    .p-15 {
        padding: 3.75rem !important;
    }

    .pt-15,
    .py-15 {
        padding-top: 3.75rem !important;
    }

    .pr-15,
    .px-15 {
        padding-right: 3.75rem !important;
    }

    .pb-15,
    .py-15 {
        padding-bottom: 3.75rem !important;
    }

    .pl-15,
    .px-15 {
        padding-left: 3.75rem !important;
    }

    .p-16 {
        padding: 4rem !important;
    }

    .pt-16,
    .py-16 {
        padding-top: 4rem !important;
    }

    .pr-16,
    .px-16 {
        padding-right: 4rem !important;
    }

    .pb-16,
    .py-16 {
        padding-bottom: 4rem !important;
    }

    .pl-16,
    .px-16 {
        padding-left: 4rem !important;
    }

    .p-17 {
        padding: 4.25rem !important;
    }

    .pt-17,
    .py-17 {
        padding-top: 4.25rem !important;
    }

    .pr-17,
    .px-17 {
        padding-right: 4.25rem !important;
    }

    .pb-17,
    .py-17 {
        padding-bottom: 4.25rem !important;
    }

    .pl-17,
    .px-17 {
        padding-left: 4.25rem !important;
    }

    .p-18 {
        padding: 4.5rem !important;
    }

    .pt-18,
    .py-18 {
        padding-top: 4.5rem !important;
    }

    .pr-18,
    .px-18 {
        padding-right: 4.5rem !important;
    }

    .pb-18,
    .py-18 {
        padding-bottom: 4.5rem !important;
    }

    .pl-18,
    .px-18 {
        padding-left: 4.5rem !important;
    }

    .p-19 {
        padding: 4.75rem !important;
    }

    .pt-19,
    .py-19 {
        padding-top: 4.75rem !important;
    }

    .pr-19,
    .px-19 {
        padding-right: 4.75rem !important;
    }

    .pb-19,
    .py-19 {
        padding-bottom: 4.75rem !important;
    }

    .pl-19,
    .px-19 {
        padding-left: 4.75rem !important;
    }

    .p-20 {
        padding: 5rem !important;
    }

    .pt-20,
    .py-20 {
        padding-top: 5rem !important;
    }

    .pr-20,
    .px-20 {
        padding-right: 5rem !important;
    }

    .pb-20,
    .py-20 {
        padding-bottom: 5rem !important;
    }

    .pl-20,
    .px-20 {
        padding-left: 5rem !important;
    }

    .m-auto {
        margin: auto !important;
    }

    .mt-auto,
    .my-auto {
        margin-top: auto !important;
    }

    .mr-auto,
    .mx-auto {
        margin-right: auto !important;
    }

    .mb-auto,
    .my-auto {
        margin-bottom: auto !important;
    }

    .ml-auto,
    .mx-auto {
        margin-left: auto !important;
    }

    .stretched-link::after {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1;
        pointer-events: auto;
        content: "";
        background-color: rgba(0, 0, 0, 0);
    }

    .text-monospace {
        font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace !important;
    }

    .text-justify {
        text-align: justify !important;
    }

    .text-wrap {
        white-space: normal !important;
    }

    .text-nowrap {
        white-space: nowrap !important;
    }

    .text-truncate {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .text-left {
        text-align: left !important;
    }

    .text-right {
        text-align: right !important;
    }

    .text-center {
        text-align: center !important;
    }


    .text-lowercase {
        text-transform: lowercase !important;
    }

    .text-uppercase {
        text-transform: uppercase !important;
    }

    .text-capitalize {
        text-transform: capitalize !important;
    }

    .font-weight-light {
        font-weight: 300 !important;
    }

    .font-weight-lighter {
        font-weight: lighter !important;
    }

    .font-weight-normal {
        font-weight: 400 !important;
    }

    .font-weight-bold {
        font-weight: 500 !important;
    }

    .font-weight-bolder {
        font-weight: 600 !important;
    }

    .font-italic {
        font-style: italic !important;
    }

    .text-white {
        color: #ffffff !important;
    }

    .text-primary {
        color: #3eaae8 !important;
    }

    a.text-primary:hover,
    a.text-primary:focus {
        color: #33192c !important;
    }

    .text-secondary {
        color: #3252DF !important;
    }

    a.text-secondary:hover,
    a.text-secondary:focus {
        color: #b4bad3 !important;
    }

    .text-success {
        color: #1BC5BD !important;
    }

    a.text-success:hover,
    a.text-success:focus {
        color: #12827c !important;
    }

    .text-info {
        color: #8950FC !important;
    }

    a.text-info:hover,
    a.text-info:focus {
        color: #5605fb !important;
    }

    .text-warning {
        color: #FFA800 !important;
    }

    a.text-warning:hover,
    a.text-warning:focus {
        color: #b37600 !important;
    }

    .text-danger {
        color: #F64E60 !important;
    }

    a.text-danger:hover,
    a.text-danger:focus {
        color: #ec0c24 !important;
    }

    .text-light {
        color: #F3F6F9 !important;
    }

    a.text-light:hover,
    a.text-light:focus {
        color: #c0d0e0 !important;
    }

    .text-dark {
        color: #181C32 !important;
    }

    a.text-dark:hover,
    a.text-dark:focus {
        color: black !important;
    }

    .text-white {
        color: #ffffff !important;
    }

    a.text-white:hover,
    a.text-white:focus {
        color: #d9d9d9 !important;
    }

    .text-body {
        color: #3F4254 !important;
    }

    .text-muted {
        color: #B5B5C3 !important;
    }

    .text-black-50 {
        color: rgba(0, 0, 0, 0.5) !important;
    }

    .text-white-50 {
        color: rgba(255, 255, 255, 0.5) !important;
    }

    .text-hide {
        font: 0/0 a;
        color: transparent;
        text-shadow: none;
        background-color: transparent;
        border: 0;
    }

    .text-decoration-none {
        text-decoration: none !important;
    }

    .flex-root {
        -webkit-box-flex: 1;
        flex: 1;
        -ms-flex: 1 0 0px;
    }

    .flex-column-auto {
        -webkit-box-flex: 0;
        -ms-flex: none;
        flex: none;
    }

    .flex-column-fluid {
        -webkit-box-flex: 1;
        -ms-flex: 1 0 auto;
        flex: 1 0 auto;
    }

    .flex-row-auto {
        -webkit-box-flex: 0;
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
    }

    .flex-row-fluid {
        -webkit-box-flex: 1;
        flex: 1 auto;
        -ms-flex: 1 0 0px;
        min-width: 0;
    }

    @media screen and (-ms-high-contrast: active),
    (-ms-high-contrast: none) {
        .flex-row-fluid {
            min-width: none;
        }
    }

    .flex-center {
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    @media (min-width: 576px) {
        .flex-sm-root {
            -webkit-box-flex: 1;
            flex: 1;
            -ms-flex: 1 0 0px;
        }

        .flex-sm-column-auto {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .flex-sm-column-fluid {
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
        }

        .flex-sm-row-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .flex-sm-row-fluid {
            -webkit-box-flex: 1;
            flex: 1 auto;
            -ms-flex: 1 0 0px;
            min-width: 0;
        }
    }

    @media screen and (min-width: 576px) and (-ms-high-contrast: active),
    (min-width: 576px) and (-ms-high-contrast: none) {
        .flex-sm-row-fluid {
            min-width: none;
        }
    }

    @media (min-width: 576px) {
        .flex-sm-center {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    @media (min-width: 768px) {
        .flex-md-root {
            -webkit-box-flex: 1;
            flex: 1;
            -ms-flex: 1 0 0px;
        }

        .flex-md-column-auto {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .flex-md-column-fluid {
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
        }

        .flex-md-row-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .flex-md-row-fluid {
            -webkit-box-flex: 1;
            flex: 1 auto;
            -ms-flex: 1 0 0px;
            min-width: 0;
        }
    }

    @media screen and (min-width: 768px) and (-ms-high-contrast: active),
    (min-width: 768px) and (-ms-high-contrast: none) {
        .flex-md-row-fluid {
            min-width: none;
        }
    }

    @media (min-width: 768px) {
        .flex-md-center {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    @media (min-width: 992px) {
        .flex-lg-root {
            -webkit-box-flex: 1;
            flex: 1;
            -ms-flex: 1 0 0px;
        }

        .flex-lg-column-auto {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .flex-lg-column-fluid {
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
        }

        .flex-lg-row-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .flex-lg-row-fluid {
            -webkit-box-flex: 1;
            flex: 1 auto;
            -ms-flex: 1 0 0px;
            min-width: 0;
        }
    }

    @media screen and (min-width: 992px) and (-ms-high-contrast: active),
    (min-width: 992px) and (-ms-high-contrast: none) {
        .flex-lg-row-fluid {
            min-width: none;
        }
    }

    @media (min-width: 992px) {
        .flex-lg-center {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    @media (min-width: 1200px) {
        .flex-xl-root {
            -webkit-box-flex: 1;
            flex: 1;
            -ms-flex: 1 0 0px;
        }

        .flex-xl-column-auto {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .flex-xl-column-fluid {
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
        }

        .flex-xl-row-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .flex-xl-row-fluid {
            -webkit-box-flex: 1;
            flex: 1 auto;
            -ms-flex: 1 0 0px;
            min-width: 0;
        }
    }

    @media screen and (min-width: 1200px) and (-ms-high-contrast: active),
    (min-width: 1200px) and (-ms-high-contrast: none) {
        .flex-xl-row-fluid {
            min-width: none;
        }
    }

    @media (min-width: 1200px) {
        .flex-xl-center {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    @media (min-width: 1400px) {
        .flex-xxl-root {
            -webkit-box-flex: 1;
            flex: 1;
            -ms-flex: 1 0 0px;
        }

        .flex-xxl-column-auto {
            -webkit-box-flex: 0;
            -ms-flex: none;
            flex: none;
        }

        .flex-xxl-column-fluid {
            -webkit-box-flex: 1;
            -ms-flex: 1 0 auto;
            flex: 1 0 auto;
        }

        .flex-xxl-row-auto {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
        }

        .flex-xxl-row-fluid {
            -webkit-box-flex: 1;
            flex: 1 auto;
            -ms-flex: 1 0 0px;
            min-width: 0;
        }
    }

    @media screen and (min-width: 1400px) and (-ms-high-contrast: active),
    (min-width: 1400px) and (-ms-high-contrast: none) {
        .flex-xxl-row-fluid {
            min-width: none;
        }
    }

    @media (min-width: 1400px) {
        .flex-xxl-center {
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    .rounded-top-0 {
        border-top-left-radius: 0 !important;
        border-top-right-radius: 0 !important;
    }

    .rounded-bottom-0 {
        border-bottom-left-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
    }

    .rounded-left-0 {
        border-top-left-radius: 0 !important;
        border-bottom-left-radius: 0 !important;
    }

    .rounded-right-0 {
        border-top-right-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
    }

    .border-transparent {
        border-color: transparent !important;
    }

    .border-x {
        border-left: 1px solid #EBEDF3 !important;
        border-right: 1px solid #EBEDF3 !important;
    }

    .border-y {
        border-top: 1px solid #EBEDF3 !important;
        border-bottom: 1px solid #EBEDF3 !important;
    }

    .border-x-0 {
        border-left: 0 !important;
        border-right: 0 !important;
    }

    .border-y-0 {
        border-top: 0 !important;
        border-bottom: 0 !important;
    }

    .border-1 {
        border-width: 1px !important;
    }

    .border-2 {
        border-width: 2px !important;
    }

    .border-3 {
        border-width: 3px !important;
    }

    .border-4 {
        border-width: 4px !important;
    }

    .border-5 {
        border-width: 5px !important;
    }

    @media (min-width: 576px) {
        .border-x-sm {
            border-left: 1px solid #EBEDF3 !important;
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-y-sm {
            border-top: 1px solid #EBEDF3 !important;
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-x-sm-0 {
            border-left: 0 !important;
            border-right: 0 !important;
        }

        .border-y-sm-0 {
            border-top: 0 !important;
            border-bottom: 0 !important;
        }

        .border-sm {
            border: 1px solid #EBEDF3 !important;
        }

        .border-top-sm {
            border-top: 1px solid #EBEDF3 !important;
        }

        .border-right-sm {
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-bottom-sm {
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-left-sm {
            border-left: 1px solid #EBEDF3 !important;
        }

        .border-sm-0 {
            border: 0 !important;
        }

        .border-top-sm-0 {
            border-top: 0 !important;
        }

        .border-right-sm-0 {
            border-right: 0 !important;
        }

        .border-bottom-sm-0 {
            border-bottom: 0 !important;
        }

        .border-left-sm-0 {
            border-left: 0 !important;
        }

        .border-sm-1 {
            border-width: 1px !important;
        }

        .border-sm-2 {
            border-width: 2px !important;
        }

        .border-sm-3 {
            border-width: 3px !important;
        }

        .border-sm-4 {
            border-width: 4px !important;
        }

        .border-sm-5 {
            border-width: 5px !important;
        }
    }

    @media (min-width: 768px) {
        .border-x-md {
            border-left: 1px solid #EBEDF3 !important;
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-y-md {
            border-top: 1px solid #EBEDF3 !important;
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-x-md-0 {
            border-left: 0 !important;
            border-right: 0 !important;
        }

        .border-y-md-0 {
            border-top: 0 !important;
            border-bottom: 0 !important;
        }

        .border-md {
            border: 1px solid #EBEDF3 !important;
        }

        .border-top-md {
            border-top: 1px solid #EBEDF3 !important;
        }

        .border-right-md {
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-bottom-md {
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-left-md {
            border-left: 1px solid #EBEDF3 !important;
        }

        .border-md-0 {
            border: 0 !important;
        }

        .border-top-md-0 {
            border-top: 0 !important;
        }

        .border-right-md-0 {
            border-right: 0 !important;
        }

        .border-bottom-md-0 {
            border-bottom: 0 !important;
        }

        .border-left-md-0 {
            border-left: 0 !important;
        }

        .border-md-1 {
            border-width: 1px !important;
        }

        .border-md-2 {
            border-width: 2px !important;
        }

        .border-md-3 {
            border-width: 3px !important;
        }

        .border-md-4 {
            border-width: 4px !important;
        }

        .border-md-5 {
            border-width: 5px !important;
        }
    }

    @media (min-width: 992px) {
        .border-x-lg {
            border-left: 1px solid #EBEDF3 !important;
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-y-lg {
            border-top: 1px solid #EBEDF3 !important;
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-x-lg-0 {
            border-left: 0 !important;
            border-right: 0 !important;
        }

        .border-y-lg-0 {
            border-top: 0 !important;
            border-bottom: 0 !important;
        }

        .border-lg {
            border: 1px solid #EBEDF3 !important;
        }

        .border-top-lg {
            border-top: 1px solid #EBEDF3 !important;
        }

        .border-right-lg {
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-bottom-lg {
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-left-lg {
            border-left: 1px solid #EBEDF3 !important;
        }

        .border-lg-0 {
            border: 0 !important;
        }

        .border-top-lg-0 {
            border-top: 0 !important;
        }

        .border-right-lg-0 {
            border-right: 0 !important;
        }

        .border-bottom-lg-0 {
            border-bottom: 0 !important;
        }

        .border-left-lg-0 {
            border-left: 0 !important;
        }

        .border-lg-1 {
            border-width: 1px !important;
        }

        .border-lg-2 {
            border-width: 2px !important;
        }

        .border-lg-3 {
            border-width: 3px !important;
        }

        .border-lg-4 {
            border-width: 4px !important;
        }

        .border-lg-5 {
            border-width: 5px !important;
        }
    }

    @media (min-width: 1200px) {
        .border-x-xl {
            border-left: 1px solid #EBEDF3 !important;
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-y-xl {
            border-top: 1px solid #EBEDF3 !important;
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-x-xl-0 {
            border-left: 0 !important;
            border-right: 0 !important;
        }

        .border-y-xl-0 {
            border-top: 0 !important;
            border-bottom: 0 !important;
        }

        .border-xl {
            border: 1px solid #EBEDF3 !important;
        }

        .border-top-xl {
            border-top: 1px solid #EBEDF3 !important;
        }

        .border-right-xl {
            border-right: 1px solid #EBEDF3 !important;
        }

        .border-bottom-xl {
            border-bottom: 1px solid #EBEDF3 !important;
        }

        .border-left-xl {
            border-left: 1px solid #EBEDF3 !important;
        }

        .border-xl-0 {
            border: 0 !important;
        }

        .border-top-xl-0 {
            border-top: 0 !important;
        }

        .border-right-xl-0 {
            border-right: 0 !important;
        }

        .border-bottom-xl-0 {
            border-bottom: 0 !important;
        }

        .border-left-xl-0 {
            border-left: 0 !important;
        }

        .border-xl-1 {
            border-width: 1px !important;
        }

        .border-xl-2 {
            border-width: 2px !important;
        }

        .border-xl-3 {
            border-width: 3px !important;
        }

        .border-xl-4 {
            border-width: 4px !important;
        }

        .border-xl-5 {
            border-width: 5px !important;
        }
    }

    .border-primary {
        border-color: #3eaae8 !important;
    }

    .border-top-primary {
        border-top-color: #3eaae8 !important;
    }

    .border-right-primary {
        border-right-color: #3eaae8 !important;
    }

    .border-bottom-primary {
        border-bottom-color: #3eaae8 !important;
    }

    .border-left-primary {
        border-left-color: #3eaae8 !important;
    }

    .border-secondary {
        border-color: #3252DF !important;
    }

    .border-top-secondary {
        border-top-color: #3252DF !important;
    }

    .border-right-secondary {
        border-right-color: #3252DF !important;
    }

    .border-bottom-secondary {
        border-bottom-color: #3252DF !important;
    }

    .border-left-secondary {
        border-left-color: #3252DF !important;
    }

    .border-success {
        border-color: #1BC5BD !important;
    }

    .border-top-success {
        border-top-color: #1BC5BD !important;
    }

    .border-right-success {
        border-right-color: #1BC5BD !important;
    }

    .border-bottom-success {
        border-bottom-color: #1BC5BD !important;
    }

    .border-left-success {
        border-left-color: #1BC5BD !important;
    }

    .border-info {
        border-color: #8950FC !important;
    }

    .border-top-info {
        border-top-color: #8950FC !important;
    }

    .border-right-info {
        border-right-color: #8950FC !important;
    }

    .border-bottom-info {
        border-bottom-color: #8950FC !important;
    }

    .border-left-info {
        border-left-color: #8950FC !important;
    }

    .border-warning {
        border-color: #FFA800 !important;
    }

    .border-top-warning {
        border-top-color: #FFA800 !important;
    }

    .border-right-warning {
        border-right-color: #FFA800 !important;
    }

    .border-bottom-warning {
        border-bottom-color: #FFA800 !important;
    }

    .border-left-warning {
        border-left-color: #FFA800 !important;
    }

    .border-danger {
        border-color: #F64E60 !important;
    }

    .border-top-danger {
        border-top-color: #F64E60 !important;
    }

    .border-right-danger {
        border-right-color: #F64E60 !important;
    }

    .border-bottom-danger {
        border-bottom-color: #F64E60 !important;
    }

    .border-left-danger {
        border-left-color: #F64E60 !important;
    }

    .border-light {
        border-color: #F3F6F9 !important;
    }

    .border-top-light {
        border-top-color: #F3F6F9 !important;
    }

    .border-right-light {
        border-right-color: #F3F6F9 !important;
    }

    .border-bottom-light {
        border-bottom-color: #F3F6F9 !important;
    }

    .border-left-light {
        border-left-color: #F3F6F9 !important;
    }

    .border-dark {
        border-color: #181C32 !important;
    }

    .border-top-dark {
        border-top-color: #181C32 !important;
    }

    .border-right-dark {
        border-right-color: #181C32 !important;
    }

    .border-bottom-dark {
        border-bottom-color: #181C32 !important;
    }

    .border-left-dark {
        border-left-color: #181C32 !important;
    }

    .border-white {
        border-color: #ffffff !important;
    }

    .border-top-white {
        border-top-color: #ffffff !important;
    }

    .border-right-white {
        border-right-color: #ffffff !important;
    }

    .border-bottom-white {
        border-bottom-color: #ffffff !important;
    }

    .border-left-white {
        border-left-color: #ffffff !important;
    }

    .border-light-white {
        border-color: #ffffff !important;
    }

    .border-top-light-white {
        border-top-color: #ffffff !important;
    }

    .border-right-light-white {
        border-right-color: #ffffff !important;
    }

    .border-bottom-light-white {
        border-bottom-color: #ffffff !important;
    }

    .border-left-light-white {
        border-left-color: #ffffff !important;
    }

    .border-light-primary {
        border-color: #F4E1F0 !important;
    }

    .border-top-light-primary {
        border-top-color: #F4E1F0 !important;
    }

    .border-right-light-primary {
        border-right-color: #F4E1F0 !important;
    }

    .border-bottom-light-primary {
        border-bottom-color: #F4E1F0 !important;
    }

    .border-left-light-primary {
        border-left-color: #F4E1F0 !important;
    }

    .border-light-secondary {
        border-color: #EBEDF3 !important;
    }

    .border-top-light-secondary {
        border-top-color: #EBEDF3 !important;
    }

    .border-right-light-secondary {
        border-right-color: #EBEDF3 !important;
    }

    .border-bottom-light-secondary {
        border-bottom-color: #EBEDF3 !important;
    }

    .border-left-light-secondary {
        border-left-color: #EBEDF3 !important;
    }

    .border-light-success {
        border-color: #C9F7F5 !important;
    }

    .border-top-light-success {
        border-top-color: #C9F7F5 !important;
    }

    .border-right-light-success {
        border-right-color: #C9F7F5 !important;
    }

    .border-bottom-light-success {
        border-bottom-color: #C9F7F5 !important;
    }

    .border-left-light-success {
        border-left-color: #C9F7F5 !important;
    }

    .border-light-info {
        border-color: #EEE5FF !important;
    }

    .border-top-light-info {
        border-top-color: #EEE5FF !important;
    }

    .border-right-light-info {
        border-right-color: #EEE5FF !important;
    }

    .border-bottom-light-info {
        border-bottom-color: #EEE5FF !important;
    }

    .border-left-light-info {
        border-left-color: #EEE5FF !important;
    }

    .border-light-warning {
        border-color: #FFF4DE !important;
    }

    .border-top-light-warning {
        border-top-color: #FFF4DE !important;
    }

    .border-right-light-warning {
        border-right-color: #FFF4DE !important;
    }

    .border-bottom-light-warning {
        border-bottom-color: #FFF4DE !important;
    }

    .border-left-light-warning {
        border-left-color: #FFF4DE !important;
    }

    .border-light-danger {
        border-color: #FFE2E5 !important;
    }

    .border-top-light-danger {
        border-top-color: #FFE2E5 !important;
    }

    .border-right-light-danger {
        border-right-color: #FFE2E5 !important;
    }

    .border-bottom-light-danger {
        border-bottom-color: #FFE2E5 !important;
    }

    .border-left-light-danger {
        border-left-color: #FFE2E5 !important;
    }

    .border-light-light {
        border-color: #F3F6F9 !important;
    }

    .border-top-light-light {
        border-top-color: #F3F6F9 !important;
    }

    .border-right-light-light {
        border-right-color: #F3F6F9 !important;
    }

    .border-bottom-light-light {
        border-bottom-color: #F3F6F9 !important;
    }

    .border-left-light-light {
        border-left-color: #F3F6F9 !important;
    }

    .border-light-dark {
        border-color: #D1D3E0 !important;
    }

    .border-top-light-dark {
        border-top-color: #D1D3E0 !important;
    }

    .border-right-light-dark {
        border-right-color: #D1D3E0 !important;
    }

    .border-bottom-light-dark {
        border-bottom-color: #D1D3E0 !important;
    }

    .border-left-light-dark {
        border-left-color: #D1D3E0 !important;
    }

    .rounded-top-sm {
        border-top-left-radius: 0.28rem !important;
        border-top-right-radius: 0.28rem !important;
    }

    .rounded-top-left-sm {
        border-top-left-radius: 0.28rem !important;
    }

    .rounded-top-right-sm {
        border-top-right-radius: 0.28rem !important;
    }

    .rounded-bottom-sm {
        border-bottom-left-radius: 0.28rem !important;
        border-bottom-right-radius: 0.28rem !important;
    }

    .rounded-bottom-left-sm {
        border-bottom-left-radius: 0.28rem !important;
    }

    .rounded-bottom-right-sm {
        border-bottom-right-radius: 0.28rem !important;
    }

    .rounded-top {
        border-top-left-radius: 0.675rem !important;
        border-top-right-radius: 0.675rem !important;
    }

    .rounded-top-left {
        border-top-left-radius: 0.675rem !important;
    }

    .rounded-top-right {
        border-top-right-radius: 0.675rem !important;
    }

    .rounded-bottom {
        border-bottom-left-radius: 0.675rem !important;
        border-bottom-right-radius: 0.675rem !important;
    }

    .rounded-bottom-left {
        border-bottom-left-radius: 0.675rem !important;
    }

    .rounded-bottom-right {
        border-bottom-right-radius: 0.675rem !important;
    }

    .rounded-xl {
        border-radius: 1.25rem !important;
    }

    .rounded-top-xl {
        border-top-left-radius: 1.25rem !important;
        border-top-right-radius: 1.25rem !important;
    }

    .rounded-top-left-xl {
        border-top-left-radius: 1.25rem !important;
    }

    .rounded-top-right-xl {
        border-top-right-radius: 1.25rem !important;
    }

    .rounded-bottom-xl {
        border-bottom-left-radius: 1.25rem !important;
        border-bottom-right-radius: 1.25rem !important;
    }

    .rounded-bottom-left-xl {
        border-bottom-left-radius: 1.25rem !important;
    }

    .rounded-bottom-right-xl {
        border-bottom-right-radius: 1.25rem !important;
    }

    .shadow-xs {
        -webkit-box-shadow: 0 0.25rem 0.5rem 0 rgba(0, 0, 0, 0.05);
        box-shadow: 0 0.25rem 0.5rem 0 rgba(0, 0, 0, 0.05);
    }

    .left-0 {
        left: 0 !important;
    }

    .right-0 {
        right: 0 !important;
    }

    .top-0 {
        top: 0 !important;
    }

    .bottom-0 {
        bottom: 0 !important;
    }

    .text-white {
        color: #ffffff !important;
    }

    .text-inverse-white {
        color: #3F4254 !important;
    }

    .text-light-white {
        color: #ffffff !important;
    }

    .text-primary {
        color: #3eaae8 !important;
    }

    .text-inverse-primary {
        color: #FFFFFF !important;
    }

    .text-light-primary {
        color: #F4E1F0 !important;
    }

    .text-secondary {
        color: #181C32 !important;
    }

    .text-inverse-secondary {
        color: #3F4254 !important;
    }

    .text-light-secondary {
        color: #EBEDF3 !important;
    }

    .text-success {
        color: #1BC5BD !important;
    }

    .text-inverse-success {
        color: #ffffff !important;
    }

    .text-light-success {
        color: #C9F7F5 !important;
    }

    .text-info {
        color: #8950FC !important;
    }

    .text-inverse-info {
        color: #ffffff !important;
    }

    .text-light-info {
        color: #EEE5FF !important;
    }

    .text-warning {
        color: #FFA800 !important;
    }

    .text-inverse-warning {
        color: #ffffff !important;
    }

    .text-light-warning {
        color: #FFF4DE !important;
    }

    .text-danger {
        color: #F64E60 !important;
    }

    .text-inverse-danger {
        color: #ffffff !important;
    }

    .text-light-danger {
        color: #FFE2E5 !important;
    }

    .text-light {
        color: #F3F6F9 !important;
    }

    .text-inverse-light {
        color: #7E8299 !important;
    }

    .text-light-light {
        color: #F3F6F9 !important;
    }

    .text-dark {
        color: #181C32 !important;
    }

    .text-inverse-dark {
        color: #ffffff !important;
    }

    .text-light-dark {
        color: #D1D3E0 !important;
    }

    .text-dark-75 {
        color: #3F4254 !important;
    }

    .text-inverse-dark-75 {
        color:  !important;
    }

    .text-light-dark-75 {
        color:  !important;
    }

    .text-dark-65 {
        color: #5E6278 !important;
    }

    .text-inverse-dark-65 {
        color:  !important;
    }

    .text-light-dark-65 {
        color:  !important;
    }

    .text-dark-50 {
        color: #7E8299 !important;
    }

    .text-inverse-dark-50 {
        color:  !important;
    }

    .text-light-dark-50 {
        color:  !important;
    }

    .text-dark-25 {
        color: #D1D3E0 !important;
    }

    .text-inverse-dark-25 {
        color:  !important;
    }

    .text-light-dark-25 {
        color:  !important;
    }

    .font-size-base {
        font-size: 1rem;
    }

    .font-size-sm {
        font-size: 0.925rem;
    }

    .font-size-xs {
        font-size: 0.8rem;
    }

    .font-size-lg {
        font-size: 1.08rem;
    }

    .font-size-h1 {
        font-size: 2rem !important;
    }

    .font-size-h2 {
        font-size: 1.75rem !important;
    }

    .font-size-h3 {
        font-size: 1.5rem !important;
    }

    .font-size-h4 {
        font-size: 1.35rem !important;
    }

    .font-size-h5 {
        font-size: 1.25rem !important;
    }

    .font-size-h6 {
        font-size: 1.175rem !important;
    }

    .display1 {
        font-size: 5.5rem !important;
    }

    .display2 {
        font-size: 4.5rem !important;
    }

    .display3 {
        font-size: 3.5rem !important;
    }

    .display4 {
        font-size: 2.5rem !important;
    }

    .display5 {
        font-size: 2.25rem !important;
    }

    @media (min-width: 576px) {
        .font-size-h1-sm {
            font-size: 2rem !important;
        }

        .font-size-h2-sm {
            font-size: 1.75rem !important;
        }

        .font-size-h3-sm {
            font-size: 1.5rem !important;
        }

        .font-size-h4-sm {
            font-size: 1.35rem !important;
        }

        .font-size-h5-sm {
            font-size: 1.25rem !important;
        }

        .font-size-h6-sm {
            font-size: 1.175rem !important;
        }

        .display1-sm {
            font-size: 5.5rem !important;
        }

        .display2-sm {
            font-size: 4.5rem !important;
        }

        .display3-sm {
            font-size: 3.5rem !important;
        }

        .display4-sm {
            font-size: 2.5rem !important;
        }

        .display5-sm {
            font-size: 2.25rem !important;
        }
    }

    @media (min-width: 768px) {
        .font-size-h1-md {
            font-size: 2rem !important;
        }

        .font-size-h2-md {
            font-size: 1.75rem !important;
        }

        .font-size-h3-md {
            font-size: 1.5rem !important;
        }

        .font-size-h4-md {
            font-size: 1.35rem !important;
        }

        .font-size-h5-md {
            font-size: 1.25rem !important;
        }

        .font-size-h6-md {
            font-size: 1.175rem !important;
        }

        .display1-md {
            font-size: 5.5rem !important;
        }

        .display2-md {
            font-size: 4.5rem !important;
        }

        .display3-md {
            font-size: 3.5rem !important;
        }

        .display4-md {
            font-size: 2.5rem !important;
        }

        .display5-md {
            font-size: 2.25rem !important;
        }
    }

    @media (min-width: 992px) {
        .font-size-h1-lg {
            font-size: 2rem !important;
        }

        .font-size-h2-lg {
            font-size: 1.75rem !important;
        }

        .font-size-h3-lg {
            font-size: 1.5rem !important;
        }

        .font-size-h4-lg {
            font-size: 1.35rem !important;
        }

        .font-size-h5-lg {
            font-size: 1.25rem !important;
        }

        .font-size-h6-lg {
            font-size: 1.175rem !important;
        }

        .display1-lg {
            font-size: 5.5rem !important;
        }

        .display2-lg {
            font-size: 4.5rem !important;
        }

        .display3-lg {
            font-size: 3.5rem !important;
        }

        .display4-lg {
            font-size: 2.5rem !important;
        }

        .display5-lg {
            font-size: 2.25rem !important;
        }
    }

    @media (min-width: 1200px) {
        .font-size-h1-xl {
            font-size: 2rem !important;
        }

        .font-size-h2-xl {
            font-size: 1.75rem !important;
        }

        .font-size-h3-xl {
            font-size: 1.5rem !important;
        }

        .font-size-h4-xl {
            font-size: 1.35rem !important;
        }

        .font-size-h5-xl {
            font-size: 1.25rem !important;
        }

        .font-size-h6-xl {
            font-size: 1.175rem !important;
        }

        .display1-xl {
            font-size: 5.5rem !important;
        }

        .display2-xl {
            font-size: 4.5rem !important;
        }

        .display3-xl {
            font-size: 3.5rem !important;
        }

        .display4-xl {
            font-size: 2.5rem !important;
        }

        .display5-xl {
            font-size: 2.25rem !important;
        }
    }

    @media (min-width: 1400px) {
        .font-size-h1-xxl {
            font-size: 2rem !important;
        }

        .font-size-h2-xxl {
            font-size: 1.75rem !important;
        }

        .font-size-h3-xxl {
            font-size: 1.5rem !important;
        }

        .font-size-h4-xxl {
            font-size: 1.35rem !important;
        }

        .font-size-h5-xxl {
            font-size: 1.25rem !important;
        }

        .font-size-h6-xxl {
            font-size: 1.175rem !important;
        }

        .display1-xxl {
            font-size: 5.5rem !important;
        }

        .display2-xxl {
            font-size: 4.5rem !important;
        }

        .display3-xxl {
            font-size: 3.5rem !important;
        }

        .display4-xxl {
            font-size: 2.5rem !important;
        }

        .display5-xxl {
            font-size: 2.25rem !important;
        }
    }

    .font-weight-boldest {
        font-weight: 700;
    }

    .table thead th,
    .table thead td {
        font-weight: 600;
        font-size: 1rem;
        border-bottom-width: 1px;
        padding-top: 1rem;
        padding-bottom: 1rem;
    }

    .table.table-head-borderless thead th,
    .table.table-head-borderless thead td {
        border-top: 0;
    }

    .table.table-head-solid thead th,
    .table.table-head-solid thead td {
        background-color: #F3F6F9;
    }

    .table.table-light-primary thead tr {
        color: #3eaae8;
        background-color: rgba(102, 50, 89, 0.1);
    }

    .table.table-light-secondary thead tr {
        color: #3252DF;
        background-color: rgba(228, 230, 239, 0.1);
    }

    .table.table-light-success thead tr {
        color: #1BC5BD;
        background-color: rgba(27, 197, 189, 0.1);
    }

    .table.table-light-info thead tr {
        color: #8950FC;
        background-color: rgba(137, 80, 252, 0.1);
    }

    .table.table-light-warning thead tr {
        color: #FFA800;
        background-color: rgba(255, 168, 0, 0.1);
    }

    .table.table-light-danger thead tr {
        color: #F64E60;
        background-color: rgba(246, 78, 96, 0.1);
    }

    .table.table-light-light thead tr {
        color: #F3F6F9;
        background-color: rgba(243, 246, 249, 0.1);
    }

    .table.table-light-dark thead tr {
        color: #181C32;
        background-color: rgba(24, 28, 50, 0.1);
    }

    .table.table-light-white thead tr {
        color: #ffffff;
        background-color: rgba(255, 255, 255, 0.1);
    }

    .table.table-head-custom thead tr,
    .table.table-head-custom thead th {
        font-weight: 600;
        color: #B5B5C3 !important;
        font-size: 0.9rem;
        text-transform: uppercase;
        letter-spacing: 0.1rem;
    }

    .table.table-foot-custom tfoot th,
    .table.table-foot-custom tfoot td {
        font-weight: 600;
        color: #B5B5C3 !important;
        font-size: 0.9rem;
        text-transform: uppercase;
        letter-spacing: 0.1rem;
    }

    .table.table-head-bg thead tr,
    .table.table-head-bg thead th {
        background-color: #F3F6F9;
        border-bottom: 0;
        letter-spacing: 1px;
    }

    .table.table-head-bg thead tr:first-child,
    .table.table-head-bg thead th:first-child {
        border-top-left-radius: 0.675rem;
        border-bottom-left-radius: 0.675rem;
    }

    .table.table-head-bg thead tr:last-child,
    .table.table-head-bg thead th:last-child {
        border-top-right-radius: 0.675rem;
        border-bottom-right-radius: 0.675rem;
    }

    .table.table-foot-bg tfoot th,
    .table.table-foot-bg tfoot td {
        border-bottom: 0;
        background-color: #F3F6F9;
    }

    .table.table-foot-bg tfoot th:first-child,
    .table.table-foot-bg tfoot td:first-child {
        border-top-left-radius: 0.675rem;
        border-bottom-left-radius: 0.675rem;
    }

    .table.table-foot-bg tfoot th:last-child,
    .table.table-foot-bg tfoot td:last-child {
        border-top-right-radius: 0.675rem;
        border-bottom-right-radius: 0.675rem;
    }

    .table.table-separate th,
    .table.table-separate td {
        border-top: 0;
        border-bottom: 1px solid #EBEDF3;
    }

    .table.table-separate th:first-child,
    .table.table-separate td:first-child {
        padding-left: 0 !important;
    }

    .table.table-separate th:last-child,
    .table.table-separate td:last-child {
        padding-right: 0 !important;
    }

    .table.table-separate tfoot th,
    .table.table-separate tfoot td {
        border-bottom: 0;
        border-top: 1px solid #EBEDF3;
    }

    .table.table-separate tbody tr:last-child td {
        border-bottom: 0;
    }

    .table.table-bordered tfoot th,
    .table.table-bordered tfoot td {
        border-bottom: 0;
    }

    .table.table-vertical-center th,
    .table.table-vertical-center td {
        vertical-align: middle;
    }

    .table:not(.table-bordered) thead th,
    .table:not(.table-bordered) thead td {
        border-top: 0;
    }

    .card-rounded-bottom .apexcharts-canvas svg {
        border-bottom-left-radius: 0.675rem;
        border-bottom-right-radius: 0.675rem;
    }

    .rounded .apexcharts-canvas svg {
        border-radius: 0.675rem !important;
    }

    .rounded-sm .apexcharts-canvas svg {
        border-radius: 0.28rem !important;
    }

    .rounded-lg .apexcharts-canvas svg {
        border-radius: 0.85rem !important;
    }

    .rounded-xl .apexcharts-canvas svg {
        border-radius: 1.25rem !important;
    }

    /*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
    html {
        font-family: sans-serif;
        -moz-text-size-adjust: 100%;
        text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    html,
    body {
        height: 100%;
        margin: 0px;
        padding: 0px;
        font-size: 13px !important;
        font-weight: 400;
        font-family: Poppins, Helvetica, "sans-serif";
        -ms-text-size-adjust: 100%;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    @media (max-width: 1199.98px) {

        html,
        body {
            font-size: 12px !important;
        }
    }

    @media (max-width: 991.98px) {

        html,
        body {
            font-size: 12px !important;
        }
    }

    html a:hover,
    html a:active,
    html a:focus,
    body a:hover,
    body a:active,
    body a:focus {
        text-decoration: none !important;
    }

    html a,
    html button,
    body a,
    body button {
        outline: none !important;
    }

    body {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        color: #3F4254;
    }

    body {
        background: #ffffff;
    }

    .wrapper {
        background-color: #ffffff;
        border-radius: 1.25rem;
    }

    @media (min-width: 992px) {
        .wrapper {
            margin: 20px 0;
            margin-left: 110px;
            padding-top: 10px;
        }

        .container,
        .container-fluid,
        .container-sm,
        .container-md,
        .container-lg,
        .container-xl,
        .container-xxl {
            padding: 0 30px;
        }
    }

    @media (max-width: 991.98px) {
        .wrapper {
            margin-left: initial;
        }

        .container,
        .container-fluid,
        .container-sm,
        .container-md,
        .container-lg,
        .container-xl,
        .container-xxl {
            max-width: none;
            padding: 0 20px;
        }
    }
</style>