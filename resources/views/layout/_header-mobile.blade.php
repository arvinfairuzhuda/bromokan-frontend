<!--begin::Header Mobile-->
<div id="kt_header_mobile" class="header-mobile header-mobile-fixed">
    <!--begin::Logo-->
    <a href="{{route('home')}}" class="text-dark font-size-h2">
        <img alt="Logo" src="{{asset('media/logos/logo.svg')}}" class="logo-default max-h-40px" />
        {{-- Bromokan --}}
    </a>
    <!--end::Logo-->
    <!--begin::Toolbar-->
    {{-- <div class="d-flex align-items-center">
        <button class="btn p-0 burger-icon burger-icon-left rounded-0" id="kt_header_mobile_toggle">
            <span></span>
        </button>
    </div> --}}
    <!--end::Toolbar-->
</div>
<!--end::Header Mobile-->