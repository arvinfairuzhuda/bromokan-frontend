<!--begin::Header-->
{{-- header-fixed --}}
<div id="kt_header" class="header header-fixed">
    <!--begin::Header Wrapper-->
    <div class="header-wrapper rounded-bottom-xl d-flex flex-grow-1 align-items-center">
        <!--begin::Container-->
        <div class="container d-flex align-items-center justify-content-end justify-content-lg-between flex-wrap">
            <!--begin::Toolbar-->
            <div class="py-3 py-lg-2 d-none d-lg-block">
                <a href="{{route('home')}}" class="text-dark font-size-h2">
                    <img alt="Logo" src="{{asset('media/logos/logo.svg')}}" class="logo-default max-h-60px" />
                    {{-- Bromokan --}}
                </a>
            </div>
            <!--end::Toolbar-->
            <!--begin::Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-right" id="kt_header_menu_wrapper">
                <!--begin::Menu-->
                <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
                    <!--begin::Nav-->
                    {{-- <ul class="menu-nav">
                        <li class="menu-item menu-item-open menu-item-here" data-menu-toggle="click"
                            aria-haspopup="true">
                            <a href="#" class="menu-link">
                                <span class="menu-text">Penginapan</span>
                            </a>
                        </li>
                        <li class="menu-item" data-menu-toggle="click" aria-haspopup="true">
                            <a href="#" class="menu-link">
                                <span class="menu-text">Wisata</span>
                            </a>
                        </li>
                        <li class="menu-item" data-menu-toggle="click" aria-haspopup="true">
                            <a href="#" class="menu-link">
                                <span class="menu-text">Transportasi</span>
                            </a>
                        </li>
                        <li class="menu-item" data-menu-toggle="click" aria-haspopup="true">
                            <a href="#" class="menu-link">
                                <span class="menu-text">Artikel</span>
                            </a>
                        </li>
                    </ul> --}}
                    <!--end::Nav-->
                </div>
                <!--end::Menu-->
            </div>
            <!--end::Menu Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header Wrapper-->
</div>
<!--end::Header-->