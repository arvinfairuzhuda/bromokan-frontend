<!DOCTYPE html>
<html>

<head>
    <title>Bromokan | Pesananmu sedang diproses</title>
    @include('mails.style')
</head>

<body id="kt_body">
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container mt-15 mt-lg-0">
                            <!--begin::Home-->
                            <div class="row mt-20 mt-lg-30">
                                <div class="col-12 col-lg-8">
                                    <h3 class="mb-10 font-weight-bold text-dark">
                                        Pesanan kamu sedang diproses yaa, silahkan tunggu dan admin akan menghubungi
                                        kamu!
                                    </h3>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="float-right">
                                        <h5 class="font-weight-bold text-dark">
                                            Kode Pesananmu
                                        </h5>
                                        <h1 class="text-primary font-weight-bolder">
                                            1622347838
                                        </h1>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row justify-content-center pt-8 pt-md-27">
                                        <div class="col-md-12">
                                            <!-- begin: Invoice header-->
                                            <div
                                                class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                                                <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>
                                                <div class="d-flex flex-column align-items-md-end px-0">
                                                    <!--begin::Logo-->
                                                    <p class="mb-5 max-w-200px">
                                                        Bromokan
                                                    </p>
                                                    <!--end::Logo-->
                                                    <span
                                                        class="d-flex flex-column align-items-md-end font-size-h5 font-weight-bold text-muted">
                                                        <span>Desa Wonokitri, Pintu Masuk Bromo via
                                                            Nongkojajar</span>
                                                        <span>Pasuruan, Jawa Timur, Indonesia</span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="rounded-xl overflow-hidden w-100 max-h-md-250px mb-15 mb-md-30">
                                                <img src="http://bromokan.com/media/bg/bg-invoice-5.jpg" class="w-100"
                                                    alt="" />
                                            </div>
                                            <!--end: Invoice header-->
                                            <!--begin: Invoice body-->
                                            <div class="row border-bottom pb-10">
                                                <div class="col-md-9 py-md-10 pr-md-10">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th
                                                                        class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Perlengkapan</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Jumlah</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Hari</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Harga</th>
                                                                    <th
                                                                        class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                        Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="font-weight-bolder font-size-lg">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-warning p-3">
                                                                            <i class="la la-map-marked-alt
                                                    icon-2x text-light"></i>
                                                                        </span>
                                                                        <div>
                                                                            <div id="name_destination" class="mr-2">
                                                                                Destinasi 1
                                                                            </div>
                                                                            <small>
                                                                                (+ Jeep)
                                                                            </small>
                                                                        </div>
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="jeep_count_total">1</span>
                                                                        Kendaraan
                                                                    </td>
                                                                    <td class="text-right pt-7">1 Hari</td>
                                                                    <td class="text-right pt-7">
                                                                        <div id="price_destination_label">
                                                                            IDR 200.000
                                                                        </div>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <div id="jeep_price_total">
                                                                            IDR 200.000
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr class="font-weight-bolder font-size-lg"
                                                                    id="review_penginapan">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-success p-3">
                                                                            <i class="la la-home
                                                    icon-2x text-light"></i>
                                                                        </span>
                                                                        <span id="nama_hotel_label">Quintessa
                                                                            Stewart</span>
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="hotel_room_label">1</span> Kamar
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="hotel_day_review_label">1</span> Malam
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="harga_hotel_review_label">IDR
                                                                            150.000</span>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <span id="total_hotel_review_label">IDR
                                                                            150.000</span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="font-weight-bolder font-size-lg"
                                                                    id="review_ticket">
                                                                    <td
                                                                        class="border-top-0 pl-0 d-flex align-items-center">
                                                                        <span
                                                                            class="navi-icon mr-2 rounded-circle bg-primary p-3">
                                                                            <i class="la la-ticket-alt
                                                    icon-2x text-light"></i>
                                                                        </span>
                                                                        Tiket Masuk Bromo
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="ticket_count_label">1</span>
                                                                        Tiket</td>
                                                                    <td class="text-right pt-7">
                                                                        -
                                                                    </td>
                                                                    <td class="text-right pt-7">
                                                                        <span id="harga_ticket_review_label">IDR
                                                                            34.000</span>
                                                                    </td>
                                                                    <td
                                                                        class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                        <span id="total_ticket_review_label">IDR
                                                                            34.000</span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                    <div class="d-flex flex-column flex-md-row">
                                                        <div class="d-flex flex-column mb-10 mb-md-0">
                                                            <div class="font-weight-bold font-size-h6 mb-3">
                                                                INVOICE MILIK</div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">Nama
                                                                    Lengkap:</span>
                                                                <span class="text-right" id="invoice_name">Emi
                                                                    Jenkins</span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">Email:</span>
                                                                <span class="text-right"
                                                                    id="invoice_email">fanekefojy@mailinator.com</span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">No
                                                                    Telepon:</span>
                                                                <span class="text-right" id="invoice_phone">+1 (622)
                                                                    525-5241</span>
                                                            </div>
                                                            <div
                                                                class="d-flex justify-content-between font-size-lg mb-3">
                                                                <span class="font-weight-bold mr-15">No
                                                                    Whatsapp:</span>
                                                                <span class="text-right"
                                                                    id="invoice_whatsapp">81023810923</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 border-left-md pl-md-10 py-md-10 text-right">
                                                    <!--begin::Total Amount-->
                                                    <div class="font-size-h4 font-weight-bolder text-muted mb-3">
                                                        TOTAL HARGA</div>
                                                    <div class="font-size-h1 font-weight-boldest" id="total_amount">
                                                        IDR 384.000
                                                    </div>
                                                    <div class="text-muted font-weight-bold mb-16"></div>
                                                    <!--end::Total Amount-->
                                                    <div class="border-bottom w-100 mb-16"></div>
                                                    <!--begin::Invoice To-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        KEPADA</div>
                                                    <div class="font-size-lg font-weight-bold mb-10" id="kepada">Emi
                                                        Jenkins
                                                    </div>
                                                    <!--end::Invoice To-->
                                                    <!--begin::Invoice No-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        NO INVOICE</div>
                                                    <div class="font-size-lg font-weight-bold mb-10">1622347838</div>
                                                    <!--end::Invoice No-->
                                                    <!--begin::Invoice Date-->
                                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3">
                                                        TANGGAL PESAN</div>
                                                    <div class="font-size-lg font-weight-bold">
                                                        Minggu, 30 Mei 2021 11:10:38
                                                    </div>
                                                    <!--end::Invoice Date-->
                                                </div>
                                            </div>
                                            <!--end: Invoice body-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Home-->
                            <!--end::Entry-->
                        </div>
                    </div>
                </div>
                <!--end::Content-->

                <!--begin::Footer-->
                <div class="footer py-2 py-lg-0 my-5 d-flex flex-lg-column" id="kt_footer">
                    <!--begin::Container-->
                    <div
                        class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted font-weight-bold mr-2">©2021</span>
                            <a href="{{url('/')}}" target="_blank"
                                class="text-dark-75 text-hover-primary">bromokan.com</a>
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Nav-->
                        <div class="nav nav-dark order-1 order-md-2">

                        </div>
                        <!--end::Nav-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Footer-->
            </div>
            <!--end::Wrapper-->


        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
</body>

</html>